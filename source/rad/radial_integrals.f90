module radial_integrals
! Time-stamp: "2003-03-26 17:34:53 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug3, bug4, bug6
  use rm_data, only: ra, lamax, mpol, maxorb
  use rm_xdata, only: nvary, maxnhf, maxnc, lrang1, lrang2, minim, &
       lexch, maxlc, ncore, lrang4
  use symmetries, only: lrang3, llpdim
  implicit none

  integer, save               :: orb
  integer, save               :: nbbpol
  integer, save               :: nccpol
  integer, save               :: nbcpol
  integer, allocatable, save  :: ibbpol(:,:,:), ibc(:)
  integer, allocatable, save  :: ibcpol(:,:), iccpol(:,:)
  integer, allocatable, save  :: ictccd(:,:,:), ictcce(:,:,:)
  integer, allocatable, save  :: nccd(:,:), ncce(:,:)
  integer, allocatable, save  :: iccdst(:,:), iccest(:,:)
  integer, allocatable, save  :: irkbb(:,:,:), irkbc(:,:,:)
  integer, allocatable, save  :: i1bb(:), i1bc(:), i1cc(:)

  private
  public genint

contains

  subroutine genint
! this routine controls the evaluation of all the radial integrals
    use file_info, only: itape3, nbuff2, jbuff1, jbuff2, jbuff4, jbuff5
    use one_el, only: jkm
    use filehand, only: newfil, endw, iwrita, iwritb, iwritc, writix, &
         close
    use error_prt, only: alloc_error
    character(len=7)                    :: stats
    integer                             :: ndummy
    integer                             :: lam, lbc, nr1bb, lamdim
    integer                             :: nlam, lamedm, ibb, nlr, lpup
    integer                             :: nrkd, iccd, icce, nrkbb, ldif
    integer                             :: nbuf, nrke, l1bc, n, l, lp
    integer                             :: nval, lamind
    integer                             :: status, jkup, jup, kup, l34

    orb = maxorb
    jkm = 999
    nbuff2 = 2
    call newfil (jbuff1, ndummy)
    call newfil (jbuff4, ndummy)

    write (fo,'(/,a,/)') 'Subroutine Genint'

! generate the multipole radial integrals
    lamind = (lamax + 1) / 2
    if (lamax > 0) write (fo,'(a)') 'Bound-bound multipole integrals'
    allocate (ibbpol(lrang1, lrang1, lamind), stat=status)
    if (status /= 0) call alloc_error (status, 'genint', 'a')
    call genmbb (jbuff1)
! write the multipole pointer arrays to rad3
    write(itape3) ibbpol, nbbpol
    deallocate (ibbpol, stat=status)
    if (status /= 0) call alloc_error (status, 'genint', 'd')

    if (mpol > 0) then   ! generate velocity gauge dipoles
       if (bug3 /= 0) write (fo,'(a)') 'Bound-continuum mul&
            &tipole integrals'
       allocate (ibcpol(lrang1,lrang2), iccpol(lrang2,lrang2),   &
            stat=status)
       if (status /= 0) call alloc_error (status, 'genint', 'a')
       call genmbc (minim, maxlc, mpol, jbuff1)
       if (bug3 /= 0) write (fo,'(a)') 'Continuum-continuum mu&
            &ltipole integrals'
       call genmcc (minim, mpol, jbuff1)
       write (itape3) nbcpol, nccpol
       write (itape3) ibcpol
       write (itape3) iccpol
       deallocate (ibcpol, iccpol, stat=status)
       if (status /= 0) call alloc_error (status, 'genint', 'd')
    end if
! generate the one electron integrals
    if (bug4 /= 0) write (fo,'(/,a)') 'Bound-bound one electron &
         &integrals'
    allocate (i1bb(lrang1), i1bc(lrang2+1), i1cc(lrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'genint', 'd')
    call gen1bb (ncore, jbuff4, nr1bb, i1bb)
    if (bug4 /= 0) write (fo,'(/,a)') 'Bound-continuum one &
         &electron integrals'
    call gen1bc (ncore, minim, maxlc, jbuff4, l1bc)
    if (bug4 /= 0) write (fo,'(/,a)') 'Continuum-continuum one &
         &electron integrals'
    call gen1cc (ncore, minim, maxlc, lexch, jbuff4, i1cc)

! generate the two electron rk integrals

    jkup = (lrang1 * (lrang1 + 1)) / 2
    jup = (lrang1 * (lrang1 + 1)) / 2
    kup = lrang1 * MIN(3*lrang1-2, lrang2)
    nlam = 2 * lrang1 - 1
!   nlam = MAX(MIN(lrang2, llpdim), 2 * lrang1 - 1)
    l34 = lrang3 + lrang4 - 1
    lamdim = lrang1 + lrang1 - 1
    lamedm = (lrang1 + lrang2) / 2
    allocate (irkbb(nlam,jkup,jkup), irkbc(nlam,jup,kup),  &
         ibc(lrang3+lrang4), ictccd(lrang1,lrang1,lamdim), &
         iccdst(llpdim,lrang2), nccd(llpdim,lrang2),       &
         ncce(llpdim,lrang2), ictcce(lrang1,lrang1,lamedm),&
         iccest(llpdim,lrang2), stat=status)
    if (status /= 0) call alloc_error (status, 'genint', 'd')

! generate the bound-bound rk integrals
    write (fo,'(/,a,/)') 'Bound-bound RK integrals'
    call genbb (jbuff1, nrkbb, ibb, irkbb)
 
! generate the bound-continuum rk integrals
    write (fo,'(/,a,/)') 'Bound-continuum RK integrals'
    call genbc (minim, jbuff1, lbc, ibc, irkbc)

! generate the continuum-continuum rk integrals
    write (fo,'(/a,/)') 'Continuum-continuum RK integrals'
    nbuf = (minim - 1) * llpdim + 1
    l_L: do l = minim-1, lrang2-1
       lpup = MIN(lrang2-1, llpdim-1+l)
       ldif = 0
       nbuff2 = nbuf
       lp_L: do lp = l, lpup
          ldif = ldif + 1
          write (fo,'(a,i5,a,i5)') 'L =', l,'; LP = ', lp
          call skiper (l+1, lp+1, 0, 0)
          call gencc (l, lp, lexch, llpdim, jbuff1, jbuff4, ictccd, &
               iccd, nrkd, ictcce, icce, nrke)
          write (jbuff2,rec=nbuff2) ictccd
          iccdst(ldif,l+1) = iccd
          nccd(ldif,l+1) = nrkd
          if (lp < lexch) then
             write (jbuff5,rec=nbuff2) ictcce
             if (bug3 > 1) then
                 write(*,*) 'ictcce for lp =', lp
                 write(*,*) ictcce
             end if
             iccest(ldif,l+1) = icce
             ncce(ldif,l+1) = nrke
          end if
          nbuff2 = nbuff2 + 1
          write (fo,'(a,i7)') '# direct integrals stored   = ', nrkd
          write (fo,'(a,i7)') '# exchange integrals stored = ', nrke
       end do lp_L
       nbuf = nbuf + llpdim
    end do l_L

! write no exchange pointers to rad3
    write (itape3) i1cc, nccd(:llpdim,:lrang2), iccdst(:llpdim,:lrang2)
! write exchange pointers to rad3
    if (lexch /=  0) then
       write(itape3) ncce(:llpdim,:lrang2), iccest(:llpdim,:lrang2)
    end if

    deallocate (ictccd, ictcce, iccdst, nccd, ncce, iccest, i1cc, &
         stat=status)
    if (status /= 0) call alloc_error (status, 'genint', 'd')

! finish writing to rad1 and rad4
    call endw (jbuff1)
    call endw (jbuff4)

! write the one electron pointer arrays to the index of rad4
    call writix (jbuff4)
    call iwritb (jbuff4, i1bb, 1, lrang1) ! write i1bb to disk
    call iwrita (jbuff4, nr1bb)
    call iwritb (jbuff4, i1bc, minim, l1bc+1) ! write i1bc to disk
    call endw (jbuff4)

! write the two electron bound-bound pointer arrays to the index of rad1
    call writix (jbuff1)

    nlr = (lrang1 * (lrang1 + 1)) / 2
    do n = 1, nlr
       call iwritc (jbuff1, irkbb(:,:nlr,n), 1, nlam, 1, nlr, &
            nlam, 0)
    end do
    call iwrita (jbuff1, ibb)
    call iwrita (jbuff1, nrkbb)
    do n = 1, lrang1*lbc
       call iwritc (jbuff1, irkbc(:,:nlr,n), 1, nlam, 1, nlr, &
            nlam, 0)
    end do
    call iwritb (jbuff1, ibc, minim, lbc+1) ! write ibc to disk
    call endw (jbuff1)

    deallocate (irkbb, irkbc, i1bb, i1bc, stat=status)
    if (status /= 0) call alloc_error (status, 'genint', 'd')

    stats = 'keep'
    call close (jbuff1, stats)
    call close (jbuff4, stats)
  end subroutine genint

  subroutine genmbb (jbuff1)
! generates all the bound-bound multipole integrals
    use filehand, only: catlog, writb
    integer, intent(in)       :: jbuff1
    real(wp)                  :: x(2)
    integer                   :: l1, l2, maxc1, maxhf1, maxc2, maxhf2
    integer                   :: lamlo, lamup, lamst, lam, n2, n2up
    integer                   :: iup, lup, n1, mxc2p1, lamlov, l1p1
    integer                   :: mxc1p1, l2p1

    if (bug3 >= 1) then
       write (fo,'(38("-"))')
       write (fo,'(a,/,a)') '    lam   l1  l1p   n1  n1p  rval',&
            '    ---   --  ---   --  ---  ----'
    end if
    ibbpol = -999

! store starting position in ibbpol(1,1,1)
    call catlog (jbuff1, ibbpol(1,1,1))
    nbbpol = 0
    lup = lrang1 - 1
    l1_L: do l1 = 0, lup
       maxc1 = maxnc(l1+1)
       maxhf1 = maxnhf(l1+1)
       if (maxc1 == maxhf1) cycle
       l2_L: do l2 = l1, lup
          maxc2 = maxnc(l2+1)
          maxhf2 = maxnhf(l2+1)
          if (maxc2 == maxhf2) cycle
          lamlo = ABS(l1 - l2)
          if (lamlo == 0) lamlo = 2
          lamst = (lamlo + 1) / 2 - 1
          lamup = MIN(l1+l2, lamax)
          lam_L: do lam = lamlo, lamup, 2
             lamst = lamst + 1
             call catlog (jbuff1, ibbpol(l1+1,l2+1,lamst))
             n1_L: do n1 = maxc1+1, maxhf1
                if (l1 == l2) then
                   n2up = n1
                else
                   n2up = maxhf2
                end if
                n2_L: do n2 = maxc2+1, n2up
                   call radint(n1,l1+1, n2, l2+1, lam, x(1))
                   iup = 1
                   call writb (jbuff1, x, 1, iup)
                   if (bug6 == 1) write (fo,'(a,2x,5i5,e16.8)') 'length', lam, l1,&
                        l2, n1, n2, x(1)
                   nbbpol = nbbpol + iup
! added by hugo
                   if (lam == 1) then
                    call derint(n1,l1+1,n2,l2+1,x(1))
                    call writb (jbuff1, x, 1, iup)
                    nbbpol=nbbpol+iup
                   endif
                   if (bug6 == 1) write (fo,'(a,2x,5i5,e16.8)') 'vel', lam, l1,&
                        l2, n1, n2, x(1)
                end do n2_L
             end do n1_L
          end do lam_L
       end do l2_L
    end do l1_L
    write (fo,'(a,i6)') 'Total # bound-bound multipole&
         & integrals stored         = ', nbbpol
  end subroutine genmbb

  subroutine radint (n1, l1, n2, l2, k, x)
! evaluates x, the radial multipole integral of order k between
! two numerical orbitals specified by the quantum numbers (n1,l1-1)
! and (n2,l2-1).
    use bound_basis, only: uj, ipos
    use radial_grid, only: xr, wt, npts
    integer, intent(in)         :: n1, l1
    integer, intent(in)         :: n2, l2
    integer, intent(in)         :: k
    real(wp), intent(out)       :: x
    integer                     :: j1, j2, i

    x = 0.0_wp
    j1 = ipos(n1,l1)
    j2 = ipos(n2,l2)
    do i = 1, npts
       x = x + wt(i) * uj(i,j1) * uj(i,j2) * xr(i)**k
    end do
  end subroutine radint

  subroutine gen1bb (ncore, jbuff4, nr1bb, i1bb)
! generates and stores all the bound-bound one electron integrals
    use filehand, only: catlog, writa
    use one_el, only: oneele
    integer, intent(in)     :: ncore
    integer, intent(in)     :: jbuff4
    integer, intent(out)    :: nr1bb
    integer, intent(out)    :: i1bb(lrang1)
    integer                 :: l, nlo, n1, n2, maxhf, istart
    real(wp)                :: x, rint

    nr1bb = 0
    if (bug4 >= 1) then
       write (fo,'(38("-"))')
       if (ncore > 0) then
          write (fo,'(a,/,a)') '      l   n1   n2      r1bb&
               &            rint', '      -   --   --      --&
               &--            ----'
       else
          write (fo,'(a,/,a)') '      l   n1   n2      r1bb', &
               '      -   --   --      ----'
       end if
    end if
    call catlog (jbuff4, istart)
    l_L: do l = 1, lrang1
       i1bb(l) = -999
       maxhf = maxnhf(l)
       nlo = maxnc(l) + 1
       if (maxhf == maxnc(l)) cycle
       call catlog (jbuff4, i1bb(l))
       n1_L: do n1 = nlo, maxhf
          n2_L: do n2 = nlo, n1
             nr1bb = nr1bb + 1
             call oneele (n1, l, n2, l, x)
             if (ncore > 0) then
                call hint (l, n1, n2, 0, rint)
                if (bug4 >= 1) write (fo,'(2x,3i5,2e16.8)') l-1, &
                     n1, n2, x, rint
                x = x + rint
             else 
                if (bug4 >= 1) write (fo,'(2x,3i5,e16.8)') l-1, &
                     n1, n2, x
             end if
             call writa (jbuff4, x)
          end do n2_L
       end do n1_L
    end do l_L
    i1bb(1) = istart
    write (fo,'(/,a,i6)') '# 1-electron BB integrals stored = ', nr1bb
  end subroutine gen1bb

  subroutine hint (ls, ns1, ns2, noexch, rint)
    use angular_momentum, only: rme
    use bassplnew_orb, only: dorkin
    integer, intent(in)       :: ls
    integer, intent(in)       :: ns1, ns2
    integer, intent(in)       :: noexch
    real(wp), intent(out)     :: rint
    integer                   :: lr, nrup, nr, lamlo, lamup, lam
    real(wp)                  :: rsum, rval(1), rtmp, rmeval

! loop over bound shells in the core
    rint = 0.0_wp
    lr_L: do lr = 1, SIZE(maxnc)
       nrup = maxnc(lr)
       rsum = 0.0_wp
       do nr = lr, nrup
!         call dorkin (nr, lr, ns1, ls, nr, lr, ns2, ls, 0, rval)
          call dorkin (ns1, ls, nr, lr, ns2, ls, nr, lr, nr, 0, rval(1))
          rsum = rsum + rval(1)
       end do
       rint = rint + REAL(4*lr-2,wp) * rsum
       if (noexch /= 0) cycle
       rtmp = 0.0_wp
       lamlo = ABS(ls - lr)
       lamup = ls + lr - 2
       lam_L: do lam = lamlo, lamup, 2
          rsum = 0.0_wp
          do nr = lr, nrup
             call dorkin (ns1, ls, nr, lr, nr, lr, ns2, ls, ns2, lam, rval(1))
             rsum = rsum + rval(1)
          end do
          rmeval = rme(lr-1,ls-1,lam)
          rtmp = rtmp + rsum * rmeval * rmeval
       end do lam_L
       
       rint = rint - rtmp / REAL(2*ls-1,wp)
    end do lr_L
  end subroutine hint

  subroutine gen1bc (ncore, minim, maxlc, jbuff4, lcup)
! generates and stores all the bound-continuum one electron integrals
    use filehand, only: catlog, writa
    use one_el, only: oneele
    integer, intent(in)       :: ncore
    integer, intent(in)       :: minim
    integer, intent(in)       :: maxlc
    integer, intent(in)       :: jbuff4
    integer, intent(out)      :: lcup
    integer                   :: l, nlo, n1, n2, nbup, nblo, nb, nc
    integer                   :: lc, nr1bc, ncup
    real(wp)                  :: x, rint

    lcup = MIN(lrang1, lrang2)
    lc_L: do lc = minim, lcup
       if (bug4 > 1) then
          write (fo,'(38("-"))')
          if (ncore > 0) then
             write (fo,'(a,/,a)') '     lc   nb   nc      r1bc&
                  &            rint', '     --   --   --      --&
                  &--            ----'
          else
             write (fo,'(a,/,a)') '     lc   nc   nc      r1bc', &
                  '     --   --   --      ----'
          end if
       end if
       nr1bc = 0
       i1bc(lc) = -999
       nblo = maxnc(lc) + 1
       nbup = maxnhf(lc)
       ncup = nvary(lc)
! fix for calculations where all shells with l = lc are in the core
! (eg 3d10 the only d orbitals)
! seems to work but needs authenticating.
       if (nbup < nblo) then
          call catlog (jbuff4, i1bc(lc))
          cycle
       end if 
       call skiper (lc, 0, 0, 0)
       call skiper (lc, 0, 0, 0, 3)
       call catlog (jbuff4, i1bc(lc))
       nb_L: do nb = nblo, nbup
          nc_L: do nc = 1, ncup
             nr1bc = nr1bc + 1
             call oneele (nb, lc, nbup+nc, lc, x)
             if (ncore > 0) then
                call hint (lc, nb, nbup+nc, 0, rint)
                if (bug4 > 1) write (fo,'(2x,3i5,2e16.8)') lc-1, &
                     nb, nc, x, rint
                x = x + rint
             else
                if (bug4 > 1) write (fo,'(2x,3i5,e16.8)') lc-1, &
                     nb, nc, x
             end if
             call writa (jbuff4, x)
          end do nc_L
       end do nb_L
       write (fo, '(/,a,i5,a,i6)') '# 1-electron BC integrals stored &
            & for cont. a.m.', lc-1, ' = ', nr1bc
    end do lc_L
    call catlog (jbuff4, i1bc(lcup+1))
  end subroutine gen1bc

  subroutine skiper (lc1, lc2, lc3, lc4, t)
! read continuum orbitals of angular momentum (lc(i)-1), i=1,4, and 
! their schmidt coefficients from scratch disc idisc1 into uj array.
! if n < 4 orbitals are required, the last (n-4) elements of lc 
! should be set to zero.
    use radial_grid, only: nix, irx
    use rm_xdata, only: maxnhf, maxnlg
    use bound_basis, only: c, nco, uj, duj, d1uj, ipos
    use bassplnew_orb, only: b, ovrlap
    use file_info, only: u1, u2, u3
    integer, intent(in)           :: lc1, lc2, lc3, lc4
    integer, intent(in), optional :: t
    integer, parameter            :: ncorb = 4
    integer                       :: lc(ncorb)
    integer                       :: npts, il, l, maxhf, itest
    integer                       :: n1, n2, n, typ, maxlg

    typ = 1
    if (PRESENT(t)) typ = t
    if (lc1 <= 0) then
       write (fo,'(a)') ' ===== error in skiper ===='
       stop
    end if
    npts = irx(nix) + 1
    lc = (/lc1, lc2, lc3, lc4/)
    n2 = orb
    il_L: do il = 1, ncorb   ! 1
       l = lc(il)
       if (l <= 0) return
       maxhf = maxnhf(l)
       maxlg = maxnlg(l)
       itest = maxhf - maxlg
       n1 = n2 + 1
       n2 = n1 + nvary(l) - 1
       select case (typ)
       case (1)
          read (u1,rec=l) uj(:npts,n1:n2)
       case (2)
          read (u2,rec=l) d1uj(:npts,n1:n2)
       case (3)
          read (u3,rec=l) duj(:npts,n1:n2)
       end select
       do n = 1, nvary(l)
          ipos(maxhf+n,l) = n1 + n - 1
       end do
! the following test hasn't been implemented in basis_orbitals/bassplnew
!       if (itest > 0) read (u2,rec=l) b, ovrlap
    end do il_L
  end subroutine skiper

  subroutine gen1cc (ncore, minim, maxlc, lexch, jbuff4, i1cc)
! generates and stores all continuum-continuum one electron integrals
    use filehand, only: catlog, writa
    use one_el, only: oneele
    use angular_momentum, only: rme
    use bassplnew_orb, only: dorkin
    integer, intent(in)          :: ncore
    integer, intent(in)          :: minim
    integer, intent(in)          :: maxlc
    integer, intent(out)         :: i1cc(lrang2)
    integer, intent(in)          :: lexch
    integer, intent(in)          :: jbuff4
    integer                      :: l, nlo, n1, n2, lc, icount, maxhf
    integer                      :: maxn, err, lam, lamup, lamlo
    integer			 :: lr,nr,nrup
    real(wp)                     :: x, rint, sumx, sumax, rmeval, rval(2000)
    real(wp), allocatable	 :: store(:,:)

    if (bug4 > 2) then
       write (fo,'(39("-"))')
       if (ncore > 0) then
          write (fo,'(a,/,a)') '     lc   n1   n2      r1cc&
               &            rint', '     --   --   --      --&
               &--            ----'
       else 
          write (fo,'(a,/,a)') '     lc   n1   n2      r1cc', &
               '     --   --   --      ----'
       end if
    end if

    lc_L: do lc = minim, lrang2
       maxhf = maxnhf(lc)
       call skiper (lc, 0, 0, 0)
       call skiper (lc, 0, 0, 0, 3)
       call catlog (jbuff4, i1cc(lc))
       icount = 0
       sumx=0._wp
       sumax=0._wp
       maxn=nvary(lc)
       allocate(store(maxn,maxn),stat=err)
       if (err /= 0 ) then
         print *,'Allocation problem in genbc'
         stop
       endif
       store=0._wp
!      n1_L: do n1 = 1, nvary(lc)
!         n2_L: do n2 = 1, n1
!            icount = icount + 1
!            call oneele (n1+maxhf, lc, n2+maxhf, lc, x)
!
! ASSUME EXCHANGE IS INCLUDED? NO, OPTIONAL
!
       if (ncore > 0) then
!               if (lc > lexch) then
!                  call hint (lc, n1+maxhf, n2+maxhf, 1, rint)
!               else
!                  call hint (lc, n1+maxhf, n2+maxhf, 0, rint)
!               end if
!   rint = 0.0_wp
         lr_L: do lr = 1, SIZE(maxnc)
           nrup = maxnc(lr)
!      rsum = 0.0_wp
           do nr = lr, nrup
             nA_L: do n1 = maxhf+1, maxhf+nvary(lc)
               call dorkin (nr, lr, n1, lc, nr, lr, maxhf+1, lc, n1, 0, rval(1:n1-maxhf))
               nB_L: do n2 = maxhf+1, n1
!                call dorkin (nr, lr, n1, lc, nr, lr, n2, lc, n2, 0, rval(1))
                 store(n2-maxhf,n1-maxhf) = store(n2-maxhf,n1-maxhf) &
                    + rval(n2-maxhf)*REAL(4*lr-2,wp)
               end do nB_L
             end do nA_L
           end do
           if (lc <= lexch) then
           lamlo = ABS(lc - lr)
           lamup = lc + lr - 2
           lam_L: do lam = lamlo, lamup, 2
             do nr = lr, nrup
               rmeval = rme(lr-1, lc-1, lam)
               print *,'RMEVAL ',rmeval*rmeval/REAL(2*lc-1,wp)
               nC_L: do n1 = maxhf+1, maxhf+nvary(lc)
                   call dorkin (n1, lc, nr, lr, nr, lr, maxhf+1, lc, n1, lam, rval(1:n1-maxhf))
                 nD_L: do n2 = maxhf+1, n1
!                  call dorkin (n1, lc, nr, lr, nr, lr, n2, lc, n2, lam, rval(1))
                   store(n2-maxhf,n1-maxhf) = store(n2-maxhf,n1-maxhf) &
                    - rval(n2-maxhf)*rmeval*rmeval/REAL(2*lc-1,wp)
                 end do nD_L
               end do nC_L
             end do
          end do lam_L
          endif
       end do lr_L

!               if (bug4 > 2) write (fo,'(2x, 3i5, 3f16.8)') lc-1, &
!                    n1, n2, x, rint
!               x = x + rint
!            else
!               if (bug4 > 2) write(fo,'(2x, 3i5, 3f16.8)') lc-1, &
!                    n1, n2, x
       end if
!            call writa (jbuff4, x)
!            sumx=sumx+x
!            sumax=sumax+abs(x)
       n1_L: do n1 = 1, nvary(lc)
          n2_L: do n2 = 1, n1
          icount=icount+1
          call oneele(n1+maxhf, lc, n2+maxhf, lc, x)
          if (ncore > 0) then
            x=x+store(n2,n1)
          end if
          call writa (jbuff4, x)
          sumx=sumx+x
          sumax=sumax+abs(x)
          end do n2_L
       end do n1_L
       write (fo,'(/,a,i5,a,i6)') '# 1-electron CC integrals stored for &
            &cont. a.m.', lc-1, ' = ', icount
       print *,' Gen1cc checksum ',sumx,sumax
       deallocate(store,stat=err)
       if (err /= 0 ) then
         print *,'Deallocation problem in genbc'
         stop
       endif
    end do lc_L
  end subroutine gen1cc

  subroutine genbb (jbuff1, nrkbb, ibb, irkbb)
! generates and stores all the bound-bound rk integrals.
! following symmetry relations are used to reduce # integrals stored:
!     lri >= lrj
!     lsi >= lsj
!     lsi >= lri
!     lri+lrj+lsi+lsj = even integer

!     lam >= MAX(ABS(lri-lrj), ABS(lsi-lsj))
!     lam <= MIN(lri+lrj, lsi+lsj) - 2
!     if  lri = lrj  then  nri <= nrj
!     if  lsi = lsj  then  nsi <= nsj
    use filehand, only: catlog, writa
    use bassplnew_orb, only: dorkin
    integer, intent(out)      :: irkbb(:,:,:)
    integer, intent(out)      :: nrkbb
    integer, intent(out)      :: ibb
    integer, intent(in)       :: jbuff1
    real(wp)                  :: rkval(1)
    integer                   :: jkup, lsi, mxcsi, lsj, mxcsj, mxhfsj
    integer                   :: nijz, lri, lrj, nrj, nri, lam, lmin, lmax
    integer                   :: nsiup, nsi, nsj, nijr, nriup
    integer                   :: mxcri, mxcrj, nijs, mxhfrj, llo
    real(wp)                  :: xtotal, atotal

    xtotal = 0.0_wp
    atotal = 0.0_wp
    nrkbb = 0
    irkbb = -999
    call catlog (jbuff1, ibb)
    if (bug6 >= 1) then
       write (fo,'(38("-"))')
       write (fo,'(a,/,a)') '    lsi  lsj  lri  lrj  lam  nrj  &
            &nri  nsj  nsi  rkbb', '    ---  ---  ---  ---  -&
            &--  ---  ---  ---  ---  ----'
    end if

    lsi_L: do lsi = 1, lrang1
       mxcsi = maxnc(lsi)
       if (mxcsi == maxnhf(lsi)) cycle
       lsj_L: do lsj = 1, lsi
          mxcsj = maxnc(lsj)
          mxhfsj = maxnhf(lsj)
          if (mxcsj == mxhfsj) cycle
          nijs = (lsi * (lsi - 1)) / 2 + lsj
          lri_L: do lri = 1, lsi
             mxcri = maxnc(lri)
             if (mxcri == maxnhf(lri)) cycle
             llo = MOD(lsi+lsj+lri-3,2) + 1
             lrj_L: do lrj = llo, lri, 2
                mxcrj = maxnc(lrj)
                mxhfrj = maxnhf(lrj)
                if (mxcrj == mxhfrj) cycle
                nijr = (lri * (lri - 1)) / 2 + lrj
                lmin = MAX(ABS(lri-lrj), ABS(lsi-lsj))
                lmax = MIN(lri+lrj, lsi+lsj) - 2
                lam_L: do lam = lmin, lmax, 2
                   irkbb(lam+1,nijr,nijs)=nrkbb
                   nrj_L: do nrj = mxcrj+1, mxhfrj
                      if (lri == lrj) then
                         nriup = nrj
                      else
                         nriup = maxnhf(lri)
                      end if
                      nri_L: do nri = mxcri+1, nriup
                         nsj_L: do nsj = mxcsj+1, mxhfsj
                            if (lsi == lsj) then
                               nsiup = nsj
                            else
                               nsiup = maxnhf(lsi)
                            end if
                            nsi_L: do nsi = mxcsi+1, nsiup
                               nrkbb = nrkbb + 1
!
! REQUIRES INVERSION
!
                               call dorkin (nri, lri, nsi, lsi, nrj, lrj,&
                                    nsj, lsj, nsj, lam, rkval(1))
                               call writa (jbuff1, rkval(1))
                               xtotal = xtotal + rkval(1)
                               atotal = atotal + ABS(rkval(1))
                               if (bug6 >= 1) write (fo, &
                                    '(2x,9i5,e16.8)') lsi-1, lsj-1,   &
                                    lri-1, lrj-1, lam, nrj, nri, nsj, &
                                    nsi, rkval(1)
                            end do nsi_L
                         end do nsj_L
                      end do nri_L
                   end do nrj_L
                end do lam_L
             end do lrj_L
          end do lri_L
       end do lsj_L
    end do lsi_L
    write (fo,'(/,a,i7)') '# 2-electron BB integrals stored = ', nrkbb
    write (fo,'(a,2e30.15)') 'genbb: ', xtotal, atotal
  end subroutine genbb

  subroutine genbc (minim, jbuff1, lcup, ibc, irkbc)
! generates and stores all the bound-continuum rk integrals.
! symmetry relations used to reduce # integrals stored:
!     lc-1 is the continuum electron angular momentum
!     lc >= minim
!     lc <= MIN(3*lrang1-2,lrang2)

!     lri >= lrj
!     lri+lrj+lsi+lc = even integer

!     lam >= MAX(ABS(lri-lrj), ABS(lc-lsi))
!     lam <= MIN(lri+lrj, lc+lsi) - 2
!     if  lri = lrj  then  nri <= nrj
    use filehand, only: catlog, writa
    use bassplnew_orb, only: dorkin
    integer, intent(in)       :: minim
    integer, intent(in)       :: jbuff1
    integer, intent(out)      :: ibc(lrang2+1)
    integer, intent(out)      :: lcup
    integer, intent(out)      :: irkbc(:,:,:)
    integer                   :: kup, lc, mxhf, lsi, lri, ncsl
    integer                   :: ncup, maxcri, llo, lrj, mxcrj
    integer                   :: mxhfrj, nijr, lmin, lmax, nrj, nriup
    integer                   :: nc, nrkbc, mxhfsi, mxcsi, ncsi, mxcri
    integer                   :: nri, nsi, lam
    real(wp)                  :: rkval(1)
    real(wp)                  :: xtotal, atotal

    xtotal = 0.0_wp
    atotal = 0.0_wp
    lcup = MIN(3*lrang1-2, lrang2)
    kup = lrang1 * lcup
    irkbc(:,:,:kup) = -999
    lc_L: do lc = minim, lcup
       if (bug6 > 1) then
          write (fo,'(38(" "))')
          write (fo,'(a,/,a)') '     lc  lsi  lri  lrj&
               &  lam  nrj  nri  nsi   nc  rkbc', '     --  ---  ---  --&
               &-  ---  ---  ---  ---   --  ----'
       end if
       call skiper (lc, 0, 0, 0)
       mxhf = maxnhf(lc)
       ncup = nvary(lc)
       nrkbc = 0
       ncsl = (lc - 1) * lrang1
       ibc(lc) = -999
       call catlog (jbuff1, ibc(lc))
       lsi_L: do lsi = 1, lrang1
          mxcsi = maxnc(lsi)
          mxhfsi = maxnhf(lsi)
          if (mxcsi == mxhfsi) cycle
          ncsi = ncsl + lsi
          lri_L: do lri = 1, lrang1
             mxcri = maxnc(lri)
             if (mxcri == maxnhf(lri)) cycle
             llo = MOD(lc+lsi+lri-3,2) + 1
             lrj_L: do lrj = llo, lri, 2
                mxcrj = maxnc(lrj)
                mxhfrj = maxnhf(lrj)
                if (mxcrj == mxhfrj) cycle
                nijr = (lri * (lri - 1)) / 2 + lrj
                lmin = MAX(ABS(lri-lrj), ABS(lc-lsi))
                lmax = MIN(lri+lrj, lc+lsi) - 2
                lam_L: do lam = lmin, lmax, 2
                   irkbc(lam+1,nijr,ncsi) = nrkbc
                   nrj_L: do nrj = mxcrj+1, mxhfrj
                      if (lri == lrj) then
                         nriup = nrj
                      else
                         nriup = maxnhf(lri)
                      end if
                      nri_L: do nri = mxcri+1, nriup
                         nsi_L: do nsi = mxcsi+1, mxhfsi
                            nc_L: do nc = 1, ncup
                               nrkbc = nrkbc + 1
                               call dorkin(nri, lri, nsi, lsi, nrj, lrj, &
                                    mxhf+nc, lc, mxhf+nc, lam, rkval(1))
 !print *,'BC DATA ',nri,lri,nsi,lsi,nrj,lrj,mxhf+nc,lc,lam,rkval
                               call writa (jbuff1, rkval(1))
                               xtotal = xtotal + rkval(1)
                               atotal = atotal + ABS(rkval(1))
                               if (bug6 > 1) write (fo, &
                                    '(2x,9i5,e16.8)') lc-1, lsi-1,    &
                                    lri-1, lrj-1, lam, nrj, nri, nsi, &
                                    nc, rkval(1)
                            end do nc_L
                         end do nsi_L
                      end do nri_L
                   end do nrj_L
                end do lam_L
             end do lrj_L
          end do lri_L
       end do lsi_L
       write (fo,'(a,i5,a,i7)') '# 2-electron BC integrals stored for &
            &cont. a.m. ', lc-1, ' = ', nrkbc
    end do lc_L
    call catlog (jbuff1, ibc(lcup+1))
    write (fo,'(a,2e30.15)') 'genbc: ', xtotal, atotal
  end subroutine genbc

  subroutine gencc (l, lp, lexch, llpdim, jbuff1, jbuff4, ictccd, &
       iccd, nrkd, ictcce, icce, nrke)
! generates and stores all the continuum-continuum rk integrals
! with continuum angular momenta l and lp
    use filehand, only: catlog, writa
    use bassplnew_orb, only: dorkin
    integer, intent(in)   :: l, lp
    integer, intent(in)   :: lexch, llpdim
    integer, intent(in)   :: jbuff1, jbuff4
    integer, intent(out)  :: iccd, icce
    integer, intent(out)  :: nrkd, nrke
    integer, intent(out)  :: ictccd(:,:,:)
    integer, intent(out)  :: ictcce(:,:,:)
    integer               :: i1, i2, maxhf, maxhfp, lamup, maxc1, maxhf1
    integer               :: llo, l1pup, l1p, maxc2, maxhf2, n1, npup
    integer               :: ir, np, n, lameup, l1plo, klam, n1pup, n1p
    integer               :: l1, lam, imeshst, icomp
    real(wp)              :: rkval(2000)
    real(wp)              :: xtotal, atotal

    xtotal = 0.0_wp
    atotal = 0.0_wp
    nrkd = 0
    nrke = 0
    i1 = MIN(2*lrang1-1, l+lp+1)
    i2 = 0
    if (lp < lexch) i2 = (MIN(lrang1+l, lrang1+lp)) / 2
    ictccd(:,:,:i1) = -999
    ictcce(:,:,:i2) = -999
    maxhf = maxnhf(l+1)
    maxhfp = maxnhf(lp+1)

! direct integrals:
  
    if (bug6 > 2) then
       write (fo,'(a)') 'Direct Integrals:'
       write (fo,'(a)') '    lam   l1  l1p   n1  n1p    n   np  &
            &rkval'
    end if

! record starting position in iccd
    call catlog (jbuff1, iccd)

    lamup = MIN(l+lp, lrang1+lrang1-2, llpdim-1)
    lam_L: do lam = ABS(l-lp), lamup, 2
       l1_L: do l1 = 0, lrang1-1
          maxc1 = maxnc(l1+1)
          maxhf1 = maxnhf(l1+1)
          if (maxc1 == maxhf1) cycle
          llo = ABS(lam-l1)
          if (llo < l1) llo = 2 * ((l1-llo+1)/2) + llo
          l1pup = MIN(lrang1-1, lam+l1)
          l1p_L: do l1p = llo, l1pup, 2
             print *,'L,LP ',l1,l1p
             maxc2 = maxnc(l1p+1)
             maxhf2 = maxnhf(l1p+1)
             if (maxc2 == maxhf2) cycle
             print *,'Start storing'
             call catlog (jbuff1, ictccd(l1+1, l1p+1, lam+1))
             n1_L: do n1 = maxc1+1, maxhf1
                if (l1p == l1) then
                   n1pup = n1
                else
                   n1pup = maxhf2
                end if
                n1p_L: do n1p = maxc2+1, n1pup
                   ir = 0
                   n_L: do n = 1, nvary(l+1)
                      if (l == lp) then
                         npup = n
                      else
                         npup = nvary(lp+1)
                      end if
                      call dorkin (n1, l1+1, maxhf+n, l+1, n1p, l1p+1,&
                              maxhfp+1, lp+1, maxhfp+npup, lam, rkval(1:npup))
                      np_L: do np = 1, npup
                         ir = ir + 1
!                        call dorkin (n1, l1+1, maxhf+n, l+1, n1p, l1p+1,&
!                             maxhfp+np, lp+1, lam, rkval)
!                        if (l == lp) then
!                          if (lp==9) then
!                            print *,n1,l1,n,n1p,l1p,np,lam,rkval
!                            print *, maxhf,maxhfp
!                          endif
!                        endif
                         call writa (jbuff1, rkval(np))
                         xtotal = xtotal + rkval(np)
                         atotal = atotal + ABS(rkval(np))
                         if (bug6 > 2) write (fo,'(2x, 7i5, f16.8)') &
                              lam, l1, l1p, n1, n1p, n, np, rkval(np)
                      end do np_L
                   end do n_L
                   nrkd = nrkd + ir
                end do n1p_L
             end do n1_L
          end do l1p_L
       end do l1_L
    end do lam_L

! exchange integrals:
    if (lp >= lexch) return

! record starting position in icce
    call catlog (jbuff4, icce)
    lameup = MIN(lp, l) + lrang1 - 1
    lame_L: do lam = 0, lameup
       l1e_L: do l1 = ABS(lp-lam), MIN(lrang1-1,lp+lam), 2
          maxc1 = maxnc(l1+1)
          maxhf1 = maxnhf(l1+1)
          if (maxc1 == maxhf1) cycle
          if (l == lp) then
             l1plo = MAX(l1, ABS(l-lam))
          else
             l1plo = ABS(l-lam)
          end if
          l1pe_L: do l1p = l1plo, MIN(lrang1-1,l+lam), 2
             maxc2 = maxnc(l1p+1)
             maxhf2 = maxnhf(l1p+1)
             if (maxc2 == maxhf2) cycle
             klam = lam / 2
             call catlog (jbuff4, ictcce(l1+1,l1p+1,klam+1))
             if (l == lp) then
                ictcce(l1p+1,l1+1,klam+1) = ictcce(l1+1,l1p+1,klam+1)
             end if
             n1e_L: do n1 = maxc1+1, maxhf1
                if (l == lp .and. l1 == l1p) then
                   n1pup = n1
                else
                   n1pup = maxhf2
                end if
                n1pe_L: do n1p = maxc2+1, n1pup
                   ir = 0
                   ne_L: do n = 1, nvary(l+1)
                      if (l == lp .and. l1 == l1p .and. n1 == n1p) then
                         npup = n
                      else
                         npup = nvary(lp+1)
                      end if
                        call dorkin (maxhf+n, l+1, n1, l1+1, n1p, l1p+1,&
                              maxhfp+1, lp+1, maxhfp+npup, lam, rkval(1:npup))
                      npe_L: do np = 1, npup
                         ir = ir + 1
!                        call dorkin (maxhf+n, l+1, n1, l1+1, n1p, l1p+1,&
!                             maxhfp+np, lp+1, lam, rkval)
                         call writa (jbuff4, rkval(np))
                         xtotal = xtotal + rkval(np)
                         atotal = atotal + ABS(rkval(np))
                         if (bug6 > 3) write (fo,'(2x, 7i5, f16.8)')&
                              lam, l1, l1p, n1, n1p, n, np, rkval(np)
                      end do npe_L
                   end do ne_L
                   nrke = nrke + ir
                end do n1pe_L
             end do n1e_L
          end do l1pe_L
       end do l1e_L
    end do lame_L
    write (fo,'(a,2e30.15)') 'gencc: ', xtotal, atotal
  end subroutine gencc

  subroutine genmbc (minim, maxlc, mpol, jbuff1)
! generates the bound-continuum multipole integrals
    use filehand, only: catlog, writb
    integer, intent(in)          :: mpol
    integer, intent(in)          :: minim
    integer, intent(in)          :: maxlc
    integer, intent(in)          :: jbuff1
    integer                      :: l1up, l1lo, l2up, l1, l2, n1, n2
    integer                      :: maxhf1, maxc1, maxhf2
    real(wp)                     :: x(1)

    if (bug3 > 1) then
       write (fo,'(38("-"))')
       write (fo,'(a,/,a)') '   mpol   l1   l2   n1   n2   rval',&
            '   ----   --   --   --   --   ----'
    end if
    ibcpol = -999

! store starting position in ibcpol(1,1)
    call catlog (jbuff1, ibcpol(1,1))
    nbcpol = 0
    l2up = MIN(lrang2, lrang1+mpol)
    l2_L: do l2 = minim, l2up
       maxhf2 = maxnhf(l2)
       call skiper (l2,0,0,0)
       if (mpol == 1) call skiper (l2,0,0,0,2) ! read derivatives
       l1lo = ABS(l2-1-mpol) + 1
       l1up = MIN(lrang1, l2+mpol)
       l1_L: do l1 = l1lo, l1up, 2
          maxc1 = maxnc(l1)
          maxhf1 = maxnhf(l1)
          if (maxc1 == maxhf1) cycle
          call catlog (jbuff1, ibcpol(l1,l2))
          n1_L: do n1 = maxc1+1, maxhf1
             n2_L: do n2 = 1, nvary(l2)
                call radint (n1, l1, maxhf2+n2, l2, mpol, x(1))
                call writb (jbuff1, x, 1, 1)
                nbcpol = nbcpol + 1
                if (bug3 > 1) write (fo,'(a,2x,5i5,e16.8)') 'length', mpol, &
                     l1-1, l2-1, n1, n2, x
             end do n2_L
             if (mpol == 1) then
                n2d_L: do n2 = 1, nvary(l2)
                   call derint (n1, l1, maxhf2+n2, l2, x(1))
                   call writb (jbuff1, x, 1, 1)
                   if (bug3 > 1) write (fo,'(a,2x,5i5,16x,e16.8)') &
                        'vel', mpol, l1-1, l2-1, n1, n2, x
                   nbcpol = nbcpol + 1
                end do n2d_L
             end if
          end do n1_L
       end do l1_L
    end do l2_L
    write (fo,'(a,i6)') 'Total # bound-continuum multipole &
         &integrals stored      = ', nbcpol
  end subroutine genmbc

  subroutine derint (n11, l11, n12, l12, result)
! radial integral of the gradient operator between two orbitals 
! specified by the quantum numbers (n11,l11-1) and (n12,l12-1)
    use bound_basis, only: uj, ipos, d1uj
    use radial_grid, only: npts, wt, xr
    integer, intent(in)     :: n11, l11
    integer, intent(in)     :: n12, l12
    real(wp), intent(out)   :: result
    integer                 :: k1, k2, i

    k1 = ipos(n11,l11)
    k2 = ipos(n12,l12)
    result = 0.0_wp
    if (l12 <= l11) then
       do i = 2, npts
          result = result + wt(i) * uj(i,k1) * (d1uj(i,k2) - &
               uj(i,k2) * REAL(l11-1,wp) / xr(i))
       end do
    else
       do i = 2, npts
          result = result + wt(i) * uj(i,k1) * (d1uj(i,k2) + &
               uj(i,k2) * REAL(l12-1,wp) / xr(i))
       end do
    end if
  end subroutine derint

  subroutine genmcc (minim, mpol, jbuff1)
! generates the continuum-continuum multipole integrals
    use bassplnew_orb, only: ends
    use filehand, only: catlog, writb
    integer, intent(in)          :: mpol
    integer, intent(in)          :: minim
    integer, intent(in)          :: jbuff1
    integer                      :: n1, n2, l1, l2, maxhf1, l2lo, l2up
    integer                      :: n2up, maxhf2
    real(wp)                     :: x(1)

    if (bug3 > 2) then
       write (fo,'(38("-"))')
       write (fo,'(a,/,a)') '   mpol   l1   l2   n1   n2  rval', &
            '   ----   --   --   --   --  ----'
    end if

    iccpol(1:lrang2,1:lrang2) = - 999
! store starting position in iccpol(1,1)
    call catlog (jbuff1, iccpol(1,1))
    nccpol = 0
    l1_L: do l1 = minim, lrang2
       maxhf1 = maxnhf(l1)
       l2lo = MAX(ABS(l1-1-mpol), l1-1) + 1
       if (MOD(l1+l2lo+mpol,2) == 1) l2lo = l2lo + 1
       l2up = MIN(l1+mpol, lrang2)
       l2_L: do l2 = l2lo, l2up, 2
          call skiper (l1, l2, 0, 0)
          if (mpol == 1) call skiper (l1, l2, 0, 0, 2)  ! read derivatives
          maxhf2 = maxnhf(l2)
          call catlog (jbuff1, iccpol(l1,l2))
          iccpol(l2,l1) = iccpol(l1,l2)
          n1_L: do n1 = 1, nvary(l1)
             if (l1 == l2) then
                n2up = n1
             else
                n2up = nvary(l2)
             end if
             n2_L: do n2 = 1, n2up
                call radint (n1+maxhf1, l1, n2+maxhf2, l2, mpol, x(1))
                call writb (jbuff1, x, 1, 1)
                nccpol = nccpol + 1
                if (bug3 > 2) write (fo,'(a,2x,5i5,2e16.8)') 'length', mpol, &
                     l1-1, l2-1, n1, n2, x
             end do n2_L
          end do n1_L

          if (mpol == 1) then  ! derivative integrals
             n1d_L: do n1 = 1, nvary(l1)
                if (l1 == l2) then
                   n2up = n1
                else
                   n2up = nvary(l2)
                end if
                n2d_L: do n2 = 1, n2up
                   call derint (n1+maxhf1, l1, n2+maxhf2, l2, x(1))
! add Bloch term:
                   x = x - 0.5_wp * ends(n1,l1) * ends(n2,l2)
                   call writb (jbuff1, x, 1, 1)
                   nccpol = nccpol + 1
                   if (bug3 > 2) write (fo,'(a,2x,5i5,16x,e16.8)') &
                        'vel', mpol, l1-1, l2-1, n1, n2, x
                end do n2d_L
             end do n1d_L
          end if
       end do l2_L
    end do l1_L
    write (fo,'(a,i6)') 'Total # continuum-continuum &
         &multipole integrals stored = ', nccpol
  end subroutine genmcc
end module radial_integrals
