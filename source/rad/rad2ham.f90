module rad2ham
! Write data files to be read be HAM
! Time-stamp: "2003-03-26 17:31:57 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug2
  implicit none

  private
  public writap

contains
  subroutine writap (encore)
! writes basic information onto stgrad permanent output file
    use rm_data, only: ra, buttle, bsto
    use rm_xdata, only: lrang1, lrang2, ncore, maxnc, nvary
    use file_info, only: itape3
    use bassplnew_orb, only: ends, dends
    use buttle_correct, only: get_buttle
    real(wp), intent(in)           :: encore
    real(wp), pointer              :: coeff(:,:)
    real(wp)                       :: ek2min, ek2max
    integer                        ::  n, l

    print *,'Started'
    rewind itape3
    write (itape3) ra, bsto, lrang1, lrang2, ncore, encore
    write (itape3) maxnc
    write (itape3) nvary
    write (itape3) ((ends(n,l),n=1,nvary(l)),l=1,lrang2)
    call get_buttle (ek2min, ek2max, coeff)
    print *,'Buttled'
    write (itape3) coeff(1:3,1:lrang2)
    print *,'Reached'
!
! we also need to store the values of the derivatives at the boundary
!
    open(unit=38,file='RADdends',form='unformatted')
    write(38)  ((dends(n,l),n=1,nvary(l)),l=1,lrang2)
    close(38)

! echo numbers written to rad3
    if (bug2 > 0) then
       write (fo,'(a)') 'Subroutine writap'
       write (fo,'(a)') 'Data written to file RAD3:'
       write (fo,'(a)') 'ra, bsto, lrang1, lrang2, ncore, encore'
       write (fo,'(2f16.9,3i5,f16.9)') ra, bsto, lrang1, lrang2, &
            ncore, encore
       write (fo,'(a)') 'maxnc, then nvary'
       write (fo,'(10i5)') maxnc
       write (fo,'(10i5)') nvary
       write (fo,'(a)') 'ends(n,l),n=1,nvary(l)),l=1,lrang2'  
       write (fo,'(6f16.9)') ((ends(n,l),n=1,nvary(l)),l=1,lrang2)
       write (fo,'(a)') 'dends(n,l),n=1,nvary(l)),l=1,lrang2'  
       write (fo,'(6f16.9)') ((dends(n,l),n=1,nvary(l)),l=1,lrang2)
    end if
    print *,'Reached'
  end subroutine writap
end module rad2ham



