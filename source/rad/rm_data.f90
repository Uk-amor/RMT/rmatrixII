module rm_data
! Input data common to all stages of R-matrix program
! Time-stamp: "2004-11-18 11:51:46 cjn"
  use precisn, only: wp
  use io_units, only: fo, fi
  use debug, only: set_debug_switches
  implicit none

  character(len=80) :: coment ! run descriptor
  integer, save     :: nelc   ! # electrons in system
  integer, save     :: nz     ! nuclear charge
  integer, save     :: lrgle1, lrgle2 ! (N+1) orb am limits, w exchge
  integer, save     :: lrgld1, lrgld2 ! (N+1) orb am limits, no exchge

  integer, save     :: ltop
  integer, save     :: lstep
  integer, save     :: lrgp    ! Total (N+1) parity, -1 if both
  integer, save     :: lrgslo, lrgsup
  integer, save     :: maxor2
  integer, save     :: lrangr
  integer, save     :: lpmax
  integer, save     :: neprnt
  integer, save     :: nexp
  integer, save     :: inorb   ! # orbitals to be read in directly

  integer, save     :: maxe    ! maximum energy (Ryd) used in SS
  logical, save     :: buttle  ! Buttle correction switch
  integer, save     :: lfixn   ! L-value, nrang2 allowed to decrease
  integer, save     :: nset    ! # N-electron symmetries
  integer, save     :: maxorb  ! # orbitals
  integer, save     :: ncfg    ! # N-electron configurations
  real(wp), save    :: ra      ! internal region radius in a.u.
  real(wp), save    :: bsto    ! Bloch parameter
  integer, save     :: ndiag   ! =1 if tgt Hams to be diagonalized
  integer, save     :: icon    ! selects cfg input method
  integer, save     :: ibop    ! Slater/Superstructure orbitals
  integer, save     :: nrang2  ! # cont orbitals for each a.m.
  real(wp), save    :: ek2max  ! max energy for Buttle fit
  integer, save     :: lamax   ! max # multipole integrals
  integer, save     :: nix     ! # subintrvals in 0 < r <= RA
  integer, save     :: npot    ! choice of cont. orb potl
  integer, save     :: mpol    ! 0 for L dipoles, 1 for L,V dipoles
  integer, save     :: nocore  ! = 0 find core orbs, else none
  integer, save     :: irbug(9) ! rad diagnostic switches
  integer, save     :: iabug(9) ! ang diagnostic switches
  integer, save     :: ihbug(9) ! ham diagnostic switches
  logical, save     :: restart  ! restart flag
  integer, save     :: diag    ! diagonalization option switch
  logical, save     :: tgt_mmts ! target moment output
  logical, save     :: asym_ptlcfs ! asymptotic potl coefficients
  integer, save     :: prm      ! PRMAT threshold
  logical, save     :: xdr      ! XDR output files for PRMAT
  logical, save     :: farm     ! FARM/Opacity HAM output files
  integer, save     :: ncfgp    ! # N+1 electron configurations
  integer, save     :: sbk    ! surcof block size
  logical, save     :: reorth ! switch to reorthogonalise orbitals

  private
  public rd_nmlst

! limit public variables to those used in RAD:
  public nelc, nz, coment, lrgle1, lrgle2, lrgld1, lrgld2,        &
       nset, maxorb, inorb, ncfg, ra, bsto, icon, ibop, nrang2,   &
       ek2max, lamax, nix, npot, lfixn, nocore, mpol, buttle,     &
       maxe, ncfgp, reorth

contains
  subroutine rd_nmlst
! read in generic RMATRXII namelist data

    integer           :: status, lrgllo, lrglup

    namelist /input/ nelc, nz, coment, lrgle1, lrgle2,               &
         lrgld1, lrgld2, ltop, lstep, lrgslo, lrgsup, lrgp, lpmax,   &
         nset, maxorb, maxor2, inorb, lrangr, ncfg, ra, bsto, buttle,&
         ndiag, icon, ibop, nrang2, ek2max, lfixn, lamax, nix, ncfgp,&
         nexp, neprnt, npot, iabug, irbug, ihbug, nocore, mpol, sbk, &
         restart, diag, prm, xdr, asym_ptlcfs, tgt_mmts, maxe, farm, &
         reorth

! initialise data
    coment = repeat(' ',80)
    lrgslo = -1;  lrgsup = 0;  lrgp = -1
    ndiag = 1
    inorb = 0;    icon = 1;    nix = -1
    nexp = 0;     neprnt = -1
    lfixn = 0 ! chosen so that lndef in rm_rd defines lfixn by default
    nocore = 0

    npot = 0;     ibop = 0   ! default to normal Slater basis

    mpol = 0;    nelc = -1
    nz = -1;      ra = - 1.0_wp;           bsto = 0.0_wp
    nrang2 = 0;   lamax = 4;
    ltop = -1;    lstep = 1
    ihbug = 0
    buttle = .true.
    restart = .false.
    diag = 4
    prm = -1
    xdr = .false.
    asym_ptlcfs = .false.
    tgt_mmts = .true.
    farm = .false.
    ncfgp = -1
    sbk = 1000
    reorth = .false.

!   rewind (fi)
    read (fi,input, iostat=status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'rd_nmlst: namelist input read error = ', &
            status
       stop
    end if

    call set_debug_switches (irbug)
    if (ltop < lrgld1 .or. ltop > lrgld2) ltop = lrgld2

! check values of nelc,nz
    if (nelc < 0) then
       write (fo,'(a)') 'value of nelc invalid, must be >= 0.'
       stop
    end if
    if (nz <= 0) then
       write (fo,'(a)') 'value of nz invalid, must be > 0.'
       stop
    end if
    if (mpol < 0 .or. mpol > 1) then
       write (fo,'(a)') 'incorrect value of mpol'
       stop
    end if
  end subroutine rd_nmlst

end module rm_data

