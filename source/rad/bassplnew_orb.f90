module bassplnew_orb
! Orthonormal orbitals satisfying model problem
!    B-spline based, 
!
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug5, bug7
  use error_prt, only: alloc_error
  use math, only: tredi, tqli, interv, bsplvb, lubksd, newmat, sort, gauleg
  use bound_basis, only : c, ze, irad, corect, nco
  implicit none

  integer, save                :: shm
  real(wp), allocatable, save  :: b(:,:)
  real(wp), allocatable, save  :: ovrlap(:,:)
  real(wp), allocatable, save  :: temp(:)

  real(wp), allocatable, save  :: ends(:,:)
  real(wp), allocatable, save  :: dends(:,:)
  real(wp), allocatable, save  :: rkint(:,:,:,:,:)
  real(wp), allocatable, save  :: splvec(:,:,:)
  real(wp), allocatable, save  :: prp(:,:), vf(:,:), facto(:)
  real(wp), allocatable, save  :: sumacc(:)
  integer:: nbstore, kstore
  integer:: n1test, l1test, n3test, l3test, lamtest

  private
  public splorb
  public ends, dends, b, ovrlap
  public dorkin
  
contains

  subroutine splorb
  use radial_grid, only: xr, wt, npts, nptsorig
!  use basis_functions, only: orb, eigen, eigens
  use rm_data, only: ra, nelc, nrang2, nz, maxorb, ek2max
  use rm_xdata, only: nvary, maxnlg, maxnhf, lrang1, lrang2, minim
  use bound_basis, only: uj, duj, d1uj, ipos, p
!  use find_orbitals, only: finder
  use file_info, only: open_tmp_files, u1, u2, u3
  
  real(wp)				:: pi
  integer				:: lplo
  
  integer				:: ndim, nser, nt, nc, k
  real(wp)				:: rain

!      THIS IS NOW NZ
!      real(wp), intent(in):: z
      real(wp), allocatable, dimension(:):: t
      integer:: NB1, NB, NINT, NLEG, NWF, NQ, nn, lp, maxhf, n, nfun, kk, nqin
      integer:: i, error, icon, nc1, nc2, j, isplrem, kmax, kj, ndm, kmax2
      real(wp):: d1, al, summ
      real(wp), dimension(12):: ovrtst
      real(wp), allocatable, dimension(:):: gp, wtg
      real(wp), allocatable, dimension(:):: val
      real(wp), allocatable, dimension(:,:):: xg
      real(wp), allocatable, dimension(:,:,:):: bb, dd
      real(wp), allocatable, dimension(:,:):: sbb, spt, sdd, vec
      real(wp), allocatable, dimension(:,:):: sfit
      real(wp), allocatable, dimension(:,:):: derivs, derivsto
      
      real(wp), allocatable, dimension(:,:):: vabs
      real(wp), allocatable, dimension(:):: pabs
      
  write (fo, '(/,a, /)') 'Subroutine Splorb'
  
  call open_tmp_files
  
  call scoeff
  call set_ends

  lplo = minim
  n1test = -1
  l1test = -1
  n3test = -1
  l3test = -1
  lamtest = -1

  d1 = 1.0_wp
  k = 9
  write (fo, '(a, i3)') 'NRANG2 = ',nrang2
  ndim = nrang2
  nb = ndim+1
  kstore = k
  nbstore = nb
  nb1 = nb+2
  nc = nb-k+2
  nint = nc-1
  nt = nc+2*k-2
  nleg = k+1
  nser = lrang2+1
  nptsorig = npts
  open (unit = 97, file='Splinedata',form='unformatted')
  write (97) nc, k, nb, ndim, nt
  if (nint*nleg+1 <= npts) then
     npts = nint*nleg+1
     print *,'NPTS = ',npts, ' rather than ',nptsorig
    else
     print *,'Whoa Nelly, npts is too large'
     stop
  end if
  allocate(derivs(npts, nb-1), derivsto(nptsorig, nb-1), facto(0:k-1), stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR DERIVS COULD NOT BE ALLOCATED'
    stop
  endif 
  facto(0)=0.5_wp
  facto(1:k-1)=1.0_wp
!
! This needs adjustment
!
  allocate(sfit(nb, 5), sumacc(ndim), stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR SFIT COULD NOT BE ALLOCATED'
    stop
  endif
  sfit = 0._wp
!
! Determine the Gauss-Legendre weights
!
  allocate(gp(NLEG), WTG(nleg), stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR GP AND WT COULD NOT BE ALLOCATED'
    stop
  endif
  call gauleg(-D1, D1, GP, WTG, NLEG)
  do i = 1, nleg
    write (fo, *) i, gp(i), WTG(i)
  enddo
!
! Determine the grid
!
  allocate(t(nt), stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR NT COULD NOT BE ALLOCATED'
    stop
  endif
  rain = ra/8.0_wp
  nc1 = nc/3
  nc2 = nc+1-nc1
  write (fo, '(a, i3)') 'NC1 equals ',nc1
  write (fo, '(a, i3)') 'NC2 equals ',nc2
!  call tlxexp(nt, nc, k, rain, ra, t)
  call tlxexp(nt, nc, nc1, nc2, k, nint, nleg, ra, rain, t)
  do i = 1, nt
   write(fo, '(i3, a, f16.9)') i, ' ',t(i)
  end do
  write(97) (t(i), i = 1, nt)
!
! DEFINE THE SPLINE mesh
!
  allocate(xg(nint, nleg), pabs(nint*nleg+1), stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR XG COULD NOT BE ALLOCATED'
    stop
  endif
  call determine_xg(nt, nc, k, nint, nleg, t, xg, gp)
!
! REDEFINE THE RADIAL GRID (PROBLEM FOR GIVEN ORBS)
!
  xr(1)=0._wp
  wt(1)=0._wp
  pabs(1)=0._wp
  do i = 1, nint
   do j = 1, nleg
    xr((i-1)*nleg+j+1)=xg(i, j)
    wt((i-1)*nleg+j+1)=(t(i+k)-t(i+k-1))*wtg(j)/2._wp
    if (xg(i, j) < (ra-60._wp)) then
     pabs((i-1)*nleg+j+1)= 0._wp
    else
     pabs((i-1)*nleg+j+1)=(2.0_wp*cos((xg(i, j)-ra)/60._wp*acos(-1._wp))+2.0_wp)
    endif
   end do
  end do
!
! obtain the values for all splines
!
      allocate(bb(nint, nleg, nb), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR BB COULD NOT BE ALLOCATED'
       stop
      endif
      call makebb(nint, nleg, k, nb, nt, bb, xg, t)
!
! determine the overlap matrix
!
      allocate(sbb(nb, nb), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SBB COULD NOT BE ALLOCATED'
       stop
      endif
      CALL ovespl(nb, k, nint, nleg, nt, sbb, t, wtg, bb)
!
! Determine the 1/r12 matrix elements between B-splines
!
      kmax = lrang1+lrang2-1
      allocate(rkint(nb, 0:k-1, nb, 0:k-1, 0:kmax), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR RKINT COULD NOT BE ALLOCATED'
       stop
      endif
      rkint = 0.0_wp
!      kmax2 = kmax
      ! This option is effectively the same as in branch hugo_changes
      kmax2 = min(kmax, 30)
      CALL ERMBSP(nb, nint, k, nleg, nt, gp, wtg, bb, xg, t, kmax2)
!
! determine the second derivative values for each spline
!
      allocate(dd(nint, nleg, nb), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR DD COULD NOT BE ALLOCATED'
       stop
      endif
      call makedd(nint, nleg, k, nb, nt, dd, xg, t)
!
! determine the kinetic energy matrix
!
      allocate(sdd(nb, nb), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SDD COULD NOT BE ALLOCATED'
       stop
      endif
      CALL KINET(NB, K, NINT, NLEG, NT, SDD, T, WTG, BB, DD)
      deallocate(dd, stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR DD COULD NOT BE DEALLOCATED'
       stop
      endif
!
! allocate arrays for THE POTENTIAL ENERGIES and the diagonalization
!
      allocate(spt(nb, nb), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SPT COULD NOT BE ALLOCATED'
       stop
      endif    
      allocate(vec(ndim, ndim), vabs(ndim, ndim), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR VEC COULD NOT BE ALLOCATED'
       stop
      endif    
      allocate(splvec(ndim, ndim, lrang2), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SPLVEC COULD NOT BE ALLOCATED'
       stop
      endif    
      allocate(prp(0:k-1, ndim), vf(ndim, 0:k-1), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR PRP COULD NOT BE ALLOCATED'
       stop
      endif    
      allocate(val(ndim), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR VAL COULD NOT BE ALLOCATED'
       stop
      endif
      
      open(unit = 38, file='AbsBouPot.dat',form='unformatted')
      lplo = minim
      nq = 0
      write (fo, '(a)') 'Reached the loop somehow'
      lp_loop: do lp = lplo, lrang2
        isplrem = lp
!  	 if (isplrem > k-1) isplrem = k-1
        ! This option is effectively the same as in branch hugo_changes
	if (isplrem > k-1) isplrem = k
	ndm = nb-isplrem
	nn = 0
	maxhf = maxnhf(lp)
        print *,' lp MAXHF ',lp, maxhf
!
! Determine the potential energy matrix
!
	write(fo, '(a)') 'Enter potspl'
        CALL POTSPL(nb, k, nint, nleg, nt, spt, t, wtg, bb, xg, nz, nelc, lp)
	write(fo, '(a)') 'Return from potspl'
!	print *,sbb
!	print *,sdd
!	print *,spt
!	stop
!
! Only check for changes to continuum functions if needed
!
        if (lp <= lrang1) then
!         if (lp > 1000) then
!
! Determine the orbitals in terms of B-splines
!

  print *,'maxhf = ',maxhf
  nqin = nq+1
       CALL FUNREM(nb, k, nint, nleg, nt, sbb, t, wtg, bb, xg, sfit, lp, nz, &
        maxhf, nq, ra, isplrem) 
	print *,'return from funrem'
	print *,nb, ndim, lp, maxhf
	print *,sbb(nb, nb)
	print *,sdd(nb, nb)
	print *,spt(nb, nb)
	print *,vec(ndim, ndim)
	print *,val(ndim)
	print *,sfit(nb, 5)
!
! Find the continuum energies and wavefunctions
!
          CALL HYDADJ(NB, NDM, SBB, SDD, SPT, VEC, VAL, LP, SFIT, MAXHF, ISPLREM)
!
! Show eigenvalues
! (-1 normally) 
   nvary(lp)=ndm-maxhf+lp-1 
  else
!
!         Follow the normal approach
!
 	  write(fo, '(a)') 'Enter hydro'
          CALL HYDRO(NB, NDM, SBB, SDD, SPT, VEC, VAL, ISPLREM)
	  write(fo, '(a)') 'Return from hydro'
! (- number should not be there)	  
	  nvary(lp) = ndm   
  end if
!
! STORE THE SPLINE COEFFICIENTS
!
  nfun = nvary(lp)
  do i = 1, nfun
!  do n = 1, ndm
    if (vec(1, i).lt.0._wp) then
     vec(1:ndm, i)=-vec(1:ndm, i)
!    print *,'VEC ',i, n, vec(n, i)
! Check what happens if last spline gone
!   vec(ndm, i)=0._wp
    endif
!  enddo
  enddo
  print *,'Reached here'  
  print *,maxhf, nfun, nqin, nq
  if (isplrem > 1) then
   if (lp <= lrang1) then
    splvec(1:isplrem-1, 1:nq-nqin+1, lp)=0._wp
!  endif
    splvec(1:isplrem-1, nq-nqin+2:nq-nqin+1+nfun, lp)=0._wp
   else
    splvec(1:isplrem-1, 1:nfun, lp)=0._wp
   endif
  endif
  if (lp <= lrang1) then
   splvec(isplrem:ndim, 1:nq-nqin+1, lp)=sfit(isplrem+1:nb, lp:nq-nqin+lp)
 !endif
  splvec(isplrem:ndim, nq-nqin+2:nq-nqin+1+nfun, lp)= &
     vec(1:ndim-isplrem+1, 1:nfun)
  else
  splvec(isplrem:ndim, 1:nfun, lp)=vec(1:ndim-isplrem+1, 1:nfun)
  endif
  print *,'But here?'
!
! write the spline coefficients to disk
!
  do i = 1, nfun
   if (lp <= lrang1) then
    write (97) splvec(1:ndim, nq-nqin+i+1, lp)
   else
    write (97) splvec(1:ndim, i, lp)
   endif
  end do
!
!
!
  print *,'Spline coeffs'
  do i = 1, ndim
   print *,(t(i)+t(i+k))/2.d0, splvec(i, 1, lp), splvec(i, 2, lp), splvec(i, 3, lp)
  enddo
!  stop
!
!
!
  uj(1:nptsorig, maxorb+1:maxorb+nfun)=0._wp
! 
! Determine the wavefunction on the R-matrix grid
! 
  call det_wave(ndim, npts, nt, nc, k, nfun, xr, uj(1:npts, maxorb+1:maxorb+nfun), &
    vec, t, isplrem)
! do kk = 1, npts
!  print *,xr(kk), uj(kk, maxorb+2)
! enddo
  if (lp <= lrang1) then
   if (nq >= nqin) then
    call det_core(ndim, npts, nt, nc, k, nq-nqin+1, xr, uj(1:npts, nqin:nq), &
    sfit(:,lp:nq-nqin+lp), t, isplrem)
   end if
  end if
!if (lp <= lrang1) then
!print *,'Core wavefunctions'
!do n = nqin, nq
!do kk = 1, npts
! print *,n, xr(kk), uj(kk, n)
!enddo
!enddo
!print *,'Spline coefficients'
!do n = nqin, nq
! do kk = 1, nb
!  print *,kk, n, sfit(kk, n-nqin+1)
! enddo
!enddo
!endif

!  do kk = 1, nfun
!   print *,kk, maxval(abs(uj(1:npts, maxorb+kk)), 1), &
!               maxloc(abs(uj(1:npts, maxorb+kk)))
!  enddo
  write (u1, rec = lp) uj(1:nptsorig, maxorb+1:maxorb+nrang2)
  print *,'MAXORB/HF ', maxorb, maxhf
  do n = 1, nfun
    ends(n, lp) = vec(nb-isplrem, n)
    ipos(maxhf+n, lp)=maxorb+n
!    print *,n, ends(n, lp)
  end do
  print *,'Ends filled'
! 
! Determine the wavefunction' on the R-matrix grid
! 
  derivs = 0._wp
  derivsto = 0._wp
  call det_dwave(ndim, npts, nt, nc, k, nfun, xr, derivs, &
    vec, t, isplrem)
!
! IS THIS REALLY CORRECT? TOO MUCH STORAGE?
!
  derivsto(1:npts, 1:nb-1)=derivs
  write (u2, rec = lp) derivsto
  if (lp <= lrang1) then
   if (nq >= nqin) then
    call det_dcore(ndim, npts, nt, nc, k, nq-nqin+1, xr, d1uj(1:npts, nqin:nq), &
    sfit(:,lp:nq-nqin+lp), t, isplrem)
   end if
  end if
!  do kk = 1, npts
!    print *,xr(kk), d1uj(kk, nqin)
!  enddo
  print *,'Boundary derivs'
  print *,t(nt), t(nt-k)
  do n = 1, nfun
    dends(n, lp) = (vec(nb-isplrem, n)-vec(nb-isplrem-1, n)) * &
                   (k-1)/(t(nt)-t(nt-k))
!   print *,'DENDS ',n, lp, dends(n, lp)
  end do
  print *,'Dends filled'
! 
! Determine the wavefunction'' on the R-matrix grid
!
  derivs = 0._wp
  derivsto = 0._wp
  call det_ddwave(ndim, npts, nt, nc, k, nfun, xr, derivs, &
    vec, t, isplrem)
!  do kk = 1, npts
!    print *,xr(kk), derivs(kk, 2)
!  enddo
!  stop
  derivs(1, :)=-derivs(1, :)
  al = real(lp-1, wp)
    do kk = 1, nfun
      derivs(2:npts, kk)=-derivs(2:npts, kk)+(al*(al+1._wp)/(xr(2:npts)*xr(2:npts))-&
        2.0_wp*real(nz, wp)/xr(2:npts))*uj(2:npts, maxorb+kk)
    end do
 derivsto(1:npts, 1:nfun)=derivs(1:npts, 1:nfun)
  write (u3, rec = lp) derivsto
!
! And now for the core
!
  if (lp <= lrang1) then
   if (nq >= nqin) then
    call det_ddcore(ndim, npts, nt, nc, k, nq-nqin+1, xr, duj(1:npts, nqin:nq), &
    sfit(:,lp:nq-nqin+lp), t, isplrem)
    do kk = 1, npts
     print *,xr(kk), duj(kk, nqin)
    enddo
  duj(1, nqin:nq)=-duj(1, nqin:nq)
  do kk = nqin, nq
    duj(2:npts, kk)=-duj(2:npts, kk)+(al*(al+1._wp)/(xr(2:npts)*xr(2:npts))-&
        2.0_wp*real(nz, wp)/xr(2:npts))*uj(2:npts, kk)
    end do
  end if
  end if

  if (lp == 1) then
   do kk = 1, 4
    do kj = 1, kk
    summ = 0.5_wp*sum(derivs(2:npts, kj)*uj(2:npts, maxorb+kk)*wt(2:npts))
    print *,' Core-continuum check ',nq, kj, kk, summ
   enddo
   enddo
  endif
  
  print *,'diagonal'
  do kk = 1, nfun
    print *,kk, sum(derivs(2:npts, kk)*uj(2:npts, maxorb+kk)*wt(2:npts)), &
               ends(kk, lp), dends(kk, lp)
  enddo
  
  do i = 1, nvary(lp)
   do j = 1, nvary(lp)
    vabs(j, i)=sum(uj(1:npts, maxorb+i)*uj(1:npts, maxorb+j)*pabs*wt(1:npts))
   enddo
  enddo
  write(38) lp, nvary(lp)
  write(38) ((vabs(j, i), j = 1, nvary(lp)), i = 1, nvary(lp))
  if (lp <= lrang1) then
   print *,'Check abs'
   do i = nqin, nq
    do j = 1, nvary(lp)
     summ = sum(uj(1:npts, i)*uj(1:npts, maxorb+j)*pabs*wt(1:npts))
!     print *,i, j, summ
    enddo
   enddo
  endif 
  print *,'Check end'
! stop
! do i = 1, npts
!  print *,xr(i), uj(i, maxorb+1), uj(i, maxorb+2)
! end do
  print *,'Second derivs done'
!  do i = 1, nvary(lp)
!   do j = nqin, nq
!    ovrtst(j)=abs(SUM(uj(1:npts, maxorb+i) * uj(1:npts, j) * wt))
!   enddo
!   do j = 1, i
!    ovrtst(nq+1)=abs(SUM(uj(1:npts, maxorb+i) * uj(1:npts, maxorb+i) * wt)-1._wp)
!    print *,j, i, ovrlap(j, i)
!     write (fo, '(i3, 5f16.9)') i, ovrtst(nqin:nq+1)
!   end do
!  stop
!  end do
!  stop
  end do lp_loop
!  stop
!
! Some things need to be deallocated
!      
  close(unit = 38)
  close(unit = 97)
  deallocate(derivs, sfit, pabs, vabs, stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR D1 COULD NOT BE DEALLOCATED'
    stop
  endif        
  deallocate(val, vec, spt, sdd, sbb, stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR D2 COULD NOT BE DEALLOCATED'
    stop
  endif  
  deallocate(bb, xg, stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR D3 COULD NOT BE DEALLOCATED'
    stop
  endif
  deallocate(t, gp, wtg, stat = error)
  if (error /= 0) THEN
    print *,'SPACE FOR D4 COULD NOT BE DEALLOCATED'
    stop
  endif  
  print *,'ipos'
  do lp = minim, lrang2
  do i = 1, maxnhf(lp)+nrang2
   print *,i, lp, ipos(i, lp)
  enddo
  enddo
  end subroutine splorb
  
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the gauss-legendre integration points for each interval
!
   subroutine determine_xg(nt, nc, k, nint, nleg, t, xg, gp)
!      use constants
      implicit none
      integer, intent(in) 				:: nc, k, nt, nint, nleg
      real(wp), dimension(nleg), intent(in) 		:: gp
      real(wp), dimension(nt), intent(in)		:: t
      real(wp), dimension(nint, nleg), intent(out) 	:: xg
      integer:: i, j
      DO I = 1, NINT 
       DO J = 1, NLEG 
        XG(I, J)=(T(I+K)-T(I+K-1))/2._wp*GP(J)+&
	        (T(I+K)+T(I+K-1))/2._wp
       ENDDO
      ENDDO
  end subroutine determine_xg
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! obtain the values for all splines
!
      subroutine makebb(nint, nleg, k, nb, nt, bb, xg, t)
!      use constants 
      implicit none
      integer, intent(in):: nint, nleg, k, nb, nt
      real(wp), intent(in), dimension(nt):: t
      real(wp), intent(in), dimension(nint, nleg):: xg
      real(wp), intent(out), dimension(nint, nleg, nb):: bb
      real(wp), dimension(nb):: values
      integer:: error, left, leftmk, mflag, i, j
      real(wp):: xt
!
      DO I = 1, NINT 
       DO J = 1, NLEG 
        VALUES = 0._wp
        Xt = XG(I, J) 
! 
! LOCATE X WITH RESPECT TO KNOT ARRAY T. 
! 
        CALL INTERV(T, NB, Xt, LEFT, MFLAG) 
        LEFTMK = LEFT-K
        CALL BSPLVB(T, NT, K, 1, Xt, LEFT, VALUES(LEFTMK+1)) 
        BB(I, J, :)=VALUES(:)
       ENDDO
      ENDDO
!      do i = 1, NB
!       do j = 1, NINT
!        do leftmk = 1, NLEG
!         print *,xg(j, leftmk), bb(j, leftmk, i)
!	 end do
!	 end do
!	 end do
!	 stop 
!
      end subroutine makebb
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the overlap matrix
!
      subroutine oveSPL(nb, k, nint, nleg, nt, sbb, t, wtg, bb)
      implicit none
      integer, intent(in):: nb, k, nint, nleg, nt
      real(wp), intent(in), dimension(nt):: t
      real(wp), intent(in), dimension(nleg):: wtg
      real(wp), intent(in), dimension(nint, nleg, nb):: bb
      real(wp), intent(out), dimension(nb, nb):: sbb
      integer:: i, j, jb, ib
      real(wp):: sum
!
      SBB = 0._wp 
      DO IB = 1, NB 
       DO JB = 1, NB 
        IF(ABS(JB-IB).LT.K) THEN 
         SUM = 0._wp
          DO J = 1, NLEG 
           DO I = 1, NINT 
            SUM = SUM+((T(I+K)-T(I+K-1))/2._wp)*WTG(J)*BB(I, J, IB)*BB(I, J, JB) 
           ENDDO
          ENDDO 
          SBB(IB, JB)=SUM 
         ENDIF
        ENDDO
       ENDDO
      end subroutine ovespl
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the second derivatives of each spline
!
      subroutine makedd(nint, nleg, k, nb, nt, dd, xg, t) 
      implicit none 
      integer, intent(in):: nint, nleg, k, nb, nt
      real(wp), intent(in), dimension(nt):: t
      real(wp), intent(in), dimension(nint, nleg):: xg
      real(wp), intent(out), dimension(nint, nleg, nb):: dd
      real(wp), dimension(nb):: values
      integer:: error, left, leftmk, mflag, i, j, ii
      real(wp):: xt, d2v, aa1, aa2, aa3, aa4, aa5
!
      VALUES = 0._wp
      DO I = 1, NINT 
       DO J = 1, NLEG 
        Xt = XG(I, J) 
        CALL INTERV(T, NB, Xt, LEFT, MFLAG) 
        LEFTMK = LEFT-(K-2) 
        CALL BSPLVB(T, NT, K-2, 1, Xt, LEFT, VALUES(LEFTMK+1)) 
! 
        DO II = 1, NB 
         D2V = 0._wp
         AA1 = T(II+K-1)-T(II) 
         AA2 = T(II+K-2)-T(II) 
         AA3 = T(II+K-1)-T(II+1) 
         AA4 = T(II+K)-T(II+1) 
         AA5 = T(II+K)-T(II+2) 
         IF (AA1*AA2 .GT. 0._wp) D2V = 1._wp/AA1/AA2*VALUES(II) 
         IF ((AA1 .GT. 0._wp).AND.(AA3 .GT. 0._wp)) THEN
               D2V = D2V-1._wp/AA1/AA3*VALUES(II+1) 
         ENDIF
         IF ((AA3 .GT. 0._wp).AND.(AA4 .Gt. 0._wp)) THEN
               D2V = D2V-1._wp/AA3/AA4*VALUES(II+1) 
         ENDIF
         IF (AA4*AA5 .Gt. 0._wp) D2V = D2V+1._wp/AA4/AA5*VALUES(II+2) 
         D2V = FLOAT((K-1)*(K-2))*D2V
         DD(I, J, II)=D2V
        ENDDO
! 
! ZERO OUT THE VALUES JUST COMPUTED IN PREPARATION FOR 
! NEXT EVALUATION POINT. 
!
        VALUES = 0._wp
       ENDDO
      ENDDO 
!
  end subroutine makedd
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the kinetic energy matrix
!
      SUBROUTINE KINET(NB, K, NINT, NLEG, NT, SDD, T, WTG, BB, DD) 
      implicit none
      integer, intent(in):: nb, k, nint, nleg, nt
      real(wp), intent(in), dimension(nt):: t
      real(wp), intent(in), dimension(nleg):: WTG
      real(wp), intent(in), dimension(nint, nleg, nb):: bb, dd
      real(wp), intent(out), dimension(nb, nb):: sdd
      integer:: i, j, jb, ib
      real(wp):: sum
      SDD = 0._wp
      DO IB = 1, NB 
       DO JB = 1, NB 
        IF(ABS(JB-IB).LT.K) THEN 
         SUM = 0._wp
         DO J = 1, NLEG 
          DO I = 1, NINT 
           SUM = SUM+((T(I+K)-T(I+K-1))/2._wp)*WTG(J)*DD(I, J, IB)*BB(I, J, JB) 
          ENDDO
         ENDDO 
         SDD(IB, JB)=SUM
        ENDIF 
       ENDDO
      ENDDO  
      SDD(NB-1, NB)=SDD(NB-1, NB)+real(k-1)/(T(NT-K+1)-T(NT-K))
      SDD(NB, NB)=SDD(NB, NB)-real(k-1)/(T(NT-K+1)-T(NT-K)) 
  END subroutine kinet
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the potential energy matrix
!
      SUBROUTINE POTSPL(nb, k, nint, nleg, nt, spt, t, wtg, bb, xg, nz, nelc, lp) 
      implicit none
      integer, intent(in):: nb, k, nint, nleg, nt, nz, nelc, lp
      real(wp), intent(in), dimension(nt):: t
      real(wp), intent(in), dimension(nleg):: wtg
      real(wp), intent(in), dimension(nint, nleg, nb):: bb
      real(wp), intent(in), dimension(nint, nleg):: xg
      real(wp), intent(out), dimension(nb, nb):: spt
      integer:: i, j, jb, ib, error
      real(wp):: sum, al
      real(wp), dimension(nint, nleg):: potl
!
      al = float(lp-1)
      DO J = 1, NLEG
       DO I = 1, NINT
          POTL(I, J)=-NELC/xg(i, j)*exp(-nz**(1._wp/3._wp)*xg(i, j)) &
 	          -(nz-nelc)/xg(i, j)+al*(al+1)/(2._wp*xg(i, j)*xg(i, j))
!         POTL(I, J)=-(real(nz, wp))/xg(i, j)+al*(al+1)/(2._wp*xg(i, j)*xg(i, j))
       ENDDO
      ENDDO
      SPT = 0._wp
      DO IB = 1, NB
       DO JB = 1, NB
        SUM = 0._wp
        IF (ABS(JB-IB).LT.K) THEN 
         DO J = 1, NLEG 
          DO I = 1, NINT
           SUM = SUM+((T(I+K)-T(I+K-1))/2._wp)*WTG(J)*BB(I, J, IB)&
             *BB(I, J, JB)*POTL(I, J)
          ENDDO
         ENDDO 
        SPT(IB, JB)=SUM 
        ENDIF
       ENDDO
      ENDDO   
!
      end subroutine potspl
!
! Ensure overlap with bound basis is 0
!
      SUBROUTINE FUNREM(nb, k, nint, nleg, nt, sbb, t, wtg, bb, xg, sfit, lp, nz, &
        maxhf, nq, ra, isplrem) 
      implicit none
      integer, intent(in):: nb, k, nint, nleg, nt, lp, nz, maxhf, isplrem
      integer, intent(inout):: nq
      real(wp), intent(in), dimension(nt):: t
      real(wp), intent(in), dimension(nleg):: wtg
      real(wp), intent(in), dimension(nint, nleg, nb):: bb
      real(wp), intent(in), dimension(nint, nleg):: xg
      real(wp), intent(in):: ra
      real(wp), intent(in):: sbb(nb, nb)
      real(wp), intent(out):: sfit(nb, 5) 
      integer:: ipiv(nb-1)
      real(wp), allocatable, dimension(:,:):: scc
      real(wp), allocatable, dimension(:):: sft, worq, temvec
      integer:: icount, mm, ib, i, j, kk, info, error, mm1, nsub
      real(wp):: wave, sump, sfnorm
      real(wp):: r0, sigma, c1, alpha
!      real(wp) NNHF(NSER), NOTERM(5, NSER), PARM(10, 5, NSER, 3)
!      real(wp):: GAM(30)
!      LOGICAL, intent(in):: CLEM
!
      nsub = 2
      allocate(scc(nb-isplrem-nsub, nb-isplrem-nsub), sft(nb-isplrem), &
               worq(nb*nb), temvec(nb-isplrem), stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SFIT COULD NOT BE ALLOCATED'
       stop
      endif
! Parameter to set last nsub splines to 0 for bound orbitals
      temvec = 0._wp
!
! Determine the gamma function
!      
      icount = 0
!     call facadj(gam, 30)
!
      do mm = lp, maxhf
!       print *,mm
       nq = nq+1
       kk = nco(nq)
       if (ra < 50._wp) then
        call corect(kk, ra, c(1:kk, mm, lp), irad(1:kk, mm, lp), ze(1:kk, mm, lp), &
                  r0, sigma, c1)
        print *,' CORECT ',r0, sigma, c1
       else
        r0 = ra
        sigma = 1._wp
        c1 = 0._wp
       endif
       print *,kk
       do kk = 1, nco(nq)
         print *,kk, c(kk, mm, lp), irad(kk, mm, lp), ze(kk, mm, lp)
       enddo
       DO 390 IB = ISPLREM+1, NB 
        SUMP = 0.0_wp 
        DO 380 I = 1, NINT 
        DO 380 J = 1, NLEG
        WAVE = 0.0_wp
	do kk = 1, nco(nq)
!	 prefac = 1.d0
!         if (CLEM) then
!	  prefac = sqrt(gam(2*parm(kk, mm, icon, 1)+1)/
!     1          (2*parm(kk, mm, icon, 2))**(2*parm(kk, mm, icon, 1)+1))
!         endif
!
! END OF BOX CORRECTION TO BE ADDED
!
	 wave = wave+c(kk, mm, lp)*xg(i, j)**irad(kk, mm, lp)* &
              exp(-ze(kk, mm, lp)*xg(i, j))
        enddo
	kk = nco(nq)
!	call corect(kk, ra, c(1:kk, mm, lp), irad(1:kk, mm, lp), ze(1:kk, mm, lp), &
!                  r0, sigma, c1)
!        print *,r0, sigma, c1
        if (ra < 50._wp) then
         alpha=(xg(i, j)-r0)/sigma
         if (-alpha < 12.0_wp) wave = wave-c1*EXP(-alpha*alpha)  
        endif
!	if (ib .eq. 2) then
!	 print *,xg(i, j), wave
!	 wavsum = wavsum+((T(I+K)-T(I+K-1))/2.)*WTG(J)*wave**2
!	endif
        SUMP = SUMP+((T(I+K)-T(I+K-1))/2.)*WTG(J)*BB(I, J, IB)*WAVE 
  380   CONTINUE 
        SFT(IB-ISPLREM)=SUMP
!	print *,ib, sump 
  390   CONTINUE
!        print *,'Integral ',wavsum
        scc(1:nb-isplrem-nsub, 1:nb-isplrem-nsub)=sbb(isplrem+1:nb-nsub, isplrem+1:nb-nsub)
!
! Solve the linear system of equations
!
        CALL DSYTRF('L',NB-ISPLREM-nsub, SCC, NB-ISPLREM-nsub, IPIV, WORQ, nb*nb, info)
        CALL DSYTRS('L',NB-ISPLREM-nsub, 1, SCC, NB-ISPLREM-nsub, IPIV, sft, &
	            nb-isplrem, info)
        print *,sft(1:nb-isplrem)
        do i = 1, nsub
         sft(nb-isplrem-i+1)=0._wp
        enddo
!
! Orthogonalisation to be included
!
        temvec(1:nb-isplrem)=&
	   matmul(sbb(isplrem+1:nb, isplrem+1:nb), sft(1:nb-isplrem))
!       print *,temvec(1:nb-isplrem)
	do mm1 = lp, mm-1
	 sft = sft-dot_product(sfit(isplrem+1:nb, mm1), &
	     temvec(1:nb-isplrem))*sfit(isplrem+1:nb, mm1)
	enddo
        SFIT(1:isplrem, mm)=0._wp
	sfit(isplrem+1:nb, mm)=sft(1:nb-isplrem)
	print *,sft(1:nb-isplrem)
! Normalise
        worq(1:nb)=matmul(sbb, sfit(1:nb, mm))
	sfnorm = dot_product(sfit(1:nb, mm), worq(1:nb))
	print *,'Normalisation ',mm, maxhf, sfnorm
	sfit(1:nb, mm)=sfit(1:nb, mm)/sqrt(sfnorm)
      enddo
      deallocate(scc, stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SFIT 1 COULD NOT BE DEALLOCATED'
       stop
      endif
       deallocate(sft, stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SFIT 2 COULD NOT BE DEALLOCATED'
       stop
      endif
        deallocate(temvec, stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SFIT 4 COULD NOT BE DEALLOCATED'
       stop
      endif
      deallocate(worq, stat = error)
      if (error /= 0) THEN
       print *,'SPACE FOR SFIT 3 COULD NOT BE DEALLOCATED'
       stop
      endif
      print *,'end of funrem'
      return 
      end subroutine funrem
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the wavefunction from the spline coefficients
!
      subroutine det_wave(ndim, npts, nt, nc, k, nfun, &
          rad_grid, wave, vec, t, isplrem)
      implicit none
      integer, intent(in):: ndim, npts, nt, nc, k, nfun, isplrem
      real(wp), intent(in), dimension(npts):: rad_grid
      real(wp), intent(out), dimension(npts, nfun):: wave
      real(wp), intent(in), dimension(ndim, ndim):: vec
      real(wp), intent(in), dimension(nt):: t
      real(wp), dimension(nc+k-2):: values
      integer:: i, left, mflag, leftmk, jb, error, nb
      real(wp):: xt
      nb = nc+k-2
      wave = 0._wp
      print *,'Enter det_wave'
!$OMP PARALLEL PRIVATE(I, VALUES, XT, LEFT, MFLAG, LEFTMK, JB), &
!$OMP SHARED(NPTS, Rad_grid, T, NB, K, ISPLREM, WAVE, VEC, nfun)
!$OMP DO
      DO 88 I = 1, NPTS 
        VALUES = 0._wp
        XT = Rad_grid(I)
        CALL INTERV(T, NB, XT, LEFT, MFLAG) 
        LEFTMK = LEFT-K 
        CALL BSPLVB(T, NT, K, 1, Xt, LEFT, VALUES(LEFTMK+1))
        DO JB = ISPLREM+1, NB
          WAVE(I, 1:nfun)=WAVE(I, 1:nfun)+ &
	                 VALUES(JB)*VEC(JB-ISPLREM, 1:nfun)
        ENDDO
   88 CONTINUE
!$OMP END DO
!$OMP END PARALLEL
      print *,'Leave det_wave'
      end subroutine det_wave
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the wavefunction from the spline coefficients
!
      subroutine det_core(ndim, npts, nt, nc, k, nfun, &
          rad_grid, wave, vec, t, isplrem)
      implicit none
      integer, intent(in):: ndim, npts, nt, nc, k, nfun, isplrem
      real(wp), intent(in), dimension(npts):: rad_grid
      real(wp), intent(out), dimension(npts, nfun):: wave
      real(wp), intent(in), dimension(ndim+1, nfun):: vec
      real(wp), intent(in), dimension(nt):: t
      real(wp), dimension(nc+k-2):: values
      integer:: i, left, mflag, leftmk, jb, error, nb
      real(wp):: xt
      nb = nc+k-2
      wave = 0._wp
      print *,'Enter det_core'
      DO 88 I = 1, NPTS 
        VALUES = 0._wp
        XT = Rad_grid(I)
        CALL INTERV(T, NB, XT, LEFT, MFLAG) 
        LEFTMK = LEFT-K 
        CALL BSPLVB(T, NT, K, 1, Xt, LEFT, VALUES(LEFTMK+1))
        DO JB = ISPLREM+1, NB
          WAVE(I, 1:nfun)=WAVE(I, 1:nfun)+ &
	                 VALUES(JB)*VEC(JB, 1:nfun)
        ENDDO
   88 CONTINUE
      print *,'Leave det_core'
      end subroutine det_core
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the wavefunction from the spline coefficients
!
      subroutine det_dwave(ndim, npts, nt, nc, k, nfun, &
          rad_grid, wave, vec, t, isplrem)
      implicit none
      integer, intent(in):: ndim, npts, nt, nc, k, nfun, isplrem
      real(wp), intent(in), dimension(npts):: rad_grid
      real(wp), intent(out), dimension(npts, nfun):: wave
      real(wp), intent(in), dimension(ndim, ndim):: vec
      real(wp), intent(in), dimension(nt):: t
      real(wp), dimension(nc+k-1):: values
      integer:: i, left, mflag, leftmk, jb, error, nb
      real(wp):: xt
      nb = nc+k-2
      wave = 0._wp
!$OMP PARALLEL PRIVATE(I, VALUES, XT, LEFT, MFLAG, LEFTMK, JB) &
!$OMP SHARED(NPTS, Rad_grid, T, NB, NT, K, ISPLREM, WAVE, VEC, nfun)
!$OMP DO
      DO 88 I = 1, NPTS
        VALUES = 0._wp
        XT = Rad_grid(I)
        CALL INTERV(T, NB, XT, LEFT, MFLAG) 
        LEFTMK = LEFT-(K-1) 
        CALL BSPLVB(T, NT, K-1, 1, Xt, LEFT, VALUES(LEFTMK+1))
	wave(I, 1:nfun)=(k-1)*values(isplrem+1)*&
	                vec(1, 1:nfun)/(t(k+isplrem)-t(isplrem+1))
        DO JB = isplrem+2, NB
          WAVE(I, 1:nfun)=WAVE(I, 1:nfun)+ (k-1)*VALUES(JB)* &
	  (VEC(JB-isplrem, 1:nfun)-VEC(JB-(isplrem+1), 1:NFUN))/(T(JB+K-1)-t(JB))
        ENDDO
   88 CONTINUE
!$OMP END DO
!$OMP END PARALLEL
      end subroutine det_dwave
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the wavefunction from the spline coefficients
!
      subroutine det_dcore(ndim, npts, nt, nc, k, nfun, &
          rad_grid, wave, vec, t, isplrem)
      implicit none
      integer, intent(in):: ndim, npts, nt, nc, k, nfun, isplrem
      real(wp), intent(in), dimension(npts):: rad_grid
      real(wp), intent(out), dimension(npts, nfun):: wave
      real(wp), intent(in), dimension(ndim+1, nfun):: vec
      real(wp), intent(in), dimension(nt):: t
      real(wp), dimension(nc+k-1):: values
      integer:: i, left, mflag, leftmk, jb, error, nb
      real(wp):: xt
      nb = nc+k-2
      wave = 0._wp
      DO 88 I = 1, NPTS
        VALUES = 0._wp
        XT = Rad_grid(I)
        CALL INTERV(T, NB, XT, LEFT, MFLAG) 
        LEFTMK = LEFT-(K-1) 
        CALL BSPLVB(T, NT, K-1, 1, Xt, LEFT, VALUES(LEFTMK+1))
	wave(I, 1:nfun)=(k-1)*values(isplrem+1)*&
	                vec(isplrem, 1:nfun)/(t(k+isplrem)-t(isplrem+1))
        DO JB = isplrem+2, NB
          WAVE(I, 1:nfun)=WAVE(I, 1:nfun)+ (k-1)*VALUES(JB)* &
	  (VEC(JB, 1:nfun)-VEC(JB-1, 1:NFUN))/(T(JB+K-1)-t(JB))
        ENDDO
   88 CONTINUE
      end subroutine det_dcore
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the wavefunction from the spline coefficients
!
      subroutine det_ddwave(ndim, npts, nt, nc, k, nfun, &
          rad_grid, wave, vec, t, isplrem)
      implicit none
      integer, intent(in):: ndim, npts, nt, nc, k, nfun, isplrem
      real(wp), intent(in), dimension(npts):: rad_grid
      real(wp), intent(out), dimension(npts, nfun):: wave
      real(wp), intent(in), dimension(ndim, ndim):: vec
      real(wp), intent(in), dimension(nt):: t
      real(wp), dimension(nc+k-1):: values
      real(wp), dimension(nfun):: veca, vecb
      integer:: ioff, i, left, mflag, leftmk, jb, error, nb
      real(wp):: xt
      nb = nc+k-2
      wave = 0._wp
!$OMP PARALLEL PRIVATE(I, VALUES, XT, LEFT, MFLAG, LEFTMK, JB, veca, vecb), &
!$OMP SHARED(NPTS, T, NB, K, NT, ISPLREM, NFUN, VEC, WAVE)
!$OMP DO
      DO 88 I = 1, NPTS 
        VALUES = 0._wp
        XT = Rad_grid(I)
        CALL INTERV(T, NB, XT, LEFT, MFLAG) 
        LEFTMK = LEFT-(K-2) 
        CALL BSPLVB(T, NT, K-2, 1, Xt, LEFT, VALUES(LEFTMK+1))
        DO JB = isplrem+2, NB
!	print *,jb
	veca = 0._wp
	vecb = 0._wp
	if (t(jb+k-1) > t(jb)) then
!	  print *,'A ',jb
	  veca(1:nfun)=(vec(jb-isplrem, 1:nfun)-vec(jb-(isplrem+1), 1:nfun))&
	              /(t(jb+k-1)-t(jb))
	endif
	if (t(jb+k-2) > t(jb-1)) then
!	  print *,'B ',jb, t(jb+k-2), t(jb-1)
	  if (jb == isplrem+2) then
	    vecb(1:nfun)=(vec(jb-(isplrem+1), 1:nfun))&
	      /(t(jb+k-2)-t(jb-1))
	  else
  	    vecb(1:nfun)=(vec(jb-(isplrem+1), 1:nfun)&
	                  -vec(jb-(isplrem+2), 1:nfun))&
	                / (t(jb+k-2)-t(jb-1))
	  endif
	endif
	if (t(jb+k-2) > t(jb)) then	
!	  print *,'C ',jb
          WAVE(I, 1:nfun)=WAVE(I, 1:nfun)+ (k-1)*(k-2)*&
	                 VALUES(JB)*(veca-vecb)/(t(jb+k-2)-t(jb))
	endif
        ENDDO
   88 CONTINUE
!$OMP END DO
!$OMP END PARALLEL
      end subroutine det_ddwave
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! determine the wavefunction from the spline coefficients
!
      subroutine det_ddcore(ndim, npts, nt, nc, k, nfun, &
          rad_grid, wave, vec, t, isplrem)
      implicit none
      integer, intent(in):: ndim, npts, nt, nc, k, nfun, isplrem
      real(wp), intent(in), dimension(npts):: rad_grid
      real(wp), intent(out), dimension(npts, nfun):: wave
      real(wp), intent(in), dimension(ndim+1, nfun):: vec
      real(wp), intent(in), dimension(nt):: t
      real(wp), dimension(nc+k-1):: values
      real(wp), dimension(nfun):: veca, vecb
      integer:: ioff, i, left, mflag, leftmk, jb, error, nb
      real(wp):: xt
      nb = nc+k-2
      wave = 0._wp
      DO 88 I = 1, NPTS 
        VALUES = 0._wp
        XT = Rad_grid(I)
        CALL INTERV(T, NB, XT, LEFT, MFLAG) 
        LEFTMK = LEFT-(K-2) 
        CALL BSPLVB(T, NT, K-2, 1, Xt, LEFT, VALUES(LEFTMK+1))
        DO JB = isplrem+2, NB
!	print *,jb
	veca = 0._wp
	vecb = 0._wp
	if (t(jb+k-1) > t(jb)) then
!	  print *,'A ',jb
	  veca(1:nfun)=(vec(jb, 1:nfun)-vec(jb-1, 1:nfun))&
	              /(t(jb+k-1)-t(jb))
	endif
	if (t(jb+k-2) > t(jb-1)) then
!	  print *,'B ',jb, t(jb+k-2), t(jb-1)
	  if (jb == isplrem+2) then
	    vecb(1:nfun)=(vec(jb-1, 1:nfun))&
	      /(t(jb+k-2)-t(jb-1))
	  else
  	    vecb(1:nfun)=(vec(jb-1, 1:nfun)&
	                  -vec(jb-2, 1:nfun))&
	                / (t(jb+k-2)-t(jb-1))
	  endif
	endif
	if (t(jb+k-2) > t(jb)) then	
!	  print *,'C ',jb
          WAVE(I, 1:nfun)=WAVE(I, 1:nfun)+ (k-1)*(k-2)*&
	                 VALUES(JB)*(veca-vecb)/(t(jb+k-2)-t(jb))
	endif
        ENDDO
   88 CONTINUE
      end subroutine det_ddcore
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! IMPLEMENT THE BOUNDARY CONDITIONS AND SOLVE THE SET OF EQUATIONS
!
      SUBROUTINE HYDRO(NB, N, SBB, SDD, SPT, AA, EIG2, ISPLREM)
      implicit none
      integer, intent(in):: nb, n, isplrem
      real(wp), intent(in), dimension(nb, nb):: sbb, spt, sdd
      real(wp), intent(out), dimension(n+isplrem-1, n):: aa
      real(wp), intent(out), dimension(n):: eig2
      real(wp), allocatable, dimension(:,:):: a, bq, cq, f, g
      real(wp), allocatable, dimension(:):: eig, g2, g3
      integer:: error, ifail, ierr, i, j
!
      allocate(a(n, n), bq(n, n), cq(n, n), stat = error)
      if (error /= 0) THEN
        print *,'SPACE FOR A, B, C COULD NOT BE ALLOCATED'
        stop
      endif
      allocate(f(n, n), g(n, n), stat = error)
      if (error /= 0) THEN
        print *,'SPACE FOR F, G COULD NOT BE ALLOCATED'
        stop
      endif
      allocate(eig(n), g2(n), g3(n), stat = error)
      if (error /= 0) THEN
        print *,'SPACE FOR EIG, G2, G3 COULD NOT BE ALLOCATED'
        stop
      endif
       A(:,:)=SDD(isplrem+1:nb, isplrem+1:nb) 
       BQ(:,:)=SPT(isplrem+1:nb, isplrem+1:nb) 
       CQ(:,:)=SBB(isplrem+1:nb, isplrem+1:nb)
      A = -0.5_wp*A+BQ  
      IFAIL = 0
      F = 0.0_wp
      G = 0.0_wp
      write (fo, '(a)') 'A constructed'
!      print *,A
!      print *,C
      CALL NEWMAT(A, N, N, CQ, F, G) 
!      do i = 1, n
!       print *,(A(J, i), j = 1, n)
!      enddO
      write (fo, '(a)') 'A transformed'
      call tredi(a, n, n, eig, g2)
      call tqli(eig, g2, n, n, a)
      write (fo, '(a)') 'and diagonalised'
!      do i = 1, N
!       write(fo, '(i3, a, f16.9)') i, '   ',eig(i)
!      end do 
      do 707 i = 1, N 
  707 call lubksd(f, N, N, a(1, i)) 
      call sort(eig, N, a, g2)
      aa(1:n, 1:n)=a(1:n, 1:n)
      eig2(1:n)=eig(1:n)
      do i = 1, N
       write(fo, '(i3, a, f16.9)') i, '   ',eig2(i)
      end do
      deallocate(a, bq, cq, f, g, eig, g2, g3, stat = error)
      if (error /= 0) THEN
        print *,'SPACE FOR A-G3 COULD NOT BE DEALLOCATED'
        stop
      endif
!
  end subroutine hydro
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! IMPLEMENT THE BOUNDARY CONDITIONS AND SOLVE THE SET OF EQUATIONS
!
      SUBROUTINE HYDADJ(NB, N, SBB, SDD, SPT, AA, EIG2, LP, SFIT, MAXHF, ISPLREM)
      implicit none
      integer, intent(in):: lp, maxhf, nb, n, isplrem
      real(wp), intent(in):: sbb(nb, nb), spt(nb, nb)
      real(wp), intent(in):: sdd(nb, nb), sfit(nb, 5)
      real(wp), intent(inout):: aa(n+isplrem-1, n), eig2(n)
      real(wp), dimension(n, n):: bq, cq, fq, gq, aq
      real(wp), dimension(n):: g2
      real(wp), dimension(n):: eig4, temvec
      real(wp), dimension(n, n):: hmod, hmod1, cc
      real(wp), dimension(nb, nb):: saq
      integer:: i, j, kk, ierr, icount, error
      real(wp):: sfnorm
!      print *,sfit
      print *,NB, N
      print *,'What'
      print *,n
!      allocate(cq(n, n), stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR A, B, C COULD NOT BE ALLOCATED'
!        stop
!      endif
      print *,'What'
!      allocate(bq(n, n), stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR A, B, C COULD NOT BE ALLOCATED'
!        stop
!      endif
      print *,'Whatter'
!      allocate(aq(n, n), stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR A, B, C COULD NOT BE ALLOCATED'
!        stop
!      endif
      print *,'Huh'
!      allocate(f(n, n), g(n, n), stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR F, G COULD NOT BE ALLOCATED'
!        stop
!      endif
      print *,'What'
!      allocate(eig4(n), g2(n), stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR EIG, G2 COULD NOT BE ALLOCATED'
!        stop
!      endif
!      allocate(hmod(n, n), hmod1(n, n), cc(n, n), stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR A, B, C COULD NOT BE ALLOCATED'
!        stop
!      endif

      Aq = sdd(isplrem+1:NB, isplrem+1:NB)
      BQ = spt(isplrem+1:NB, isplrem+1:NB)
      CQ = sbb(isplrem+1:nb, isplrem+1:nb)
      Aq = -0.5_wp*Aq+BQ
!
! calculate the modification matrix for H, if needed
!
      print  *,'hydro ',lp, maxhf
!      if (icon .le. nlhf) then
       print *,'enter'
       do 100 i = 1, N
!       print *,i, sfit(i+1)
       do 100 j = 1, N
       cc(j, i)=0._wp
       do 101 kk = lp, maxhf
       cc(j, i)=cc(j, i)+sfit(i+isplrem, kk)*sfit(j+isplrem, kk)
  101  continue
  100  continue
       do 110 i = 1, N
        do 120 j = 1, N
         hmod(j, i)=0._wp
 	if (j .eq. i) then
          hmod(j, i)=1._wp
 	endif
         do 130 kk = 1, N
          hmod(j, i)=hmod(j, i)-cq(j, kk)*cc(kk, i)
  130    continue
  120   continue
  110  continue
       do 140 i = 1, N
        do 150 j = 1, N
         hmod1(j, i)=0._wp
  	 if (j .eq. i) then
          hmod1(j, i)=1._wp
	 endif
         do 160 kk = 1, N
          hmod1(j, i)=hmod1(j, i)-cc(j, kk)*cq(kk, i)
  160    continue
  150   continue
  140  continue
!
! calculate the modification for H
!
       do 210 i = 1, N
        do 220 j = 1, N
         bq(j, i)=0._wp
         do 230 kk = 1, N
          bq(j, i)=bq(j, i)+hmod(j, kk)*aq(kk, i)
  230    continue
  220   continue
  210  continue
!
!
!
       do 310 i = 1, N
        do 320 j = 1, N
         aq(j, i)=0._wp
         do 330 kk = 1, N
          aq(j, i)=aq(j, i)+bq(j, kk)*hmod1(kk, i)
  330    continue
!         print *,j, i, aq(j, i)
  320   continue
  310  continue
!      endif
 
!      do i = 1, n     
!      print *,i, aq(i, 1), cq(i, 1)
!      end do
!      do i = 1, n     
!      print *,i, aq(i, n), cq(i, n)
!      end do
!     CALL F02AEF(Aq, N, CQ, N, NBMX, EIG, B, N, G2, G3, ifail)
      CALL NEWMAT(Aq, N, N, CQ, Fq, Gq) 
      print *,'***** begin tred2' 
      CALL TREDI(aq, n, n, eig4, g2) 
      CALL TQLI(eig4, g2, n, n, aq) 
      print *,'backsub'
      do 707 i = 1, N 
  707 call lubksd(fq, N, N, aq(1:n, i)) 
      print *,'enter sort'
      call sort(eig4, N, aq, g2)
!      
! rearrange eigenvalues, and remove bound orbitals
!     
      print *,'back from sort'
      icount = 1
      do 10 i = 1, N
       if (abs(eig4(i)).gt.1e-5) then
        do 20 j = 1, n
         aa(j, icount)=aq(j, i)
   20   continue
        eig2(icount)=eig4(i)
        print *,eig4(i)
	icount = icount+1
       else
        print *,'------- ',eig4(i)	
       endif
   10 continue
      saq(1:nb, 1:n)=matmul(sbb(1:nb, isplrem+1:nb), aa(1:n, 1:n))
      do i = 1, icount-1
        do j = lp, maxhf
	aa(1:n, i)=aa(1:n, i)- &
	          dot_product(sfit(1:nb, j), saq(1:nb, i))&
		  *sfit(isplrem+1:nb, j)
        enddo
      enddo
!
! Orthogonalisation to be included
!
       do i = 1, icount-1
        do j = 1, i-1
         temvec = matmul(sbb(isplrem+1:nb, isplrem+1:nb), aa(1:n, j))
	 aa(1:n, i)=aa(1:n, i)-&
	           dot_product(aa(1:n, i), temvec)*aa(1:n, j)
        enddo 
! Normalise
        temvec = matmul(sbb(isplrem+1:nb, isplrem+1:nb), aa(1:n, i))
	sfnorm = dot_product(aa(1:n, i), temvec)
	print *,'Normalisation ',i, icount-1, sfnorm
	aa(1:n, i)=aa(1:n, i)/sqrt(sfnorm)
      enddo
      print *,'Orthogonality'
      saq(1:nb, 1:n)=matmul(sbb(1:nb, isplrem+1:nb), aa(1:n, 1:n))
      do i = 1, icount-1
       write(fo, *) i, &
        (dot_product(sfit(isplrem+1:nb, j), saq(isplrem+1:nb, i)), j = lp, maxhf), &
	dot_product(aa(1:n, i), saq(isplrem+1:nb, i))
      enddo
!      print *,dot_product(sfit(1:nb, 2), saq(1:nb, 3))
!      print *,dot_product(sfit(1:nb, 3), saq(1:nb, 3))
!      print *,dot_product(sfit(1:nb, 4), saq(1:nb, 3))
!      print *,dot_product(aa(1:n, 3), saq(2:nb, 3))
!      if (icon .le. nlhf) then
!       if (icount .eq. nnhf(icon)) then
!         do kk = 1, nnhf(icon)
!         do 30 j = 1, n
!          aa(j, kk)= sfit(j+1, kk)
!   30	 continue
!         enddo
!	else
!	 print *,'Trouble in paradise',icount, nnhf(icon)
!       endif
!      endif
!      deallocate(aq, bq, cq, f, g, g2, eig4, hmod, hmod1, cc, stat = error)
!      if (error /= 0) THEN
!        print *,'SPACE FOR A, B, C COULD NOT BE DEALLOCATED'
!        stop
!      endif
!      stop
      return 
  end subroutine hydadj
!
! AND COMBINE LINEAR AND EXPONENTIAL KNOT SETS
!
      subroutine tlxexp(nt, nc, nc1, nc2, k, nint, nleg, rr0, rr1, t)
      implicit real*8(a-h, o-z)
      real(wp), intent(inout):: t(nt)
      real(wp):: t1(nt), rf, rfs, rff
      integer, intent(in):: nt, nc, nc1, nc2, k, nint, nleg
      real(wp), intent(in):: rr0, rr1
      integer:: i, j
      BETA1=(RR0-RR1)/FLOAT(NC2-1)
      BETA2 = LOG(RR1*EXP(3._wp)+1._wp)/FLOAT(NC1-1)   
!      PRINT *,' BETA1 = ',BETA1 
!      PRINT *,' BETA2 = ',BETA2
      T1(1)=0.   
!     DO 65 I = 2, NC1   
!  65 T1(I)=EXP(-3._wp+FLOAT(I-1)*BETA2)-EXP(-3._wp)   
!     DO 60 I = 1, NC2-1   
!  60 T1(I+NC1)=FLOAT(I)*BETA1+RR1   
      rf = asinh(rr0)
      rfs = rf/float(nc-3)
      rff = rr0/(tanh(rf/5.5_wp)*rf)
      DO 60 I = 4, NC
   60 T1(I)=tanh(rfs*float(i-3)/5.5_wp)*rfs*float(i-3)*rff
      T1(2)=T1(4)/5._wp
      T1(3)=T1(4)*0.75_wp
      T1(4)=T1(4)*1.75_wp
      T1(5)=T1(5)*0.80_wp
      t1(6)=t1(6)*0.90_wp
      DO 55 I = 1, K 
   55 T(I)=0.0 
      DO 50 I = 1, NC 
   50 T(I+K-1)=T1(I) 
      DO 45 I = NC+K, NC+2*K-2 
   45 T(I)=T1(NC) 
      PRINT 121, (T(I), I = 1, NT) 
  121 FORMAT(1X, 6F12.7) 
!     stop
!      PRINT *,'*** GRID ***' 
!      DO 500 I = 1, NINT 
!      DO 500 J = 1, NLEG 
!  500 XG(I, J)=(T(I+K)-T(I+K-1))/2._wp*GP(J)+(T(I+K)+T(I+K-1))/2._wp 
      return
      end subroutine tlxexp
!
! Stuff from the previous version
!
  subroutine scoeff
    use rm_data, only: nrang2
    use rm_xdata, only: maxnhf
    integer             :: kd, status

    shm = MAXVAL(maxnhf)      ! global variable
    kd = nrang2+shm
    allocate (b(kd, kd), ovrlap(nrang2, shm), temp(kd), stat = status)
    if (status /= 0) call alloc_error (status, 'scoeff', 'a')
  end subroutine scoeff

  subroutine xscoeff
    integer             :: status
    deallocate (b, ovrlap, temp, stat = status)
    if (status /= 0) call alloc_error (status, 'xscoeff', 'd')
  end subroutine xscoeff
  
  subroutine set_ends
    use rm_data, only: nrang2
    use rm_xdata, only: lrang4
    use symmetries, only: lrang3
    integer               :: id, status

    id = lrang3+lrang4-1
    allocate (ends(nrang2, id), dends(nrang2, id), stat = status)
    if (status /= 0) call alloc_error (status, 'set_ends', 'a')
  end subroutine set_ends
  
  subroutine xends
    integer               :: status
    deallocate (ends, dends, stat = status)
    if (status /= 0) call alloc_error (status, 'xends', 'd')
  end subroutine xends

      SUBROUTINE ERMBSP(nb, nint, k, nleg, nt, gp, wtg, bb, xg, t, kmax)
!      IMPLICIT REAL*8 (A-H, O-Z)
      real(wp), intent(in):: T(NT)
      real(wp), intent(in):: GP(NLEG), WTG(NLEG)
      real(wp), intent(in):: BB(NINT, NLEG, NB)
      real(wp), allocatable:: BA(:,:,:,:), XX(:,:,:), xpow(:,:,:,:)
      real(wp), intent(in):: XG(NINT, NLEG)
      real(wp), allocatable:: AA(:,:,:), A(:,:,:,:,:), tdif(:), tmp(:,:,:,:)
!      real(wp), allocatable:: DIST(:,:,:)
      real(wp), allocatable:: xgk(:,:,:), xgik(:,:,:), b1(:,:,:)
      integer, intent(in):: nb, nint, k, nleg, nt, kmax
      integer:: i, kk, j, ii, j1, jj, i2, j12, error, ik, ik2, inde
      real(wp):: sum, final
      integer, external:: omp_get_thread_num
      integer, external:: omp_get_num_threads
      
!      allocate(rkint(nb, 0:k-1, nb, 0:k-1, 0:kmax), stat = error)
!      if (error /= 0) then
!       print *,'RKINT could not be allocated'
!       stop
!      endif
       allocate(b1(nint, nleg, nb), stat = error)
       if (error /= 0) then
        print *,'DIST could not be allocated'
        stop
       endif
      allocate(ba(nint, nleg+1, nleg, nb), stat = error)
      if (error /= 0) then
       print *,'BA could not be allocated'
       stop
      endif
      allocate(xx(nint, nleg+1, nleg), stat = error)
      if (error /= 0) then
       print *,'XX could not be allocated'
       stop
      endif
      allocate(xpow(nint, nleg+1, nleg, 0:kmax), xgk(nint, nleg, 0:kmax), &
               xgik(nint, nleg, 0:kmax), stat = error)
      if (error /= 0) then
       print *,'XPOW could not be allocated'
       stop
      endif
      allocate(aa(nint, nleg, 0:kmax), stat = error)
      if (error /= 0) then
       print *,'AA could not be allocated'
       stop
      endif
      allocate(a(nint, nleg, nb, 0:k-1, 0:kmax), stat = error)
      if (error /= 0) then
       print *,'A could not be allocated'
       stop
      endif
      allocate(tdif(nint), tmp(nint, nleg, nb, 0:k-1), stat = error)
      if (error /= 0) then
       print *,'TDIF, TMP could not be allocated'
       stop
      endif
!      NB = NC+K9-2
!      NT = NC+2*K9-2
!      NS = NB-2
!      NDIM = NS
!      NINT = NC-1
!      NLEG = K9+1
!      KMAX = 2*LWHAT-1
!
! DETERMINE THE VALUES OF THE SPLINES AT THE INTERMEDIATE POINTS
!
!      K = K9
      DO I = 1, NINT
       TDIF(I)=(T(I+K)-T(I+K-1))/2._wp
      ENDDO
      CALL INTBSP(NINT, NLEG, NT, NB, K, XG, T, BA, XX, WTG, GP)
!
! FIRST LOOP OVER KK TO SAVE MEMORY
!
!$OMP PARALLEL PRIVATE(KK, II, J, I, j1, final, jj, i2, j12, ik, ik2, sum, inde) &
!$OMP shared(a, aa, tmp, kmax, k, xx, nleg, nint, nb, ba, bb, nt, wtg, t, xg, rkint, tdif, xgk, xgik, xpow, b1)
!$OMP DO
        DO I = 1, NB
!        PRINT *,'J1 I :',j1, k, ' STARTED'
         DO IK = 1, NLEG
          DO IK2 = 1, NINT
           B1(IK2, IK, I)=BB(IK2, IK, I)*TDIF(IK2)
          ENDDO
         ENDDO
        ENDDO
!$OMP END DO
!$OMP DO
       DO J1 = 0, K-1
        DO I = 1, NB-J1
!        PRINT *,'J1 I :',j1, k, ' STARTED'
         J = I+J1
         DO IK = 1, NLEG
          DO IK2 = 1, NINT
           TMP(IK2, IK, I, J1)=B1(IK2, IK, I)*BB(IK2, IK, J)
          ENDDO
         ENDDO
        ENDDO
       ENDDO
!$OMP END DO
!$OMP DO
      DO KK = 0, KMAX
      inde = omp_get_thread_num()
      j12 = omp_get_num_threads()
      PRINT *,'POWER ',KK, ' ON THREAD ',inde, ' OF ',j12
!
! FIRST, DETERMINE B_1 R**KK B_2
!  ASSUME B_2 >= B_1
!
       DO II = 1, NLEG
        DO J = 1, NLEG+1
         DO I = 1, NINT
          XPOW(I, J, II, kk) = XX(I, J, II) ** KK
         ENDDO
        ENDDO
        DO I = 1, NINT
          xgik(i, ii, kk)=xg(i, ii)** (-kk-1)
        enddo
       ENDDO
!       PRINT *,'POSITIVE POWERS DETERMINED'
       DO I = 1, NB
!        PRINT *,'SERIES ',I, ' STARTED'
        DO J1 = 0, K-1
        IF ((I+J1).LE.NB) THEN
         J = I+J1
         CALL FINT(I, J, NINT, NLEG, NB, NT, K, AA, BA, XPOW, FINAL, WTG, T, XG, kk, kmax)
!         DIST(I, J, KK)=FINAL
!         PRINT *,'FINAL = ',I, J, KK, FINAL
!         DO II = 1, NINT
          DO JJ = 1, NLEG
           A(1:nint, JJ, I, J1, kk) = AA(1:nint, JJ, kk)*xgik(1:nint, jj, KK)
          ENDDO
!         ENDDO
        ENDIF
        ENDDO
       ENDDO
!       PRINT *,'HALFWAY THE FIRST INTEGRATION'
!
! SECOND DETERMINE B_1 R**(-K-1) B_2
!
       DO II = 1, NLEG
        DO J = 1, NLEG+1
         DO I = 1, NINT
          XPOW(I, J, II, kk) = XX(I, J, II) ** ( -KK-1 )
         ENDDO
        ENDDO
        DO I = 1, NINT
         XGK(I, II, kk)=xg(I, II)**KK
        ENDDO
       ENDDO
!       PRINT *,'NEGATIVE POWERS DETERMINED'
       DO I = 1, NB
!       PRINT *,'SERIES ',I, ' STARTED'
        DO J1 = 0, K-1
        IF ((I+J1).LE.NB) THEN
         J = I+J1
         CALL FINT(I, J, NINT, NLEG, NB, NT, K, AA, BA, XPOW, FINAL, WTG, T, XG, kk, kmax)
         DO JJ = 1, NLEG
          DO II = 1, NINT
           A(II, JJ, I, J1, kk) = A(II, JJ, I, J1, kk)+(FINAL-AA(II, JJ, kk))*xgk(ii, jj, kK)
          ENDDO
         ENDDO
        ENDIF
        ENDDO
       ENDDO
!
! AND PERFORM THE SECOND INTEGRATIONS
!
!       PRINT *,'START THE SECOND INTEGRATION'
       DO J12 = 0, K-1
        DO I2 = 1, NB
         DO J1 = 0, K-1
          DO I = 1, NB
           RKINT(I, J1, I2, J12, KK)=0.
          ENDDO
         ENDDO
        ENDDO
       ENDDO
!       PRINT *,'ARRAY SET TO ZERO'
       DO J1 = 0, K-1
        DO I = 1, NB-J1
!        PRINT *,'J1 I :',j1, k, ' STARTED'
         J = I+J1
!         DO IK = 1, NLEG
!          DO IK2 = 1, NINT
!           TMP(IK2, IK)=BB(IK2, IK, I)*BB(IK2, IK, J)
!          ENDDO
!         ENDDO
         DO J12 = 0, K-1
          DO I2 = 1, NB-J12
!            J2 = I2+J12
            SUM = 0._wp
            CALL FININT(NINT, NLEG, NB, TMP, A, WTG, I2, J12, SUM, K, kk, kmax, i, j1)
            RKINT(I2, J12, I, J1, KK)=SUM 
!           PRINT *,I, J1, I2, J12, KK, SUM
          ENDDO
         ENDDO
       ENDDO
      ENDDO
      ENDDO
!$OMP END DO
!$OMP END PARALLEL
      PRINT *,'ERMBSP FINISHED'
      deallocate(ba, xx, aa, a, tdif, tmp, xpow, xgk, xgik, stat = error)
      if (error /= 0) then
       print *,'STUFF could not be deallocated'
       stop
      endif
      RETURN
      END subroutine ermbsp
!
! HERE IS THE FIRST INTEGRATION PERFORMED
!
      SUBROUTINE FINT(I, J, NINT, NLEG, NB, NT, K, AA, BA, XPOW, FINAL, WTG, T, XG, mm, kmax)
!      IMPLICIT REAL*8 (A-H, O-Z)
      integer, intent(in):: i, j, nint, nleg, nb, nt, k, mm, kmax
      real(wp), intent(inout):: AA(NINT, NLEG, 0:kmax)
      real(wp), intent(in):: BA(NINT, NLEG+1, NLEG, NB)
      real(wp), intent(in):: XPOW(NINT, NLEG+1, NLEG, 0:kmax), WTG(NLEG)
      real(wp), intent(in):: XG(NINT, NLEG), T(NT)
      real(wp), intent(out):: final
      integer:: init, ifin, ii, jj, kk
      real(wp):: sum
      init = j-k+1
      if (init .lt. 1) init = 1
      ifin = i
      if (ifin .gt. nint) ifin = nint
      DO II = 1, INIT-1
       DO JJ = 1, NLEG
       AA(II, JJ, mm) =0._wp
       ENDDO
      ENDDO
      SUM = 0._wp
      DO II = INIT, IFIN
       DO KK = 1, NLEG
        SUM = SUM+BA(II, 1, KK, I) * BA(II, 1, KK, J) * XPOW(II, 1, KK, mm) &
                  * wtg(kk) * ( xg(ii, 1) - t(ii+k - 1)) /2._wp
       ENDDO
       AA(II, 1, mm) = SUM
       DO JJ = 2, NLEG
        DO KK = 1, NLEG
         SUM = SUM+BA(II, JJ, KK, I) * BA(II, JJ, KK, J) * XPOW(II, JJ, KK, mm) &
                   * wtg(kk) * ( xg(ii, jj) - xg(ii, jj-1) ) /2._wp
        ENDDO
        AA(II, JJ, mm) = SUM
       ENDDO
       DO KK = 1, NLEG
        SUM = SUM+BA(II, NLEG+1, KK, I) * BA(II, NLEG+1, KK, J) &
                  * XPOW(II, NLEG+1, KK, mm) * wtg(KK) * (T(II+K) &
                  - XG(II, NLEG))/2._wp 
       ENDDO
      ENDDO
      DO II = IFIN+1, NINT
       DO JJ = 1, NLEG
        AA(II, JJ, mm)=SUM
       ENDDO
      ENDDO
      FINAL = SUM
      RETURN
      END subroutine fint
!
!
!
      SUBROUTINE FININT(NINT, NLEG, NB, TMP, A, WTG, I3, I4, SUM, K, kk, kmax, i1, j1)
!      IMPLICIT REAL*8(A-H, O-Z)
      integer, intent(in):: nint, nleg, nb, i3, i4, k, kk, kmax, i1, j1
      real(wp), intent(in):: A(NINT, NLEG, NB, 0:K-1, 0:kmax), TMP(NINT, NLEG, nb, 0:k-1)
      real(wp), intent(in):: WTG(NLEG)
      real(wp), intent(out):: sum
      real(wp):: sum1
      integer:: i, j
      SUM = 0._wp
      DO J = 1, NLEG
       sum1 = 0._wp
       DO I = 1, NINT
        SUM1 = SUM1+TMP(I, J, i1, j1)*A(I, J, I3, I4, kk)
       ENDDO
       sum = sum+sum1*wtg(j)
      ENDDO
      RETURN
      END subroutine finint

      SUBROUTINE INTBSP(nint, nleg, nt, nb, k, xg, t, ba, xx, wtg, gp)
!      implicit real*8(a-h, o-z) 
!      PARAMETER (NBMX = 200)
      real(wp), intent(in):: xg(nint, nleg), t(nt) 
      real(wp), intent(out):: ba(nint, nleg+1, nleg, nb)
      real(wp), intent(out):: xx(nint, nleg+1, nleg)
      real(wp), allocatable:: values(:) 
      real(wp), intent(in):: wtg(nleg), gp(nleg)
      integer, intent(in):: nint, nleg, nt, nb, k
      integer:: i, j, error, kk, jj, left, leftmk, mflag
      real(wp):: x1, x2, xt
      PRINT *,'*** TEST ***'
!
! DETERMINE THE NEW GRID
! 
      allocate(values(nb), stat = error)
      if (error /= 0) then
       print *,'VALUES (INTBSP) could not be allocated'
       stop
      endif

      DO I = 1, NINT
       X1 = T(I+K-1)
       X2 = XG(I, 1)
       DO Kk = 1, NLEG
        XX(I, 1, Kk) = (x1+x2)/2._wp+gp(kk)*(x2-x1)/2._wp
       enddo
       Do J = 2, NLEG
        X1 = XG(I, J-1)
        X2 = XG(I, J)
        DO Kk = 1, NLEG
         XX(I, J, Kk) = (x1+x2)/2._wp+gp(kk)*(x2-x1)/2._wp
        enddo
       ENDDO
       X1 = XG(I, NLEG)
       X2 = T(I+K)
       DO Kk = 1, NLEG
        XX(I, NLEG+1, KK) = (x1+x2)/2._wp+gp(kk)*(x2-x1)/2._wp
       enddo
      ENDDO
!
!
!     
      DO I = 1, NINT 
       DO J = 1, NLEG+1
        DO KK = 1, NLEG
         DO JJ = 1, NB 
          VALUES(JJ)=0._wp
         ENDDO
         Xt = XX(I, J, KK) 
! 
! LOCATE X WITH RESPECT TO KNOT ARRAY T. 
! 
         CALL INTERV(T, NB, Xt, LEFT, MFLAG) 
         LEFTMK = LEFT-K
!      print *,left
         CALL BSPLVB(T, NT, K, 1, Xt, LEFT, VALUES(LEFTMK+1)) 
!         DO JB = 1, NB 
          BA(I, J, KK, :)=VALUES(:)
         END DO
        END DO
      END DO
      deallocate(values, stat = error)
      if (error /= 0) then
       print *,'VALUES (INTBSP) could not be deallocated'
       stop
      endif
      RETURN
      END subroutine intbsp
      
      subroutine dorkin(n1, l1, n2, l2, n3, l3, n4, l4, n5, lam, rkval)
      integer, intent(in):: n1, l1, n2, l2, n3, l3, n4, l4, n5, lam
      real(wp), intent(out):: rkval(1:n5-n4+1)
      logical:: test, test1
      test=((n1 .eq. n1test).and.(l1 .eq. l1test))
      test1=(test.and.(n3 .eq. n3test).and.(l3 .eq. l3test))
      test=(test1.and.(lamtest .eq. lam))
      if (.not. test ) then
         CALL PRPNEW(N1-L1+1, L1-1, N3-L3+1, L3-1, LAM)
         n1test = n1
         l1test = l1
         n3test = n3
         l3test = l3
         lamtest = lam
      endif
      CALL ACCDUB(N2-L2+1, L2-1, N4-L4+1, L4-1, N5-L4+1, LAM, rkval)
      print *,'RK: ',n1, l1, n2, l2, n3, l3, n4, l4, lam, rkval, test
      end subroutine dorkin
      
      SUBROUTINE PRPNEW(IX, IXL, IZ, IZL, LAM)
!      IMPLICIT REAL*8(A-H, O-Z)
!      PARAMETER (NC = 39)
!      PARAMETER (K9 = 6)
!      PARAMETER (NC2 = 39)
!      PARAMETER (NLEG = K9+1)
!      PARAMETER (NSER = 11)
!      PARAMETER (Z = 3.)
!      PARAMETER (RHO = -4.0)
!c      PARAMETER (NRP = 651)
!      PARAMETER (H = 0.015625)
!      PARAMETER (NB1 = NC+K9)
!      PARAMETER (NB = NC+K9-2)
!      PARAMETER (NT = NC+2*K9-2)
!      PARAMETER (NS = NB-2)
!      PARAMETER (NDIM = NC+K9-4)
!      PARAMETER (NINT = NC-1)
!      PARAMETER (NB12 = NC2+K9)
!      PARAMETER (NB2 = NC2+K9-2)
!      PARAMETER (NT2 = NC2+2*K9-2)
!      PARAMETER (NS2 = NB2-2)
!      PARAMETER (NDIM2 = NC2+K9-3)
!      PARAMETER (NINT2 = NC2-1)
!      PARAMETER (NS1 = NSER-1)
!      PARAMETER (NKMX = 2*NSER-1)
!      PARAMETER (NEMX = 4*NSER+2)
!      PARAMETER (K = K9)
!      PARAMETER (KMAX = 2*NSER-1)
!      COMMON/RKVAL/RKINT(NB, 0:K9-1, NB, 0:K9-1, 0:KMAX)
!      DIMENSION VEC(NDIM, NDIM, NSER)
!c      DIMENSION VEC2(NDIM2, NDIM2, NSER)
!      DIMENSION PRP(NB, 0:K9-1, 0:2*KMAX+1)
!      real(wp):: FACTO(0:K-1)
      integer, intent(in):: ix, ixl, iz, izl, lam
      integer:: jj, ii, nb, k, ndim, i, j
      real(wp):: V11, V21, V31, V41, VFIRST, fac2
!
!
!
      ndim = nbstore-1
      k = kstore
!      DO JJ = 0, K-1
!       DO II = 1, NDIM
!         PRP(II, JJ)=0._wp
!       ENDDO
!      ENDDO
!      DO J = 0, K-1
!       if (j == 0) then
!         facto = 0.5_wp
!	else
!	 facto = 1.0_wp
!       endif
!$OMP PARALLEL PRIVATE(JJ, II, FAC2, V11, V21, V31, V41, VFIRST, J, I) &
!$OMP SHARED(PRP, NDIM, K, SPLVEC, FACTO, LAM, VF)
!$OMP DO
       DO J = 0, K-1
        FAC2 = FACTO(J)
        DO I = 1, NDIM-J
         V11 = SPLVEC(i, ix, ixl+1)
         V31 = SPLVEC(I+j, ix, ixl+1)
         V41 = SPLVEC(i, iz, izl+1)
         V21 = SPLVEC(i+j, iz, izl+1)
         VF(I, J)=(V11*V21+V31*V41)*FAC2
        ENDDO
       ENDDO
!$OMP END DO
!$OMP DO
      DO II = 1, NDIM
       DO JJ = 0, K-1
         PRP(jj, ii)=0._wp
       ENDDO
       DO JJ = 0, K-1
       IF (II+JJ .LE. NDIM) THEN
        DO J = 0, K-1
         DO I = 1, NDIM-J
!        DO JJ = 0, K-1
!        DO II = 1, NDIM-JJ
! Note rkint has a bizarre order now: r2 r2 r1 r1 
            PRP(jj, ii)=PRP(jj, ii) &
                             +VF(I, j)*rkint(i+1, j, ii+1, jj, lam)
!        ENDDO
         ENDDO
       ENDDO
       ENDIF
       ENDDO
      ENDDO
!$OMP END DO
!$OMP END PARALLEL
      RETURN
      END subroutine prpnew
!C
!C
!C
       SUBROUTINE ACCDUB(IX, IXL, IT, ITL, ITF, LAM, RKVAL)
!      IMPLICIT REAL*8(A-H, O-Z)
!      PARAMETER (NC = 39)
!      PARAMETER (K9 = 6)
!      PARAMETER (NC2 = 39)
!      PARAMETER (NLEG = K9+1)
!      PARAMETER (NSER = 11)
!      PARAMETER (Z = 3.)
!      PARAMETER (RHO = -4.0)
!      PARAMETER (NRP = 651)
!      PARAMETER (H = 0.015625)
!      PARAMETER (NB1 = NC+K9)
!      PARAMETER (NB = NC+K9-2)
!      PARAMETER (NT = NC+2*K9-2)
!      PARAMETER (NS = NB-2)
!      PARAMETER (NDIM = NC+K9-4)
!      PARAMETER (NINT = NC-1)
!      PARAMETER (NB12 = NC2+K9)
!      PARAMETER (NB2 = NC2+K9-2)
!      PARAMETER (NT2 = NC2+2*K9-2)
!      PARAMETER (NS2 = NB2-2)
!      PARAMETER (NDIM2 = NC2+K9-3)
!      PARAMETER (NINT2 = NC2-1)
!      PARAMETER (NS1 = NSER-1)
!      PARAMETER (NKMX = 2*NSER-1)
!      PARAMETER (NEMX = 4*NSER+2)
!      PARAMETER (K = K9)
!      PARAMETER (KMAX = 2*NSER-1)
!      COMMON/RKVAL/RKINT(NB, 0:K9-1, NB, 0:K9-1, 0:KMAX)
!      DIMENSION VEC(NDIM, NDIM, NSER)
!C      DIMENSION VEC2(NDIM2, NDIM2, NSER)
!      DIMENSION PRP(NB, 0:K9-1, 0:2*KMAX+1)
       integer, intent(in):: ix, ixl, it, itl, lam, itf
       real(wp), intent(out):: rkval(1:itf-it+1)
       real(wp):: sum, fac2, v12, v22, v32, v42, accsum
       integer  :: jj, ii, k, ndim, itc
       k = kstore
       ndim = nbstore-1
!$OMP PARALLEL PRIVATE(II, JJ, V12, V42, FAC2, V22, V32, ITC), &
!$OMP SHARED(RKVAL, NDIM, SUMACC, SPLVEC, IT, ITF, ITL, IX, IXL, K, FACTO, PRP)
!$OMP DO 
       DO ITC = IT, ITF
        RKVAL(ITC-IT+1)=0._wp
        DO II = 1, NDIM
        V12 = SPLVEC(ii, itc, itl+1)
        V42 = SPLVEC(ii, ix, ixl+1)
         DO JJ = 0, K-1
         if (ii+jj .le. ndim) then
          fac2 = facto(jj)
           V22 = SPLVEC(ii+jj, ix, ixl+1)
           V32 = SPLVEC(ii+jj, itc, itl+1)
           RKVAL(itc-it+1)=RKVAL(itc-it+1)+FAC2*(V12*V22+V32*V42)*PRP(jj, ii)
         endif
         ENDDO
        ENDDO
       ENDDO
!$OMP END DO
!$OMP END PARALLEL
      RETURN
      END subroutine accdub

end module bassplnew_orb
