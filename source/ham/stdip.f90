module dipoles
! Prepare dipole matrices for subsequent photoionization calculations
! Time-stamp: "2005-02-04 10:44:12 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, itape1, itape3, jbufim, jbuffm, jbufev
! itape1 is used for d00 file
! itape3 is used for dxx files (photoionisation) or d file (multiphoton)
! (currently set for multiphoton work)
  implicit none

  integer, allocatable, save       :: lpset(:)
  integer, allocatable, save       :: ipsset(:)
  integer, allocatable, save       :: ippset(:)

  private
  public stdip, finsym
  public lpset, ipsset, ippset

contains

  subroutine stdip (lrgslo, lrgsup, lrgllo, lrglup, mvely, ivec)
! pre and post multiply dipole matrix by eigenvectors of Hamiltonians
    use sc_ham, only: npset, npcset
    use filehand, only: open, readix, ireada, ireadb, ireadc, close
    use error_prt, only: alloc_error
    use set_data, only: nast
    use rad_data, only: lrang2
    integer, intent(in)  :: lrgslo
    integer, intent(in)  :: lrgsup
    integer, intent(in)  :: lrgllo
    integer, intent(in)  :: lrglup
    integer, intent(in)  :: mvely
    integer, intent(in)  :: ivec(npset)
    integer              :: immei(npset)
    integer              :: immer(npset,npset)
    integer              :: itrans(npset,npset)
    integer              :: ipos(2), ivsize(2), jnchv(2)
    integer              :: ichan(nast*lrang2+2,npset)
    integer              :: idummy(nast*lrang2)
    character(len=7)     :: filea, fileb, stats
    integer              :: ieven = 0
    integer              :: iodd = 1
    integer              :: mvel, nsym, mv, npsb, inext, inex2, ijcat
    integer              :: status, jnbi, jnch, jnb, j, j1, lrgli, inbi
    integer              :: i, imin, intr, ipnogo, iposch, lrgs
    integer              :: jeig, isize, inch, iposvec, inb
    logical              :: real_typ

    mvel = mvely + 1
    nsym = 0
! open dipole elements file
    filea = 'mulei'
    fileb = 'muler'
    stats = 'old'
    real_typ = .false.
    call open (jbufim, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffm, fileb, stats, real_typ)

    call readix (jbufim)          ! index of mulei
    call ireada (jbufim, mv)      ! mvely
    call ireada (jbufim, npsb)    ! npset
    if (mv /= mvely .or. npsb /= npset) then
       write (fo,'(a,4i6)') 'stdip: incompatible data - mv, mvely, &
            &npsb, npset = ', mv, mvely, npsb, npset
       stop
    end if
    allocate (lpset(npset), ipsset(npset), ippset(npset), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'stdip', 'a')
    call ireadb (jbufim, lpset, 1, npset)
    call ireadb (jbufim, ipsset, 1, npset)
    call ireadb (jbufim, ippset, 1, npset)
    call ireadb (jbufim, npcset, 1, npset)
    call ireadb (jbufim, immei, 1, npset)
    call ireada (jbufim, intr)
    call ireadc (jbufim, immer, 1, npset, 1, npset, npset, 0)
    call ireadc (jbufim, itrans, 1, npset, 1, npset, npset, 0)
!
! d00 index file and d element file
    open (itape1, file='d00', status='unknown', form='unformatted')
    write (itape1) intr
    ipnogo = 2 ! dummy parity # to signify no matrix element
    open (itape3, file='d', status='old', form='unformatted')
! get past target dipoles:
    read (itape3)
    read (itape3)

! read channel information

! ichan(i,j)
! j = (inbi) n+1 symmetry label
! i = 1 : (inch) number of channels in symmetry inbi
! i = 2...inch+1 : (l2p) nb of continuum functions / channel
! i = inch+2 : (inb) nb of bound configurations
    iposch = 0
    do i = 1,npset
       imin = 0
       j1 = 0
       do j = 1,npset
          if (immei(j) > 0) then
             if (imin == 0 .or. immei(j) < imin) then
                j1 = j
                imin = immei(j)
             end if
          end if
       end do
       immei(j1) = -999
       call filestart (imin, imin, iposch, jbufim)
       call ireada (jbufim, inch)    ! # channels
       print *,'J, INCH', j1,inch
       ichan(1,j1) = inch
       call ireada (jbufim, ichan(inch+2, j1))   ! # cfgs
       call ireadb (jbufim, idummy, 1, inch)     ! l2pi values ignored
       call ireadb (jbufim, ichan(1:inch+1,j1), 2, inch+1)  ! nvary values
       write (fo,'(a,5i10,/(28x,5i10))') 'sym, # chls, nvary, # cfgs: ',&
            j1, ichan(1:inch+2,j1)
    end do

! reopen eigenvectors file
    iposvec = 1
    filea = 'hvec'
    stats = 'old'
    real_typ = .true.
    call open (jbufev, filea, stats, real_typ) 
!
! we require i-side even, j-side odd 
! (consistent with stmmat structure for mpol .eq. 1)
! deal with one initial symmetry (even) and two final symmetries (odd) at
!    a time

    spin_loop: do lrgs = lrgslo, lrgsup, 2  ! total spin
       inext = 1
! get eigenvectors for lrgl .eq. lrgllo (even and odd)
! ivsize (jeig): jeig = 1 & 2 odd symmetries
! isize   : ieig = 3 even symmetry
       call finsym (lrgllo, lrgs, ieven, inb, inbi)
       inch = ichan(1,inbi)
       if (inch > 0) call getvec (inbi, ivec, isize, iposvec, jbufev)
       jeig = inext
       call finsym (lrgllo, lrgs, iodd, jnb, jnbi)
       jnch = ichan(1,jnbi)
       jnchv(inext) = jnch
       if (jnch > 0) call getvec (jnbi, ivec, ivsize(inext), &
            iposvec, jbufev)
       ipos(inext) = jnbi
!
       if (lrgllo /= 0) then
          if (inch > 0 .and. jnch > 0) then
             write (itape1) lrgs, lrgllo, iodd, lrgs, lrgllo, ieven
             nsym = nsym + 1
             ijcat = immer(inbi,jnbi)
             call trdip (ijcat, mvel, inbi, jnbi, isize, ivsize(jeig),&
                  ichan(1,inbi), ichan(:,jnbi), ivec, iposvec)
          else
             write (itape1) lrgs, lrgllo, ipnogo, lrgs, lrgllo, ipnogo
          end if
       else
          write(itape1) lrgs, lrgllo, ipnogo, lrgs, lrgllo, ipnogo
       end if

       inext = inext + 1
       l_loop: do lrgli = lrgllo+1, lrglup

! get eigenvectors lrgli for odd symetry
          call finsym (lrgli, lrgs, iodd, jnb, ipos(inext))
          jnch = ichan(1,ipos(inext))
          jnchv(inext) = jnch
          if (jnch > 0) call getvec (ipos(inext), ivec, & 
               ivsize(inext), iposvec, jbufev)

! transform transition lrgli-1 - lrgli
          if (inch > 0 .and. jnch > 0) then  
             write (itape1) lrgs, lrgli, iodd, lrgs, lrgli-1, ieven
             nsym = nsym + 1
             ijcat = immer(inbi,ipos(inext))
             call trdip (ijcat, mvel, inbi, ipos(inext), isize,       &
                  ivsize(inext), ichan(1,inbi), ichan(:,ipos(inext)), &
                  ivec, iposvec)
          else
             write (itape1) lrgs, lrgli, ipnogo, lrgs, lrgli-1, ipnogo
          end if
!
          inext = inext + 1
          if (inext == 3) inext = 1

! get eigenvectors lrgli for even symmetry
          call finsym (lrgli, lrgs, ieven, inb, inbi)
          inch = ichan(1,inbi)
          if (inch > 0) then
             call getvec (inbi, ivec, isize, iposvec, jbufev)
          else
             write (itape1) lrgs, lrgli-1, ipnogo, lrgs, lrgli, ipnogo
             write (itape1) lrgs, lrgli, ipnogo, lrgs, lrgli, ipnogo
             cycle
          end if

! transform transition lrgli - lrgli-1
          if (jnchv(inext) > 0) then
             write (itape1) lrgs, lrgli-1, iodd, lrgs, lrgli, ieven
             nsym = nsym + 1
             ijcat = immer(inbi,ipos(inext))
             call trdip (ijcat, mvel, inbi, ipos(inext), isize,      &
                  ivsize(inext), ichan(1,inbi), ichan(:,ipos(inext)),&
                  ivec, iposvec)
          else
             write (itape1) lrgs, lrgli-1, ipnogo, lrgs, lrgli, ipnogo
          end if

! transform transition lrgli - lrgli
          inex2 = inext - 1
          if (inex2 == 0) inex2 = 2

          if (jnchv(inex2) > 0) then
             write (itape1) lrgs, lrgli, iodd, lrgs, lrgli, ieven
             nsym = nsym + 1
             ijcat = immer(inbi,ipos(inex2))
             call trdip (ijcat, mvel, inbi, ipos(inex2), isize,      &
                  ivsize(inex2), ichan(1,inbi), ichan(:,ipos(inex2)),&
                  ivec, iposvec)
          else
             write (itape1) lrgs, lrgli, ipnogo, lrgs, lrgli, ipnogo
          end if
       end do l_loop
    end do spin_loop
!
    if (nsym /= intr) then
       write (fo,'(a,2i10)') 'stdip error (nsym /= intr): nsym, &
            &intr =', nsym, intr
       stop
    end if

    close (itape1)
    close (itape3)
    stats = 'scratch'
    call close (jbufev, stats)
    stats = 'keep'
    call close (jbufim, stats)
    call close (jbuffm, stats)
  end subroutine stdip

  subroutine filestart (icat, ival, ipos, jbuf)
! use filehand to position a file for data read-in
    use filehand, only: lookup, moveb, movef, startr
    integer, intent(in)    :: icat ! catologue number
    integer, intent(in)    :: ival ! reqd posn label
    integer, intent(inout) :: ipos ! current position label
    integer, intent(in)    :: jbuf ! unit number of data file
    integer                :: nsrec, nsbuf

    call lookup (icat, nsrec, nsbuf) ! cat entry -> rec, posn
    if (ival > ipos) then
       call movef (jbuf, nsrec, nsbuf) ! skip forward
    else
       call moveb (jbuf, nsrec, nsbuf) ! skip back
    end if
    call startr (jbuf, nsrec, nsbuf) ! initiate read at rec, posn
    ipos = ival
  end subroutine filestart

  subroutine getvec (inbv, ivec, ny, ipos, jbuf)
! find symmetry label inbv for lrgl,lrgs,ipar and then locate and 
! read in ny eigenvectors to position ieig in eigvev array.
    use filehand, only: ireada
    integer, intent(in)      :: inbv    ! symm label
    integer, intent(in)      :: ivec(:) ! catalog #s
    integer, intent(out)     :: ny      ! vector length
    integer, intent(inout)   :: ipos    ! current posn 
    integer, intent(in)      :: jbuf    ! unit #
! 
    call filestart (ivec(inbv), inbv, ipos, jbuf)
    call ireada (jbuf, ny)
  end subroutine getvec

  subroutine trdip (ijcat, mvel, inbi, jnbi, isi, jsi, inch, jchan, &
       ivec, iposvec)
! transform dipole matrix elements using the eigenvectors of initial
! and final states and stores it on unit itape3
    use debug, only: bug2
    use filehand, only: ireada, readc, readb
    use error_prt, only: alloc_error
    integer, intent(in)             :: ijcat
    integer, intent(in)             :: mvel
    integer, intent(in)             :: isi, jsi
    integer, intent(in)             :: inch
    integer, intent(in)             :: inbi, jnbi
    integer, intent(in)             :: jchan(:)  ! itdimc
    integer, intent(in)             :: ivec(:)   ! npset
    integer, intent(inout)          :: iposvec
    integer                         :: jnch, noterm
    real(wp), allocatable           :: dipsum(:), eivev(:)
    real(wp), allocatable           :: dip1(:,:), dip2(:,:)
    integer                         :: isize, jsize, ival, ipos, i1, i
    integer                         :: nraj, jnb, itot, nydum
    integer                         :: j2, status, ijsize, j1, j

! print *, SIZE(dip1)

! write info on number of channels, size of each vector
    jnch = jchan(1)   ! # channels
    noterm = jchan(2) ! # cont fns per channel
    write (itape3) noterm, isi, inch, jsi, jnch

! read dipole file
    ival = 1
    ipos = ival - 1
    call filestart (ijcat, ival, ipos, jbuffm)
    call ireada (jbuffm, isize)
    call ireada (jbuffm, jsize)
    if (jsi /= jsize .or. isi /= isize) then
       write (fo,'(a)') 'trdip: size differences between evectors and &
            &dipole matrices'
       write (fo,'(4i6)') isi, isize, jsi, jsize
       stop
    end if
    ijsize = MAX(isize,jsize)
    allocate (dipsum(ijsize), eivev(ijsize), stat=status)
    if (status /= 0) call alloc_error (status, 'trdip', 'a')

! read dipole matrix in dip
    i1 =1
    if (mvel == 2) then
       allocate (dip1(isize,jsize), dip2(isize,jsize), stat=status)
       print *,'CKHUGO ',isize,jsize
       if (status /= 0) call alloc_error (status, 'trdip', 'a')
       do i = 2, jnch+1
          nraj = jchan(i)
          call readc (jbuffm, dip1(:,i1:), 1, isize, 1, nraj, isize,&
               0)
          call readc (jbuffm, dip2(:,i1:), 1, isize, 1, nraj, isize,&
               0)
          i1 = i1 + nraj
       end do
    else
       allocate (dip1(isize,jsize), stat=status)
       if (status /= 0) call alloc_error (status, 'trdip', 'a')
       do i = 2, jnch+1
          nraj = jchan(i)
          call readc (jbuffm, dip1(:,i1:), 1, isize, 1, nraj, isize,&
               0)
          i1 = i1 + nraj
!	print *, i1 , 'RIAN'
       end do
    end if
! (number of bound configurations)
    jnb = jchan(jnch+2)
    itot = i1 + jnb - 1
    if (itot /= jsize) then
       write (fo,'(a)') 'trdip: size differences between channels and &
            &dipole matrices'
       write (fo,'(3i6)') itot, jsize
       stop
    end if


!Rian - Printing dip1 sizes
!	print *, SIZE(dip1)


!   print *,dip1(63,95),dip2(63,95),jnb
    if (jnb /= 0) then
       call readc (jbuffm, dip1(:,i1:), 1, isize, 1, jnb, isize, 0)
       print *,'MVEL ',mvel,jnb
       if (mvel == 2) call readc (jbuffm, dip2(:,i1:), 1, isize, 1, jnb,&
         isize, 0)
       print *, 'jnb, dip1(1,i1),dip2(1,i1)', jnb, dip1(1,i1),dip2(1,i1)
    end if
!
!
!
    write (fo,*) 'Pre-transformation'
    if (bug2 > 0) then
       write (fo,'(a,2i6)') 'transition ', inbi, jnbi
       write (fo,'(a)') 'length dipole matrix:'
       if (jsize /= 0) then
          do i = 1, isize
             write (fo,'(8e16.8)') (dip1(i,j),j=1,jsize)
             write (fo,'(/)')
          end do
       end if
       write (fo,'(a)') '------------'
       if (mvel == 2) then
          write (fo,'(a)') 'velocity dipole matrix:'
          if (jsize /= 0) then
             do i = 1, isize
                write (fo,'(8e16.8)') (dip2(i,j),j=1,jsize)
                write (fo,'(/)')
             end do
          end if
          write (fo,'(a)') '------------'
       end if
       write (fo,'(/)')
    end if

! treat rhs (j-eigenvectors)
    do i = 1, isize
       call filestart (ivec(jnbi), jnbi, iposvec, jbufev)
       call ireada (jbufev, nydum)
       dipsum(:jsize) = 0.0_wp
       do j2 = 1, jsize
          call readb (jbufev, eivev, 1, jsize)
! Check eigenfunction
          if ((i ==1).and.(j2==1)) then
!          print *,'Eigenvector'
!          print *,eivev
          endif
          do j1 = 1, jsize
             dipsum(j2) = dipsum(j2) + dip1(i,j1) * eivev(j1)
          end do
       end do
       do j = 1,jsize
          dip1(i,j) = dipsum(j)
       end do
    end do
! treat lhs (i-eigenvectors)
    do i = 1, jsize
       call filestart (ivec(inbi), inbi, iposvec, jbufev)
       call ireada (jbufev, nydum)
       dipsum(1:isize) = 0.0_wp
       do j2 = 1, isize
          call readb (jbufev, eivev, 1, isize)
! Check eigenfunction
          if ((i ==1).and.(j2==1)) then
!          print *,'Eigenvector'
!          print *,eivev
          endif
          do j1 = 1, isize
             dipsum(j2) = dipsum(j2) + eivev(j1) * dip1(j1,i)
          end do
       end do
       do j = 1,isize
          dip1(j,i) = dipsum(j)
       end do
    end do

    if  (mvel == 2) then  ! velocity elements required
       do i = 1, isize
          call filestart (ivec(jnbi), jnbi, iposvec, jbufev)
          call ireada (jbufev, nydum)
          dipsum(:jsize) = 0.0_wp
          do j2 = 1, jsize
             call readb (jbufev, eivev, 1, jsize)
             do j1 = 1, jsize
                dipsum(j2) = dipsum(j2) + dip2(i,j1) * eivev(j1)
             end do
          end do
          do j = 1,jsize
             dip2(i,j) = dipsum(j)
          end do
       end do
! treat lhs (i-eigenvectors)
       do i = 1, jsize
          call filestart (ivec(inbi), inbi, iposvec, jbufev)
          call ireada (jbufev, nydum)
          dipsum(1:isize) = 0.0_wp
          do j2 = 1, isize
             call readb (jbufev, eivev, 1, isize)
             do j1 = 1, isize
                dipsum(j2) = dipsum(j2) + eivev(j1) * dip2(j1,i)
             end do
          end do
          do j = 1,isize
             dip2(j,i) = dipsum(j)
          end do
       end do
    end if

! write dipole matrix : debugging print initial as fastest moving
    write (fo,*) 'After transformation'
    if (bug2 > 0) then
       write (fo,'(a,2i6)') 'transition ', inbi, jnbi
       write (fo,'(a)') 'length dipole matrix:'
       if (jsize /= 0) then
          do i = 1, isize
             write (fo,'(8e16.8)') (dip1(i,j),j=1,jsize)
             write (fo,'(/)')
          end do
       end if
       write (fo,'(a)') '------------'
       if (mvel == 2) then
          write (fo,'(a)') 'velocity dipole matrix:'
          if (jsize /= 0) then
             do i = 1, isize
                write (fo,'(8e16.8)') (dip2(i,j),j=1,jsize)
                write (fo,'(/)')
             end do
          end if
          write (fo,'(a)') '------------'
       end if
       write (fo,'(/)')
    end if
    
! store dipole matrix treated
    write (itape3) dip1
    if (mvel == 2) then
       write (itape3) dip2
       deallocate (dip2, stat=status)
       if (status /= 0) call alloc_error (status, 'trdip', 'd')
    end if
    deallocate (dipsum, eivev, dip1, stat=status)
    if (status /= 0) call alloc_error (status, 'trdip', 'd')
  end subroutine trdip
    
  subroutine finsym (lrgl, lrgs, lrgp, inb, inbi)
! find the (n+1) set and the number of bound configurations
    use sc_ham, only: npset, npcset
    integer, intent(in)         :: lrgl
    integer, intent(in)         :: lrgs
    integer, intent(out)        :: inb    ! # cfgs
    integer, intent(out)        :: inbi   ! symmetry set #
    integer                     :: i, lrgp

    inb = 0
    inbi = 0
    do i = 1, npset
       if (lrgl == lpset(i) .and. lrgp == ippset(i) .and. &
            lrgs == ipsset(i)) then
          inbi = i
          inb = npcset(i)
          return
       end if
    end do
    write (fo,'(a)') 'finsym: (N+1) symmetry not found'
    stop
  end subroutine finsym
end module dipoles
