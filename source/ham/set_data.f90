module set_data
! Time-stamp: "02/02/01 07:21:23 cjn"
  use io_units, only: fi, fo
  implicit none

!sets:
  integer, save               :: nset   
! number of target sets (L_t S_t \pi_t)
  integer, allocatable, save  :: lset(:)  
! l-value L_t for each set 
  integer, allocatable, save  :: isset(:)
! spin multiplicity for each set 
  integer, allocatable, save  :: ipset(:) 
! parity (even or odd 0,1) for each set 

! state:
  integer, save                :: nast     
! total number of target states
  integer, allocatable, save   :: nstat(:) 
! number of states for each set (1:nset)
  integer, save                :: isplo    ! lowest target spin multiplicity
  integer, save                :: ispup    ! highest target spin multiplicity

! rad:
  integer, save                :: lrang1   ! lrang1 = 1 + max target orbital l_t
! **  not set in this module's routines: the 'rad_data' value is used throughout
  integer, save                :: lrang3   ! lrang3 = 1 + max target L_t
  integer, save                :: llpdim   ! 2 * lrang3 + 1: 
! in the high total L= lrgl limit, continuum l ranges from lrgl - (lrang3-1) 
!                                                       to lrgl + (lrang3-1)
 
  integer, save                :: spn   !  max target spin multiplicity

  integer, save                :: nss   ! max value of nstat(:)

  private
  public   rd_set_data
  public   nset, lset, isset, ipset, nast, nstat, nss
  public   isplo, ispup, lrang1, llpdim, lrang3, spn

contains

  subroutine rd_set_data (nset_tmp, lrgslo, lrgsup)
    use error_prt, only: alloc_error
    integer, intent(in)        :: nset_tmp   
! nset fron rm_data, used to set this module's nset
    integer, intent(inout)     :: lrgslo, lrgsup  
! lower and upper total spin multiplicities: see conditions below
    integer                    :: is, isran1, isran2, i, status

    nset = nset_tmp
    allocate (lset(nset), isset(nset), ipset(nset), nstat(nset), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'rd_set_data', 'a')

    nast = 0
    do is = 1, nset
       read (fi,*) lset(is), isset(is), ipset(is), nstat(is)
       nast = nast + nstat(is)
    end do
    lrang3 = 0
    isran2 = 0
    isran1 = 99
    do i = 1, nset
       lrang3 = MAX(lset(i), lrang3)
       isran1 = MIN(isset(i), isran1)
       isran2 = MAX(isset(i), isran2)
    end do
    lrang3 = lrang3 + 1
    llpdim = lrang3 + lrang3 - 1
    nss = MAXVAL(nstat)
! set range of total spin multiplicities if input values request this:
    if (lrgslo < 0 .and. lrgsup == 0) then  ! note double condition
       lrgslo = isran1 - 1
       if (lrgslo == 0) lrgslo = 2
       lrgsup = isran2 + 1
    end if
    if (MOD(lrgslo+isran1, 2) == 0 .or. MOD(lrgsup+isran2, 2) == 0) &
         then
       write (fo,'(a,i3,a,i3)') 'rd_set_data error: lrgslo = ', lrgslo,&
            ' lrgsup = ', lrgsup
       stop
    end if
    isplo = isran1
    ispup = isran2
    spn = isran2
  end subroutine rd_set_data
end module set_data
