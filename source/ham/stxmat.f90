module exchange_Hamiltonian
! Time-stamp: "2005-02-04 10:33:13 dlcjn1"
  use precisn, only: wp
  use io_units, only: fo, jbuff4, jbuff5, jbuffe, jbufd, jbufie, itape1
  use debug, only: bug5, bug6
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save   :: ninj(:,:) ! square->triangle index map

  integer, allocatable, save   :: iecont(:) ! integral pointer data
  real(wp), allocatable, save  :: rkcc(:)   ! RK exchange integrals
  integer, allocatable, save   :: irhsgl(:) ! ANG index data
  real(wp), allocatable, save  :: vsh(:)    ! ANG coefficient

  private
  public stxmat

contains
  subroutine stxmat (ilpi, lbufd)
! Calculate exchange terms in the Hamiltonian matrix
! Exchange + relevant direct term stored on disc (records nrang2xnrang2)
    use stghrd_mod, only: lrglup, nrang1
    use z_coeffs, only: genze, zc, kilze
    use channels, only: nchset, iconch, nschan, nsprec
    use l_data, only: lrgl, npty, ltot, lval, nscol, nsetl
    use rad_data, only: lrang1, lrang2, nvary
    use rm_data, only: lamax, nrang2
    use set_data, only: nset, lset, isset, nstat, llpdim, &
         lrang3, nss
    use accdi, only: ncset
    use comp_data, only: njcomp, ljcomp
    use states, only: nsetst, aij
    use filehand, only: readix, ireada, ireadb, lookup, movef, moveb,&
         startr, readb
    integer, intent(in)        :: ilpi
    integer, intent(in)        :: lbufd    ! record length for jbufd
    logical                    :: nzero
    integer        :: ictcce(lrang1,lrang1,(lrang1+lrang3+lrglup)/2)
    integer, allocatable, save :: iccest(:,:)
    integer, allocatable,save  :: ncce(:,:)
    real(wp)                   :: xmat(nrang2*nrang2,2,2)
    real(wp)                   :: xmats(nrang2*nrang2)
    real(wp)                   :: hsmat(nrang2*nrang2*nss*nss,2)
    real(wp)                   :: bufd(lbufd), buf1d(lbufd)
    integer                    :: lrgse(2), nche(2)
    integer                    :: nsrece(2), ijrece(2)
    integer                    :: nra12, lrglpi, ns, nset2, status
    integer                    :: ifora, iforr, kdim, ikl1, kl, ikls1, l
    integer                    :: nra1, klp, ikls2, lp, nra2, nrasqr
    integer                    :: icclng, ldif, nrecic, iccdif, nrke
    integer                    :: nrec1, nbuf1, nrec, nbuf, i, ikl2, is
    integer                    :: ichan1, lcfg, isp, nch, nsrec, jlo, j
    integer                    :: js, jchan1, lcfgp, jsp, kis, kjs, nsp
    integer                    :: lrgs, ki, icont1, icont2, nvshel
    integer                    :: kisjs, icount, kics, jcslo, kjcs
    integer                    :: nhalf, irsks, nshhun, nsh, jcs
    integer                    :: nrsks, irho, lrho, nrho, irs, irhop
    integer                    :: lrhop, nrhop, ico, nh, istat, jstlo
    integer                    :: jchann, ih, ispe, in, n2, n3, n
    integer                    :: np, n1, jn, ihs, ichan2, ib, ics
    integer                    :: jchlo, jch, ijrecd, ichs, jchs, ish
    integer                    :: ichann, ist, jst, jstat, jchan2, ich
    integer                    :: ichcon, jchcon, ihs1, ijrec
    real(wp)                   :: ai, ai2, aj, aiaj, ajai, xin
    real(wp), pointer          :: z1(:,:), z2(:,:)

    lrglpi = lrgl + lrgl + npty

! at first entry read pointer arrays for rk and angular integrals
    if (ilpi == 1) then
       allocate (ncce(llpdim,lrang2), iccest(llpdim,lrang2), &
            stat=status)
       if (status /= 0) call alloc_error (status, 'exchange1', 'a')
       read (itape1) ncce, iccest
       call readix (jbufie)
       call ireada (jbufie, ns)
       nset2 = (nset * (nset + 1)) / 2 + 1
       if (allocated(iecont)) then
          deallocate (iecont, stat=status)
         if (status /= 0) call alloc_error (status, 'exchange1', 'd')
       end if
       allocate (iecont(nset2), stat=status)
         if (status /= 0) call alloc_error (status, 'exchange2', 'a')
       call ireadb (jbufie, iecont, 1, nset2)
       if (bug6 == -2) then
          write (fo,'(a,/(10i6))') 'iecont', iecont
          write (fo,'(a,/(10i6))') ' ncce', ncce
       end if
       ifora = 0
       allocate (ninj(nrang1,nrang1), stat=status)
       if (status /= 0) call alloc_error (status, 'exchange3', 'a')
       call triang
    else
       ifora = -1
    end if
    iforr = 1

    if (ltot == 0) return

    call genze   ! Z-coefficients for current lrgl, pi

!   kdim = (lrang1 + lrglup + lrang3) / 2
    kdim = (lrang1 + lrang2) / 2

    ikl1 = 0 ! count for locating Z-coefficients
    continuum_l: do kl = 1, ltot
       ikls1 = ikl1
       l = lval(kl)
       nra1 = nvary(l+1)
       nra12 = (nra1 * (nra1 + 1)) / 2
       continuum_lp: do klp = kl, ltot
          if (kl == klp) then
             ikls2 = ikls1
          else
             ikls2 = ikl2
          end if
          ikl1 = ikls1
          lp = lval(klp)
          nra2 = nvary(lp+1)
          nrasqr = nra1 * nra2

! read CC RK integral pointers for l, lp combination
          icclng = nrasqr
          ldif = lp - l + 1
          nrecic = l * llpdim + ldif
          read (jbuff5, rec=nrecic) ictcce(1:lrang1,1:lrang1,1:kdim)
          iccdif = iccest(ldif, l+1)   ! l, l' filehand index value
          nrke = ncce(ldif,l+1)        ! # integrals
          call lookup (iccdif, nrec1, nbuf1)
          nrec = nrec1
          nbuf = nbuf1
          if (nrec < 2) then
             write (fo,'(a)') 'stxmat: error finding RK integral start'
             write (fo,'(4x,8i4)') l, lp, nrec, nbuf, nrecic, ldif, &
                  llpdim, iccdif
             stop
          end if
          if (iforr > 0) then
             call movef (jbuff4, nrec, nbuf)
          else if (iforr == 0) then
             call startr (jbuff4, nrec, nbuf)
          else
             call moveb (jbuff4, nrec, nbuf)
          end if
          if (ALLOCATED(rkcc)) then
             deallocate (rkcc, stat=status)
             if (status /= 0) call alloc_error (status, 'exchange', 'd')
          end if
          allocate (rkcc(nrke), stat=status)
          if (status /= 0) call alloc_error (status, 'exchange', 'a')
          call readb (jbuff4, rkcc, 1, nrke) ! read exchange integrals
          iforr = 1

          i_sym: do i = 1, nscol(kl) ! sets coupled to l
             if (l == lp) then
                ikl2 = ikl1
             else
                ikl2 = ikls2
             end if
             ikl1 = ikl1 + 1
             is = nsetl(i, kl)        ! symmetry set #
             ichan1 = nchset(is,kl)   ! initial channel for symm
             lcfg = lset(is)          ! L_i
             isp = isset(is)         ! S_i  target spin
             nch = nschan(isp)        ! # channels
             nsrec = nsprec(isp) - 1  ! output starting record

             jlo = 1
             if (l == lp) jlo = i

             j_sym: do j = jlo, nscol(klp)   ! sets coupled to l'
                ikl2 = ikl2 + 1
                js = nsetl(j,klp)             ! symmetry seet #
                jchan1 = nchset(js,klp)       ! initial channel for symm
                lcfgp = lset(js)              ! L_j
                jsp = isset(js)              ! S_j

                if (is > js) then ! is, js sets must be reversed
                   kis = js
                   kjs = is
                else
                   kis = is
                   kjs = js
                end if

! spin:
                if (ABS(isp-jsp) >  2) cycle j_sym ! spins dont couple
                if (isp == 1 .or. jsp == 1) then ! one a spin singlet
                   nsp = 1                       ! one coupling only
                   lrgse(1) = 2                  ! N+1 spin multiplicity
                   nsrece(1) = nsprec(2) - 1
                   nche(1) = nschan(2)
                else if (isp /= jsp) then ! spins differ
                   nsp = 1                ! one spin coupling
                   lrgs = MIN(isp, jsp) + 1 ! S
                   lrgse(1) = lrgs          ! N+1 elec spin multiplicity
                   nsrece(1) = nsprec(lrgs) - 1
                   nche(1) = nschan(lrgs)
                else       ! ccfgs have identical spins
                   nsp = 2 ! two possible couplings
                   lrgs = isp - 1
                   do ki = 1, 2
                      lrgse(ki) = lrgs      ! N+1 ccfg spins
                      nsrece(ki) = nsprec(lrgs) - 1
                      nche(ki) = nschan(lrgs)
                      lrgs = isp + 1
                   end do
                end if

! ANG data:
                kisjs = nset * (kis-1) - (kis*(kis-1))/2 + kjs
                icont1 = iecont(kisjs)
                icont2 = iecont(kisjs+1) - 1
                nvshel = icont2 - icont1 + 1
                call lookup (icont1, nrec1, nbuf1)
                nrec = nrec1
                nbuf = nbuf1
                if (ifora > 0) then
                   call movef (jbufie, nrec, nbuf)
                   call movef (jbuffe, nrec, nbuf)
                else if (ifora == 0) then
                   call startr (jbufie, nrec, nbuf)
                   call startr (jbuffe, nrec, nbuf)
                else
                   call moveb (jbufie, nrec, nbuf)
                   call moveb (jbuffe, nrec, nbuf)
                end if
                if (ALLOCATED(irhsgl)) then
                   deallocate (irhsgl, stat=status)
                   if (status /= 0) call alloc_error (status, 'exchange', 'd')
                end if
                if (ALLOCATED(vsh)) then
                   deallocate (vsh, stat=status)
                   if (status /= 0) call alloc_error (status, &
                        'exchange', 'd')
                end if
                allocate (irhsgl(nvshel), vsh(nvshel), stat=status)
                if (status /= 0) call alloc_error (status, &
                     'exchange', 'a')
                call ireadb (jbufie, irhsgl, 1, nvshel) ! read ANG ptrs
                call readb (jbuffe, vsh, 1, nvshel) ! read ANG coeffs
                ifora = 1      ! filehand direction switch
                icount = 1     ! vsh use counter

                hsmat = 0.0_wp

                i_ccfgs: do kics = 1, ncset(kis) ! cfgs in set kis
                   if (kis == kjs) then
                      jcslo = kics
                   else
                      jcslo = 1
                   end if
                   j_ccfgs: do kjcs = jcslo, ncset(kjs) ! cfgs in kjs

                      nhalf = 1
! if tgt symms equal, ang. ints. stored only for upper RH of cfg matrix
! Then two terms to obtain and multiply with mixing parameters aij, aji
                      if (kis == kjs .and. kics /= kjcs) nhalf = 2

                      nzero = .true.
                      irsks = irhsgl(icount)
                      if (irsks == 9999) then
                         icount = icount + 1
                         nzero = .false.
                      end if
                      icclng = nrasqr
                      if (irsks < 0) then ! same tgt shell structure
                         if (l == lp) icclng = nra12 ! triangular
                         nrsks = - irsks
                         nsh = nrsks / 10000 ! # shells
                         nshhun = nsh * 100
                         xmat(1:icclng,1:nsp,1:nhalf) = 0.0_wp
                         shells: do ish = 1, nsh
                            nrsks = ABS(irhsgl(icount))
                            irho = nrsks / 100 - nshhun
                            lrho = ljcomp(irho)  ! l_rho
                            nrho = njcomp(irho)  ! l_rho'
                            z1 => zc(lrho+1,ikl2)
                            z2 => zc(lrho+1, ikl1)
                            call xmatij (l, lp, lrho, lrho, nrho, nrho,&
                                 nhalf, icount,lcfg, lcfgp, &
      nsp,nrasqr,nra12,iccdif,ictcce,lrang1,lrang3,lrglup,xmat,nrang2, &
      lrho,lrhop,ikl1,ikl2,1)
                         end do shells
                      else if (irsks /= 9999) then ! different structure
                         irs = irsks / 100
                         irho = irs / 100
                         lrho = ljcomp(irho)
                         nrho = njcomp(irho)
                         irhop = MOD(irs, 100)
                         lrhop = ljcomp(irhop)
                         nrhop = njcomp(irhop)
                         xmat(1:icclng,1:nsp,1:nhalf) = 0.0_wp

! if sets were interchanged rho and rhop must be changed back
! if sets are equal and both orbital and continuum a.m. are different
! two calls are required to xmatij to obtain cfgs for aij and aji
                         if (nhalf == 2 .and. l /= lp .and. lrho /= &
                              lrhop) then ! only n values to swap
                            ico = icount  ! save ANG use count
                            nh = -1       ! do not reverse n-values
                            z1 => zc(lrho+1, ikl2)
                            z2 => zc(lrhop+1, ikl1)
                            call xmatij (l, lp, lrho, lrhop, nrho,     &
                                 nrhop, nh, icount,lcfg, lcfgp, &
      nsp,nrasqr,nra12,iccdif,ictcce,lrang1,lrang3,lrglup,xmat,nrang2, &
      lrho,lrhop,ikl1,ikl2,2)
                            icount = ico  ! restore icount value
                            nh = - 2 ! second case, rho, rhop reversed
                            z1 => zc(lrhop+1, ikl2)
                            z2 => zc(lrho+1, ikl1)
                            call xmatij (l, lp, lrhop, lrho, nrhop,    &
                                 nrho, nh, icount, lcfg, lcfgp,&
      nsp,nrasqr,nra12,iccdif,ictcce,lrang1,lrang3,lrglup,xmat,nrang2, &
      lrho,lrhop,ikl1,ikl2,3)
                         else if (is <= js) then ! symms in order
                            z1 => zc(lrho+1, ikl2)
                            z2 => zc(lrhop+1, ikl1)
                            call xmatij (l, lp, lrho, lrhop, nrho,     &
                                 nrhop, nhalf, icount, lcfg, lcfgp, &
      nsp,nrasqr,nra12,iccdif,ictcce,lrang1,lrang3,lrglup,xmat,nrang2, &
      lrho,lrhop,ikl1,ikl2,4)
                         else   ! symmetries have been reversed
                            z1 => zc(lrhop+1, ikl2)
                            z2 => zc(lrho+1, ikl1)
                            call xmatij (l, lp, lrhop, lrho, nrhop,    &
                                 nrho, nhalf, icount, lcfg, lcfgp, &
      nsp,nrasqr,nra12,iccdif,ictcce,lrang1,lrang3,lrglup,xmat,nrang2, &
      lrho,lrhop,ikl1,ikl2,5)
                         end if
                      end if

                      if (is > js) then ! restore symmetry order
                         ics = kjcs
                         jcs = kics
                      else              ! symms were not reversed
                         ics = kics
                         jcs = kjcs
                      end if

                      ihs = 0
                      ichann = ichan1   ! initial i-channel
                      i_tgts: do ist = 1, nstat(is) ! tgts from set is
                         istat = nsetst(is, ist)    ! state #
                         ai = aij(istat,ics)
! if is = js ccfg pair contributes twice, compensate for symmetry saving
! but the if statement results in undefined variables...
!                        if (nhalf == 2) ai2 = aij(istat,jcs)
                         ai2 = aij(istat,jcs)

                         if (ichan1 == jchan1) then
                            jstlo = ist
                            jchann = ichann
                         else
                            jstlo = 1
                            jchann = jchan1
                         end if

                         j_tgts: do jst = jstlo, nstat(js)
                            if (.NOT.nzero) then ! no coupling
                               jchann = jchann + 1
                               cycle
                            end if
                            jstat = nsetst(js,jst)
                            aj = aij(jstat,jcs)
                            aiaj = ai * aj
!                           if (nhalf == 2) ajai = ai2 * aij(jstat,ics)
!HUGO Remove the if statement to avoid uninitialised variables
                            ajai = ai2 * aij(jstat,ics)

                            ih_loop: do ih = 1, nhalf
                               total_spin: do ispe = 1, nsp
                                  lrgs = lrgse(ispe) ! total spin, S

                                  if (icclng == nra12) then ! fill square
                                     in = 0
                                     n3 = - nra1
                                     do n = 1, nra1
                                        n3 = n3 + nra1
                                        n2 = n - nra1
                                        do np = 1, n
                                           in = in + 1
                                           n1 = n3 + np
                                           n2 = n2 + nra1
                                           xin = xmat(in,ispe,ih)
                                           xmats(n1) = xin
                                           xmats(n2) = xin
                                        end do
                                     end do
                                  else if (iconch(jchann,lrgs) < &
                                       iconch(ichann,lrgs)) then
! channels in RM order are reversed:
                                     in = 0
! FUDGE BY HUGO
                                     if (nra1 >= nra1) then
!                                    if (nra1 >= nra2) then
                                        do n = 1, nra1
                                           do np = 1, nra2
                                              in  = in + 1
                                              jn = (np-1) * nra1 + n
                                              xmats(jn) = xmat(in,ispe,ih)
                                           end do
                                        end do
                                     else  ! nra1 < nra2
                                        do n = 1, nra2
                                           do np = 1, nra1
                                              in = in + 1
                                              jn = (np-1) * nra2 + n
                                              xmats(jn) = xmat(in,ispe,ih)
                                           end do
                                        end do
                                     end if
                                  else    ! simple copy required
                                     do in = 1, icclng
                                        xmats(in) = xmat(in,ispe,ih)
                                     end do
                                  end if

                                  do n = 1, nrasqr
                                     hsmat(n+ihs,ispe) =      &
                                          hsmat(n+ihs,ispe) + &
                                          aiaj * xmats(n)
                                  end do
                               end do total_spin
                               aiaj = ajai
                            end do ih_loop

                            ihs = ihs + nrasqr
                            jchann = jchann + 1  ! increment j-channel
                         end do j_tgts
                         ichann = ichann + 1     ! increment i-channel
                      end do i_tgts

                   end do j_ccfgs
                end do i_ccfgs
                if (icount /= nvshel + 1) then ! not all vsh used
                   write (fo,'(a,2i4)') 'stxmat: error using vsh for &
                        &sets ', kis, kjs
                   stop
                end if

! write hsmat to disk; include asymp coeffs for each lambda
                ichan2 = ichann - 1
                jchan2 = jchann - 1
                ihs = 0

                if (isp == jsp) then ! equal spins, add in direct terms
                   i_channels: do ich = ichan1, ichan2
                      ichcon = iconch(ich,isp) ! channel # RM conversion
                      if (ichan1 == jchan1) then
                         jchlo = ich
                      else
                         jchlo = jchan1
                      end if
                      j_channels: do jch = jchlo, jchan2
                         jchcon = iconch(jch,isp)

! calculate record # so that the matrices are stored in sequence
                         if (jchcon < ichcon) then
                            ijrecd = nsrec + nch*(jchcon-1) - &
                                 (jchcon*(jchcon-1))/2 + ichcon
                            read (jbufd,rec=ijrecd) buf1d ! direct els
                            t_spins1: do ispe = 1, nsp
                               lrgs = lrgse(ispe)
                               ichs = iconch(ich,lrgs)
                               jchs = iconch(jch,lrgs)
                               ijrece(ispe) = nsrece(ispe) + &
                                    nche(ispe) * (jchs-1)-   &
                                    (jchs*(jchs-1))/2 + ichs
                            end do t_spins1
                         else   ! jchcon >= ichcon
                            ijrecd = nsrec + nch*(ichcon-1) - &
                                 (ichcon*(ichcon-1))/2 + jchcon
                            read (jbufd,rec=ijrecd) buf1d
                            t_spins2: do ispe = 1, nsp
                               lrgs = lrgse(ispe)
                               ichs = iconch(ich,lrgs)
                               jchs = iconch(jch,lrgs)
                               ijrece(ispe) = nsrece(ispe) +  &
                                    nche(ispe)*(ichs-1) - &
                                    (ichs*(ichs-1))/2 + jchs
                            end do t_spins2
                         end if

                         bufd(:lamax) = buf1d(:lamax) ! cf coeffs

                         ihs1 = ihs
                         t_spins: do ispe = 1, nsp
                            ihs = ihs1
                            do ib = 1+lamax, nrasqr+lamax
                               ihs = ihs+1
                               bufd(ib) = buf1d(ib) - hsmat(ihs,ispe)
                            end do
                            write (jbufd,rec=ijrece(ispe)) bufd
                         end do t_spins
                      end do j_channels
                   end do i_channels

                else ! target spins not equal so no direct terms to add

                   i_channels2: do ich = ichan1, ichan2
                      lrgs = lrgse(1)
                      ichs = iconch(ich,lrgs)
                      j_channels2: do jch = jchan1, jchan2
                         jchs = iconch(jch,lrgs)
                         if (jchs < ichs) then ! RM record number
                            ijrec = nsrece(1) + nche(1) * &
                                 (jchs-1) - (jchs*(jchs-1))/2 + ichs
                         else
                            ijrec = nsrece(1) + nche(1) * &
                                 (ichs-1) - (ichs*(ichs-1))/2 + jchs
                         end if

                         bufd(1:lamax) = 0.0_wp ! zero asymptotic coeffs

                         do ib = 1+lamax, nrasqr+lamax
                            ihs = ihs + 1
                            bufd(ib) = -hsmat(ihs,1) ! no direct terms
                         end do
                         write (jbufd,rec=ijrec) bufd
                      end do j_channels2
                   end do i_channels2
                end if
             end do j_sym
          end do i_sym
          ifora = - 1   ! filehand direction switch - back
       end do continuum_lp
    end do continuum_l
    call kilze    ! delete Z-coefficient table
    end subroutine stxmat

    subroutine xmatij (lt, ltp, lrh, lrhp, nrh, nrhp, nhalf, icount, lcfg, lcfgp, &
      nsp,nrasqr,nra12,iccdif,ictcce,lrang1,lrang3,lrglup,xmat,nrang2, &
      lrhq,lrhqp,ikl1,ikl2,icase)
! sums exchange contribn to cplg between pair of shells over kappa,lambda
!      use rm_data, only: nrang2
!      use rad_data, only: maxnhf, maxnc, lrang1
      use rad_data, only: maxnhf, maxnc
!      use set_data, only: lrang3
      use l_data, only: lrgl
      use z_coeffs, only: genze, zc, kilze
      integer, intent(in)    :: lt, ltp    ! continuum l-values
      integer, intent(in)    :: lrh, lrhp  ! surfaced electron l-values
      integer, intent(in)    :: nrh, nrhp  ! surfaced electron n-values
      integer, intent(in)    :: nhalf      ! swap nrh, nrhp switch
      integer, intent(inout) :: icount     ! count of used ANG coeffs
      integer, intent(in)    :: lcfg, lcfgp
      integer, intent(in)    :: nsp,nrasqr,nra12,iccdif
      integer, intent(in)    :: lrang1,lrang3,lrglup,nrang2,icase
      integer, intent(in)    :: lrhq,lrhqp,ikl1,ikl2
      real(wp), intent(inout):: xmat(nrang2*nrang2,2,2)
      integer, intent(in)    :: ictcce(lrang1,lrang1,(lrang1+lrang3+lrglup)/2)
      real(wp)               :: rk(nrang2*nrang2,lrang1)
      real(wp)               :: b(lrang1+lrang3,2)
      real(wp)               :: blam(lrang1,2)
      real(wp)               :: temp(nrang2*nrang2)
      integer                :: lrho, lrhop, nrho, nrhop, l, lp, kapmin
      integer                :: kapmax, lam1, lam2, lammin, klamin
      integer                :: kap, isp, klamd, lam, kaplo, kapup
      integer                :: klam2, kapp, klamdx, nh, icclng, lrho1
      integer                :: lrhop1, maxncp, ni, nj, nij, icce, klam1
      integer                :: itran, n, nra1, i, nhs, nstart, lamup
      real(wp), pointer          :: z1(:,:), z2(:,:)

      if ((icase.le.0).or.(icase.gt.5)) then
       print *,'ICASE incorrect'
       stop
      endif
      if (icase.eq.1) then
       z1 => zc(lrhq+1,ikl2)
       z2 => zc(lrhq+1, ikl1)
      endif
      if (icase.eq.2) then
       z1 => zc(lrhq+1, ikl2)
       z2 => zc(lrhqp+1, ikl1)
      endif
      if (icase.eq.3) then
                            z1 => zc(lrhqp+1, ikl2)
                            z2 => zc(lrhq+1, ikl1)
      endif
      if (icase.eq.4) then
                            z1 => zc(lrhq+1, ikl2)
                            z2 => zc(lrhqp+1, ikl1)
      endif
      if (icase.eq.5) then
                           z1 => zc(lrhqp+1, ikl2)
                            z2 => zc(lrhq+1, ikl1)
      endif

     

      lrho = lrh
      lrhop = lrhp
      nrho = nrh
      nrhop = nrhp
      l = lt
      lp = ltp
      kapmin = MAX(ABS(lcfg-lrhop), ABS(lcfgp-lrho))
      kapmax = MIN(lcfg+lrhop, lcfgp+lrho)
      if (kapmin > kapmax) then ! no sum over kappa
         icount = icount + 1
         return
      end if
      
      lam1 = ABS(lrho-lp)
      lam2 = ABS(lrhop-l)
      lammin = MAX(lam1, lam2)
      klamin = lammin / 2
      lamup = MIN(lrho+lp, lrhop+l)
      do kap = kapmin, kapmax
         do isp = 1, nsp
            b(kap+1,isp) = vsh(icount) ! ANG coefs -> b
            icount = icount + 1
         end do
      end do
      
      ANG_coefs0: do isp = 1, nsp    ! perform kappa sum
         klamd = 0
         lambda0: do lam = lammin, lamup, 2
            klamd = klamd + 1
            blam(klamd,isp) = 0.0_wp         
            kaplo = MAX(kapmin, ABS(lrgl-lam))
            kapup = MIN(kapmax, lrgl+lam)
            klam1 = (lam - lam1)/2 + 1
            klam2 = (lam - lam2)/2 + 1
            kappa0: do kapp = kaplo+1, kapup+1
               blam(klamd,isp) = blam(klamd,isp) + b(kapp,isp) * &
                    z1(kapp,klam1) * z2(kapp,klam2)
            end do kappa0
         end do lambda0
      end do ANG_coefs0
      klamdx = klamd
      
      icclng = nrasqr     ! icclng = # radial integrals
      if (l == lp .and. lrho == lrhop .and. nrho == nrhop) icclng = nra12
      lrho1 = lrho + 1
      lrhop1 = lrhop + 1
      maxncp = maxnc(lrhop1) ! # core orbitals for lrhop

      nhs = 1
      if (nhalf < 0) nhs = ABS(nhalf)
      nh_loop: do nh = nhs, ABS(nhalf)
         ni = nrho - maxnc(lrho1) ! cortex nrho value
         nj = nrhop - maxncp      ! cortex nrhop value
         if (l == lp  .and.  lrho == lrhop) then
            nij = ninj(ni,nj)*nrasqr + (MAX(ni,nj)-1)*nra12 - iccdif
         else
            nij = (ni-1) * (maxnhf(lrhop1)-maxncp) + nj
            nij = (nij-1) * nrasqr - iccdif  ! ni,nj base address
         end if
         
         lamda_1: do klamd = 1, klamdx
            icce = ictcce(lrho1,lrhop1,klamin+klamd) ! ni,nj,lambda ptr
            if (icce == -999) then
               write (fo,'(a)') 'xmatij: no pointer in ictcce:'
               write (fo,'(2(a,i4))') 'lrho = ', lrho, ' lrhop = ', lrhop
               write (fo,'(2(a,i4))') 'klam = ', klamin+klamd
            end if
            nstart = nij + icce  ! integral block base address
            if (nstart < 0) then
               write (fo,'(a)') 'xmatij: incorrect pointer in ictcce:'
               write (fo,'(2(a,i4))') 'l    = ', l,    ' lp    = ', lp
               write (fo,'(2(a,i4))') 'lrho = ', lrho, ' lrhop = ', lrhop
               write (fo,'(2(a,i4))') 'nrho = ', nrho, ' nrhop = ', nrhop
               write (fo,'(2(a,i4))') 'klam = ', klamin+klamd
               write (fo,'(a,i5)') 'starting position = ', nstart
               write (fo,'(a,7i8)') 'lcfg, lcfgp, kapmin, kapmax, nsp, &
                    &lammin,lamup: ', lcfg, lcfgp, kapmin, kapmax, nsp,&
                    lammin, lamup
               write (fo,'(a,6i8)') 'icclng, maxncp, ni, nj, nij, icce: ', &
                    icclng, maxncp, ni, nj, nij, icce
               write(fo,'(a,4i8)') 'lrhop1, maxnhf(lrhop1), nrasqr, &
                    &iccdif: ', lrhop1, maxnhf(lrhop1), nrasqr, iccdif
               stop
            end if

            do n = 1, icclng
               rk(n,klamd) = rkcc(nstart+n)
            end do
         end do lamda_1

         itran = 0  ! resset to itran = 1 if RK matrix is transposed
         if (l == lp) then
            if (lrho == lrhop .and. nrho < nrhop) itran = 1
            if (lrho > lrhop) itran = 1
         end if
            
         do isp = 1, nsp
            do klamd = 1, klamdx
               do n = 1, icclng
                  xmat(n,isp,nh) = xmat(n,isp,nh) + blam(klamd,isp) * &
                       rk(n,klamd)
               end do
            end do
         end do
         
! if interactg tgt symms are the same make use of the symm of ang terms
! unless lrho /= lrhop .and. l /= lp
         if (nh < nhalf) then
            if (nrho == nrhop .and. lrho == lrhop) then
               do isp = 1, nsp
                  do n = 1, icclng
                     xmat(n,isp,2) = xmat(n,isp,1)
                  end do
               end do
            else if (l == lp) then
               nra1 = NINT(SQRT(REAL(icclng,wp)))
               do isp = 1, nsp
                  if (itran == 1) then ! transpose required
                     do n = 1, icclng
                        xmat(n,isp,2) = xmat(n,isp,1)
                     end do
                     call trans (xmat(:,isp:,2), xmat(:,isp:,1), nra1)
                  else
                     call trans (xmat(:,isp:,1), xmat(:,isp:,2), nra1)
                  end if
               end do
            else if (lrho == lrhop) then ! swap nrho, nrhop values
               nrho = nrhp
               nrhop = nrh
               cycle nh_loop
            else
               return
            end if
         end if
         exit
      end do nh_loop
      
      if (nhalf == 1 .and. itran == 1) then
         nra1 = NINT(SQRT(REAL(icclng,wp)))
         do isp = 1, nsp
            do i = 1, icclng
               temp(i) = xmat(i,isp,1)
            end do
            call trans (temp, xmat(:,isp:,1), nra1)
         end do
      end if
    end subroutine xmatij

  subroutine triang
! Define ninj, giving upper triangle index for square indices i,j
    use stghrd_mod, only: nrang1
    integer               :: i, j, k

    k = 0
    ninj(1,1) = 0
    do i = 1, nrang1-1
       do j = 1, i
          k = k + 1
          ninj(i+1,j+1) = k
          ninj(j+1,i+1) = k
       end do
       ninj(i+1,1) = ninj(i,i)
       ninj(1,i+1) = ninj(i,i)
    end do
  end subroutine triang

  subroutine trans (x, y, ndim)
! transpose square matrix, dimension ndim in array x into array y
    integer, intent(in)      :: ndim
    real(wp), intent(in)     :: x(ndim,ndim)
    real(wp), intent(out)    :: y(ndim,ndim)
    integer                  :: i, j

    do i = 1, ndim
       do j = i, ndim
          y(i,j) = x(j,i)
          y(j,i) = x(i,j)
       end do
    end do
  end subroutine trans

end module exchange_Hamiltonian
