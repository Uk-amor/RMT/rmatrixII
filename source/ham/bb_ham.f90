module bb_ham
! set up and diagonalize target Hamiltonians
! Time-stamp: "03/02/24 10:58:23 cjn"
  use precisn, only: wp
  use io_units, only: fo, jibb, jfbb
  use debug, only: bug6
  use error_prt, only: alloc_error
  implicit none

  integer, pointer, save     :: ivbbi(:)=>NULL()
  integer, pointer, save     :: ivbbr(:)=>NULL()

  integer, pointer, save     :: i1bb(:)=>NULL() ! 1e b-b rad int ptrs
  integer, save              :: nr1bb        ! size of r1bb
  real(wp), pointer, save    :: r1bb(:)=>NULL() ! 1e b-b rad integrals
  integer, pointer, save     :: irkbb(:,:,:)=>NULL() !2e BB rad int ptrs
  integer, save              :: nrkbb        ! size of rkbb
  real(wp), pointer, save    :: rkbb(:)=>NULL() ! 2e b-b rad integrals

  private
  public wrtham, hambb
  public import_ptrs

contains

  subroutine import_ptrs (nr1bbt, nrkbbt, v1, v2, v3, v4, v5, v6)
! import global variables from targets and sc_ham
    integer, intent(in)  :: nr1bbt
    integer, intent(in)  :: nrkbbt
    integer, pointer :: v1(:), v2(:), v3(:), v5(:,:,:)
    real(wp), pointer :: v4(:), v6(:)
    ivbbi => v1
    ivbbr => v2
    i1bb => v3
    nr1bb = nr1bbt
    r1bb => v4
    irkbb => v5
    nrkbb = nrkbbt
    rkbb => v6
  end subroutine import_ptrs

  subroutine hambb (isbb, lenhbb, hbb)
! calculate the bound-bound Hamiltonian matrix for a given Slp value
    use comp_data, only: njcomp, ljcomp
    use filehand, only: lookup, startr, ireada, ireadb, readb
    use rm_data, only: maxorb
    use dim, only: ldim2
    use vpack, only: wrt2, unpckl2, unpck, unpck2, unpckl
    integer, intent(in)   :: isbb
    integer, intent(in)   :: lenhbb    ! Hamiltonian size
    real(wp), intent(out) :: hbb(lenhbb) ! Hamiltonian matrix
    integer, allocatable  :: irs(:), lamst(:)
    real(wp), allocatable :: abb(:), abb1(:)
    real(wp), allocatable :: rk(:), r1(:)
    integer               :: nreci, ibuffi, nrecr, ibuffr, status, ih
    integer               :: nelnc, nelem, ncont, ncont1, ncont2, i
    integer               :: inc, lamdat, ishldt, irho, jrho, isig, jsig
    integer               :: nri, lri, nrj, lrj, nsi, lsi, nsj, lsj
    integer               :: lamlo, lamup, lam, nterm, nrk

    call lookup (ivbbi(isbb), nreci, ibuffi)
    call lookup (ivbbr(isbb), nrecr, ibuffr)
    call startr (jibb, nreci, ibuffi)
    call startr (jfbb, nrecr, ibuffr)
    hbb(1:lenhbb) = 0.0_wp
    ih_loop: do ih = 1, lenhbb
       call ireada (jibb, nelnc)
       call ireada (jibb, nelem)
       if (nelnc == 0) return
       nelem = ABS(nelem)
       if (bug6 == -5) then
          write (fo,'(a,i6)') 'bound-bound matrix element ', nelem
          write (fo,'(a)') '-----------------------------------'
       end if
       ncont = ABS(nelnc)
       call unpck2 (ncont, ncont1, ncont2)
       if (ncont1 > 0) then
          allocate (r1(ncont1), stat=status)
          if (status /= 0) call alloc_error (status, 'hambb:1', 'a')
       end if
       if (ncont2 /= 0) then       ! find 2-electron contributions
          nrk = 4 * ldim2 * maxorb
          allocate (irs(ncont2), lamst(ncont2), rk(nrk), stat=status)
          if (status /= 0) call alloc_error (status, 'hambb:2', 'a')
          call ireadb (jibb, irs, 1, ncont2)
          if (wrt2) call ireadb (jibb, lamst, 1, ncont2)
          i = 0
          if (bug6 == -5) then
             write (fo,'(a)') 'two electron contributions'
             write (fo,'(a,5i6)') 'nelnc, nelem, ncont, ncont2, &
                  &ncont1: ', nelnc, nelem, ncont, ncont2, ncont1
          end if
          inc_loop: do inc = 1, ncont2
             ishldt = irs(inc)
             if (ishldt == 0) cycle inc_loop
             if (wrt2) then
                lamdat = lamst(inc)
             else
                call unpckl(irs(inc), ishldt, lamdat)
             end if
             call unpck (ishldt, jsig, isig, jrho, irho)
             nri = njcomp(irho)
             lri= ljcomp(irho)
             nrj = njcomp(jrho)
             lrj = ljcomp(jrho)
             nsi = njcomp(isig)
             lsi = ljcomp(isig)
             nsj = njcomp(jsig)
             lsj = ljcomp(jsig)
             if (bug6 == -5) then
                write (fo,'(a,i6,i20,i6)') 'inc, irs(inc), lamdat: ', &
                     inc, irs(inc), lamdat
                write (fo,'(a,4i6)') 'irho, jrho, isig, jsig: ', &
                     irho, jrho, isig, jsig
             end if
             call unpckl2 (lamdat, lamup, lamlo)
             lamup = lamlo + 2 * lamup - 2
             lam_loop: do lam = lamlo, lamup, 2
                i = i + 1
                if (MAX(lri, lrj) <= MAX(lsi, lsj)) then
                   call locbb (lam, nri, lri, nrj, lrj, nsi, lsi, nsj,&
                        lsj, rk(i))
                else
                   call locbb (lam, nsi, lsi, nsj, lsj, nri, lri, nrj,&
                        lrj, rk(i))
                end if
                if (bug6 == -5) then
                   write (fo,'(a,5i6)') 'lam, nri, lri, nsi, lsi: ', &
                        lam, nri, lri, nsi, lsi
                   write (fo,'(a,4i6)') 'nrj, lrj, nsj, lsj: ', &
                        nrj, lrj, nsj, lsj
                   write (fo,'(a,i3,4(2x,2i3),a,e13.5)') ' rkbb(', lam, &
                        nri, lri, nsi, lsi, nrj, lrj, nsj, lsj, ') = ',&
                        rk(i)
                end if
             end do lam_loop
          end do inc_loop
          nterm = i

          if (nelnc > 0) then
             if (allocated(abb)) then
                deallocate(abb, stat=status)
                if (status /= 0) call alloc_error (status, 'hambb:d1', 'd')
             end if
             allocate (abb(nterm), stat=status)
             if (status /= 0) call alloc_error (status, 'hambb:3', 'a')
             call readb (jfbb, abb, 1, nterm)
          end if
          if (bug6 == -5) write (fo,'(a,/,(6e13.5))') ' angular &
               &integrals', abb(1:nterm)
          do i = 1, nterm
             hbb(nelem) = hbb(nelem) + rk(i) * abb(i)
          end do
          deallocate (irs, lamst, rk, stat=status)
          if (status /= 0) call alloc_error (status, 'hambb:d2', 'd')
       end if

       if (ncont1 == 0) cycle ih_loop
       if (bug6 == -5) write (fo,'(a)') 'one electron contributions'
       allocate (irs(ncont1), stat=status)
       if (status /= 0) call alloc_error (status, 'hambb:4', 'a')
       call ireadb (jibb, irs, 1, ncont1)
       do inc = 1, ncont1 
          ishldt = irs(inc)
          call unpck2(ishldt, jsig, isig)
          nsi = njcomp(isig)
          lsi = ljcomp(isig)
          nsj = njcomp(jsig)
          lsj = ljcomp(jsig)
          if (lsi == lsj) then
             call loc1bb (lsi, nsi, nsj, i1bb(1), i1bb(lsi+1), r1(inc))
             if (bug6 == -5) write (fo,'(a,2i3,2x,2i3,a,e13.5)') &
                  '   r1bb(', nsi, lsi, nsj, lsi, ') = ', r1(inc)
          else
             write (fo,'(a)') 'hambb: angbb gives different l-values &
                  &in data for one electron radial integrals'
             write (fo,'(2(a,i3))') 'lsi = ', lsi, ' lsj = ', lsj
             stop
          end if
       end do
       if (nelnc > 0) then
          if (allocated(abb1)) deallocate(abb1)
          allocate (abb1(ncont1), stat=status)
          if (status /= 0) call alloc_error (status, 'hambb:5', 'a')
          call readb (jfbb, abb1, 1, ncont1)
       end if
       if (bug6 == -5) write (fo,'(a,8e13.5)') 'angular integrals', &
            abb1(1:ncont1)
       do i = 1, ncont1
          hbb(nelem) = hbb(nelem) + r1(i) * abb1(i)
       end do
       deallocate (irs, r1, stat=status)
       if (status /= 0) call alloc_error (status, 'hambb:d3', 'd')
    end do ih_loop
  end subroutine hambb
    
  subroutine locbb (lam, nri, lri, nrj, lrj, nsi, lsi, nsj, lsj,&
       rk)
 ! locate 2-electron bound-bound integral
    use rad_data, only: maxnhf, maxnc
    integer, intent(in)         :: lam
    integer, intent(in)         :: nri, lri
    integer, intent(in)         :: nrj, lrj
    integer, intent(in)         :: nsi, lsi
    integer, intent(in)         :: nsj, lsj
    real(wp), intent(out)       :: rk
    integer                     :: mxcri, mxcrj, mxcsi, mxcsj, n3, n4
    integer                     :: nijr, nijs, n1, n2, ids, nrkl, nrs

    mxcri = maxnc(lri+1)
    mxcrj = maxnc(lrj+1)
    mxcsi = maxnc(lsi+1)
    mxcsj = maxnc(lsj+1)
    if (lri > lrj) then
       nijr = (lri * (lri+1)) / 2 + lrj + 1
       n3 = nri - mxcri - 1
       n4 = (nrj - mxcrj - 1) * (maxnhf(lri+1) - mxcri)
    else if (lrj > lri) then
       nijr = (lrj * (lrj+1)) / 2 + lri + 1
       n3 = nrj - mxcrj - 1
       n4 = (nri - mxcri - 1) * (maxnhf(lrj+1) - mxcrj)
    else 
       nijr = (lri * (lri+1)) / 2 + lrj + 1
       if (nri <= nrj) then
          n3 = nri - mxcri - 1
          n4 = ((nrj - mxcrj - 1) * (nrj - mxcrj)) / 2
       else
          n3 = nrj - mxcrj - 1
          n4 = ((nri - mxcri - 1) * (nri - mxcri)) / 2
       end if
    end if
    if (lsi > lsj) then
       nijs = (lsi * (lsi + 1)) / 2 + lsj + 1
       n1 = nsi - mxcsi
       n2 = (nsj - mxcsj - 1) * (maxnhf(lsi+1) - mxcsi)
       ids = (maxnhf(lsi+1) - mxcsi) * (maxnhf(lsj+1) - mxcsj)
    else if (lsj > lsi) then
       nijs = (lsj * (lsj + 1)) / 2 + lsi + 1
       n1 = nsj - mxcsj
       n2 = (nsi - mxcsi - 1) * (maxnhf(lsj+1) - mxcsj)
       ids = (maxnhf(lsi+1) - mxcsi) * (maxnhf(lsj+1) - mxcsj)
    else 
       nijs = (lsi * (lsi + 1)) / 2 + lsj + 1
       if (nsi <= nsj) then
          n1 = nsi - mxcsi
          n2 = ((nsj - mxcsj - 1) * (nsj - mxcsj)) / 2
          ids = ((maxnhf(lsj+1) - mxcsj) * (maxnhf(lsj+1) - &
               mxcsj+1)) / 2
       else
          n1 = nsj - mxcsj
          n2 = ((nsi - mxcsi - 1) * (nsi - mxcsi)) / 2
          ids = ((maxnhf(lsi+1) - mxcsi) * (maxnhf(lsi+1) - &
               mxcsi+1)) / 2
       end if
    end if
    nrkl = irkbb(lam+1, nijr, nijs)
    if (nrkl == -999) then
       write (fo,'(a)') 'locbb: no pointer found in irkbb for'
       write (fo,'(5(a,i4))') '   lam = ', lam, ' lri = ', lri,&
            ' lrj = ', lrj, ' lsi = ', lsi, ' lsj = ', lsj
       stop
    end if
    nrs = nrkl + n1 + n2 + ids * (n3 + n4)
    rk = rkbb(nrs)
  end subroutine locbb

  subroutine loc1bb (lsi, nsi, nsj, i1bb, istl, rval)
! locate one-electron bound-bound radial integral
    use rad_data, only: maxnc
    integer, intent(in)         :: nsi, lsi
    integer, intent(in)         :: nsj
    integer, intent(in)         :: i1bb, istl
    real(wp), intent(out)       :: rval
    integer                     :: maxcl, n1bb

    if (istl == -999) then
       write (fo,'(a,i4)') 'loc1bb: no pointer found in ict1bb for &
            &lsi = ', lsi
       stop
    end if
    maxcl = maxnc(lsi+1)
    if (nsi >= nsj) then
       n1bb = istl - i1bb + nsj - maxcl + ((nsi - maxcl - 1) * &
            (nsi - maxcl)) / 2
    else
       n1bb = istl - i1bb + nsi - maxcl + ((nsj - maxcl - 1) * &
            (nsj - maxcl)) / 2
    end if
    rval = r1bb(n1bb)
  end subroutine loc1bb

  subroutine wrtham (idham, lenham, hmat)
! write out the hamiltonian matrix which is to be diagonalised
    integer, intent(in)      :: idham ! Hamiltonian dimension
    integer, intent(in)      :: lenham ! Hamiltonian matrix size
    real(wp), intent(in)     :: hmat(lenham)  ! Hamiltonian matrix
    character(len=12) :: fo5(5) = (/'    (5f14.7)', '(14x,4f14.7)', &
         '(28x,3f14.7)', '(42x,2f14.7)', ' (56x,f14.7)'/)
    integer                  :: idham2, m, k, l, n, i, mo, mh, nfo
    
    write (fo,'(a,2i6,/)') 'WRTHAM, Target Hamiltonian: idham, &
         &lenham = ', idham, lenham
    idham2 = 2 * idham
    m = 0
    do while (m < idham)    ! rows of Hamiltonian
       k = m
       l = m + 1    ! start of row
       m = m + 5
       n = MIN(m, idham)   ! end of row
       nfo = 1
       do i = 1, n
          if (i > k) then
             nfo = i - k
             l = i
          end if
          mo = ((i-1) * (idham2-i)) / 2 + l
          mh = mo + n - l
          write (fo,fo5(nfo)) hmat(mo:mh)
       end do
       write (fo,'(a)') ' '
    end do
  end subroutine wrtham
end module bb_ham

