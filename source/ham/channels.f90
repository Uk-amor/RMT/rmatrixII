module channels
! Time-stamp: "2005-08-19 11:44:56 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug8
  use error_prt, only: alloc_error
  implicit none

  logical, save              :: first_call = .true.
  integer, save              :: nchan               ! # channels
  integer, save              :: chs   ! nast * ltot, ie bounding value on channels
  integer, allocatable, save :: istch(:)  ! target state label for each channel
  integer, allocatable, save :: nchset(:,:) 
  integer, allocatable, save :: iconch(:,:)
! total spin dependent channel count number for (target state and continuum l) 
! channel number and either target 2S_t+1 or total 2S+1
  integer, allocatable, save :: l2pspn(:,:) 
! continuum l value for total spin dependent channel count number and 
!  either target 2S_t+1 or total 2S+1
  integer, allocatable, save :: kconat(:,:)
! number of (continuum l) channels for each target state and given either target 2S_t+1 or total 2S+1
  integer, allocatable, save :: nschan(:)
! total spin dependent number of channels (ie count) for given either 
!  target 2S_t+1 or total 2S+1

  integer, allocatable, save :: nsprec(:)
  integer, allocatable, save :: krech(:)   ! see below

  integer, allocatable, save :: lenham(:)
! Hamiltonian length for given either target 2S_t+1 or total 2S+1

  private
  public def_channels, setchn
  public nchan, istch, nchset, iconch, l2pspn, kconat, nschan, nsprec
  public krech, lenham, chs

contains

  subroutine def_channels
    use set_data, only: lrang3, spn, nast, nset
    use stghrd_mod, only: lrglup
    integer                  :: s, status, lr4

    lr4 = lrglup + 1
    s = spn + 2
    allocate (nchset(nset,2*lrang3-1), kconat(nast,s), nschan(s),   &
         nsprec(s), krech(0:2*lr4), lenham(s), stat=status)
    if (status /= 0) call alloc_error (status, 'def_channels', 'a')
  end subroutine def_channels

  subroutine setchn (iexch)
! Given lrgl, parity: calculate total # channels; order of channels for
! l and state; first channel # for l and set; set up iconch to convert 
! channel #s from new no exchange code to those in rmatrix code; set up
! nconat = # channels coupled to each state; l2p = lvalue of each 
! channel ordered as in the rmatrix code
    use rad_data, only: nvary
    use l_data, only: lrgl, npty, ltot, lval, nscol, nsetl
    use states, only: nsetst, lspn, kspin
    use set_data, only: nast, nstat, isplo, ispup, spn
    integer, intent(in)         :: iexch
    integer                     :: ichan(ltot,nast)
    integer                     :: kl, i, j, ist, is, nrecs, irech, isp
    integer                     :: icount, ich, l, lslo, lsup, lcount
    integer                     :: lsp, status, chf

    if (bug8 == 1) then
       write (fo,'(a)') 'subroutine setchn'
       write (fo,'(a)') '-----------------'
       write (fo,'(a)') '      is    kl nchset  ist ichan istch'
    end if

    call get_chf (chf)   ! total number of channels for the current total symmetry
    chs = nast * ltot
    if (first_call) then
       first_call = .false.
    else
       deallocate (istch, iconch, l2pspn, stat=status)
       if (status /= 0) call alloc_error (status, 'setchn', 'd')
    end if
    allocate (istch(chf), iconch(chf,spn+2), l2pspn(chs,spn+2), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'setchn', 'a')
    iconch = 0

    ichan = 0
    nchan = 0
    continuum_am: do kl = 1, ltot    
       i_loop: do i = 1, nscol(kl) ! sets coupled to this l=lval(kl)
          is = nsetl(i,kl)
          if (nstat(is) == 0) cycle continuum_am
          nchset(is,kl) = nchan + 1 ! first channel # for set and l
          tgts: do j = 1, nstat(is) ! states composed from this set
             ist = nsetst(is,j)
             nchan = nchan + 1
             ichan(kl,ist) = nchan  ! channel # for l and state
             istch(nchan) = ist     ! target seq for channel
             if (bug8 == 1) then
                write (fo,'(a,6i6)') '   ', is, kl, nchset(is,kl), &
                     ist, ichan(kl,ist), istch(nchan)
             end if
          end do tgts
       end do i_loop
    end do continuum_am

! construct channel conversion array for ordering Hamiltonian on disc.
! store l2p and nconat arrays for asymptotic program

! krech(2*lrgl+npty) holds the starting record # for each lrgl, npty set
! This version has starting record 3 for every lrgl,npty to save space

! lenham(isp) holds the length of the Hamiltonian for spin isp

! nsprec(isp) holds starting record # for Hamiltonian for spin isp

    irech = lrgl + lrgl + npty
    krech(irech) = 3
    nrecs = krech(irech) + 1
    isp_loop: do isp = isplo, ispup, 2
       nsprec(isp) = nrecs
       lenham(isp) = 0
       if (kspin(isp) == 0) cycle isp_loop
       icount = 0
       ist_loop: do ist = 1, nast
          lcount=0
          if (lspn(ist) == isp) then
             do kl = 1, ltot
                ich = ichan(kl,ist)
                if (ich /= 0) then
                   icount = icount + 1
                   lcount = lcount + 1
                   iconch(ich,isp) = icount
                   l2pspn(icount,isp) = lval(kl)
                   l = lval(kl) + 1
                   lenham(isp) = lenham(isp) + nvary(l)
                end if
             end do
          end if
          kconat(ist,isp) = lcount ! =0 if state does not have this spin
       end do ist_loop
       nschan(isp) = icount
       if (icount > SIZE(l2pspn, DIM=1)) then
          write (fo,'(a,i6)') 'setchn: dimension overflow, icount = ', &
               icount
          stop
       end if
       nrecs = (icount * (icount + 1)) / 2 + nrecs
    end do isp_loop
    krech(irech+1) = nrecs
    if (bug8 == 1) then
       write (fo,'(a)') 'iconch(nchan)'
       do isp = isplo, ispup, 2
          write (fo,'(3x,10i6)') iconch(1:nchan,isp)
       end do
    end if
    do isp = isplo, ispup, 2
       write (fo,'(a,i3)') 'Target spin (2s+1) = ', isp
       write (fo,'(a)') '------------------------'
       write (fo,'(a,i4)') 'No. of channels    =', nschan(isp)

       write (fo,'(a)') 'l2p coupled to each channel:'
       write (fo,'(3x,10i6)') (l2pspn(i,isp),i=1,nschan(isp))
       write (fo,'(a)') 'no. of channels coupled to each state:'
       write (fo,'(3x,10i6)') kconat(1:nast,isp)
    end do

    if (bug8 == 1) then
       write (fo,'(a)') 'nsprec(isp)'
       write (fo,'(3x,10i6)') nsprec(1:ispup+1)
    end if

    if (iexch == 1) then
       lslo = isplo - 1
       if (isplo == 1) lslo = 2
       lsup = ispup + 1
       isp_loop2: do isp = lslo, lsup, 2
          nsprec(isp) = nrecs
          lenham(isp) = 0
          icount = 0
          ist_loop2: do ist = 1, nast
! if the state does not have allowed spin enter zero in kconat
             lcount = 0
             lsp = lspn(ist)
             if (lsp == isp-1 .or. lsp == isp+1) then
                do kl = 1, ltot
                   ich = ichan(kl,ist)
                   if(ich /= 0) then
                      icount = icount + 1
                      lcount = lcount + 1
                      iconch(ich,isp) = icount
                      l2pspn(icount,isp) = lval(kl)
                      l = lval(kl)+1
                      lenham(isp) = lenham(isp) + nvary(l)
                   end if
                end do
             end if
             kconat(ist,isp) = lcount
          end do ist_loop2
          nschan(isp) = icount
          if (icount > SIZE(l2pspn, DIM=1)) then
             write (fo,'(a,i6)') 'setchn: dimension overflow, &
                  &icount = ', icount
             stop
          end if
          nrecs = (icount * (icount + 1)) / 2 + nrecs
       end do isp_loop2
       krech(irech+1) = nrecs
       if (bug8 == 1) then
          write (fo,'(a)') 'iconch(nchan)'
          do isp = lslo, lsup, 2
             write (fo,'(3x,10i6)') iconch(1:nchan,isp)
          end do
       end if
       do isp = lslo, lsup, 2
          write (fo,'(/,a,i3)') 'Total spin (2s+1) = ', isp
          write (fo,'(a)') '-----------------------'
          write (fo,'(a,i6)') 'No. of channels   = ', nschan(isp)

          write (fo,'(a)') 'l2p coupled to each channel:'
          write (fo,'(3x,10i6)') (l2pspn(i,isp),i=1,nschan(isp))
          write (fo,'(a)') 'No. of channels coupled to each state:'
          write (fo,'(3x,10i6)') kconat(1:nast,isp)
       end do
       if (bug8 == 1) then
          write (fo,'(a)') 'nsprec(isp)'
          write (fo,'(3x,10i6)') nsprec(1:ispup+1)
       end if
    end if
  end subroutine setchn

  subroutine get_chf (chf)
! calculate total number of channels
    use set_data, only: nstat
    use l_data, only: ltot, nscol, nsetl
    integer, intent(out)   :: chf    ! total # channels
    integer                :: kl, is, i, j, nch

    nch = 0
    am: do kl = 1, ltot   ! continuum a.m.
       sets: do i = 1, nscol(kl)  ! sets coupled to this l-lval(kl)
          is = nsetl(i,kl)
          if (nstat(is) > 0) then
             states: do j = 1, nstat(is) ! states composed from set
                nch = nch + 1
             end do states
          end if
       end do sets
    end do am
    chf = nch
  end subroutine get_chf

end module channels
