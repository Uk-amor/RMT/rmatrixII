module dim
! Time-stamp: "2003-03-26 11:16:12 cjn"
  implicit none

  integer, save    :: ldim0, ldim1, ldim2

  private
  public ldim0, ldim1, ldim2, ldim

contains
  subroutine ldim
! defines the value of the variable ildim9
    use rad_data, only: lrang1
    use set_data, only: lrang3
    integer            :: iddim, lr1, lr3

    lr1 = lrang1
    lr3 = lrang3
!    ldim1 = iddim + iddim + 1

    iddim = MAX(lr1, lr3-1)
    ldim1 = iddim + iddim + 2
    ldim2 = iddim
    ldim0 = 4 * (lr1 - 1)
  end subroutine ldim
end module dim
