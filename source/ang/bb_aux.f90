module bb_aux
! auxilliary routines used by angbb
! Time-stamp: "2005-03-11 15:20:44 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug7
  implicit none

!  real(wp), parameter     :: eps = 1.0e-5_wp
  real(wp), parameter     :: eps = 1.0e-9_wp
  character(len=7), save :: filea, fileb
  integer, save          :: jbufiv, jbuffv
  logical, save          :: tgt

  private
  public bbcup, cup1, wrbb, set_files
  public jbufiv, jbuffv, filea, fileb
  public tgt

contains

  subroutine set_files (target)
! define files for target or BB matrix elements
    use io_units, only: jbufiv1, jbuffv1, jbufivt, jbuffvt
    logical, intent(in) :: target   ! target flag

    if (target) then
       jbufiv = jbufivt
       jbuffv = jbuffvt
       filea = 'atgi'
       fileb = 'atgr'
       tgt = .true.
    else
       jbufiv = jbufiv1
       jbuffv = jbuffv1
       filea = 'abbi'
       fileb = 'abbr'
       tgt = .false.
    end if
  end subroutine set_files

  subroutine bbcup (nelc, indni, surni, indnj, surnj, indmi, surmi, &
       indmj, surmj, vi, lsseti, vj, lssetj, mincoi, maxcoi, mincoj,&
       maxcoj, vm, lssetm, lrhoi, lrhoj, lsigi, lsigj, lrgl, lrgs,  &
       mcomx, lamdmn, lamdmx, lamemn, lamemx, vval, exval, idebug)
! couple two bound configurations after the interacting shells have
! been determined.  that is to determine vval(lamd) and exval(lamd) for
! given lrhoi,lrhoj,lsigi,lsigj.
    use angular_momentum, only: dracah, rmetab, spinw
    use ang_aux, only: cupl2
    use ls_states, only: lvec, svec
    use symmetries, only: isran2
    use dim, only: ldim1
     integer, intent(in)    :: nelc
     integer, intent(in)    :: lrhoi, lrhoj
     integer, intent(in)    :: lsigi, lsigj
     integer, intent(in)    :: lrgl, lrgs
     integer, intent(in)    :: mcomx
     integer, intent(out)   :: lamdmn, lamdmx, lamemn
     integer, intent(inout) :: lamemx
     integer, intent(in)    :: idebug
     real(wp), intent(in)   :: surni(:), surnj(:)
     integer, intent(in)    :: indni(:), indnj(:)
     real(wp), intent(in)   :: surmi(:)
     integer, intent(in)    :: indmi(:)
     real(wp), intent(in)   :: surmj(:)
     integer, intent(in)    :: indmj(:)
     integer, intent(in)    :: vi, vj, vm 
     integer, intent(in)    :: lsseti, lssetj, lssetm 
     integer, intent(in)    :: mincoi(:), maxcoi(:)
     integer, intent(in)    :: mincoj(:), maxcoj(:)
     real(wp), intent(out)  :: vval(0:), exval(0:)
     integer                :: lrgl2, incou, ispin
     integer                :: lamd1, lamd2, lame1, lame2, lrhoi2
     integer                :: lrhoj2, lsigi2, lsigj2, lcfg, lcfg2
     integer                :: isc1, isc2, jncou, lcfgp, lcfgp2, lcurl1
     integer                :: lcurl2, jspin, idrct
     integer                :: lamdup, lcurls, lam2, lamd, iscurl
     integer                :: lsign, lcurl, lcur2, isp1, iscur1, iscur2
     integer                :: kap1, kap2, lame, kaplo, lamdlo
     integer                :: kapup, kap, kapt2, npoint, i1, i2, j1, j2
     integer                :: incoux, jncoux
     real(wp)               :: prod1, surf1j, prod, d1, rac, d2, ex1
     real(wp)               :: ex2, ex3, surf1i, rac1, rac2, rac3
     integer, pointer       :: li(:), lj(:), lmi(:)
     integer, pointer       :: isi(:), isj(:), ismi(:)
     real(wp)               :: ashel(isran2+4,0:ldim1 + 4)  ! extra value added to limit, to be checked
     real(wp)               :: spifac(isran2+4)

     li => lvec(vi, lsseti)
     lj => lvec(vj, lssetj)
     lmi => lvec(vm, lssetm)
     isi => svec(vi, lsseti)
     isj => svec(vj, lssetj)
     ismi => svec(vm, lssetm)
     if (MOD(lrhoi+lrhoj+lsigi+lsigj,2) /= 0) then
        write (fo,'(2(a,i4))') 'bbcup: parity violation, lrgl = ', &
             lrgl, ' lrgs = ', lrgs
        stop
     end if
     vval = 0.0_wp
     exval = 0.0_wp
! exchange lambda limits will not alter
! lamemx < 0 => no exchange integral should be calculated
! (when irho=isig or jrho=jsig)
     lamdmn = 999    ! lower limit of direct lambda, direct pointer
     lamdmx = 0      ! upper limit of direct lambda
     lamemn = 999    ! pointer than and exchange integral is calculated

! calculate limits on direct and exchange lambda
     lamd1 = MAX(ABS(lrhoi-lrhoj), ABS(lsigi-lsigj))
     lamd2 = MIN((lrhoi+lrhoj), (lsigi+lsigj))
     lame1 = MAX(ABS(lrhoi-lsigj), ABS(lsigi-lrhoj))
     if (lamemx >= 0) then
        lame2 = MIN((lrhoi+lsigj), (lsigi+lrhoj))
     else
        lame2 = -999
     end if

     if (idebug == 1) write (fo,'(a,4i6)') 'lams: ', lamd1, lamd2, &
          lame1, lame2
     if (lame1 > lame2 .and. lamd1 > lamd2) return ! no cases

     lrgl2 = lrgl + lrgl
     lrhoi2 = lrhoi + lrhoi
     lrhoj2 = lrhoj + lrhoj
     lsigi2 = lsigi + lsigi
     lsigj2 = lsigj + lsigj
     incou_loop: do incoux = 1, SIZE(surni)
        incou = indni(incoux)
        surf1i = surni(incoux)
        if (idebug == 1) write (fo,'(a,e16.8)') 'surf1i = ', surf1i
        if (ABS(surf1i) <= eps) cycle incou_loop
        lcfg = li(incou)
        lcfg2 = lcfg + lcfg
        ispin = isi(incou)
        isc1 = ispin - 1
        if (isc1 == 0) isc1 = 2
        isc2 = ispin + 1
        prod1 = SQRT(REAL(lcfg+lcfg+1,wp)) * surf1i
        if (idebug /= 0) write (fo,'(a,2i6)') 'li, ispin ', lcfg, ispin
        jncou_loop: do jncoux = 1, SIZE(surnj)
           jncou = indnj(jncoux)
           surf1j = surnj(jncoux)
           if (ABS(surf1j) <= eps) cycle jncou_loop
           lcfgp = lj(jncou)
           lcfgp2 = lcfgp + lcfgp
           if (nelc == 2) then ! n-2 tree is 1 empty shell
              lcurl1 = 0
              lcurl2 = 0
              isc1 = 1
              isc2 = 1
              ashel(1,0) = 1.0_wp
              ashel(3,0) = 0.0_wp
              npoint = 1
           else ! get limits on top coupling of n-1 tree
              lcurl1 = MAX(ABS(lcfg-lrhoi), ABS(lcfgp-lrhoj))
              lcurl2 = MIN((lcfg+lrhoi), (lcfgp+lrhoj))
              i1 = mincoi(incou)
              i2 = maxcoi(incou)
              j1 = mincoj(jncou)
              j2 = maxcoj(jncou)
              if (i1 == 0 .or. j1 == 0) then
                 npoint = 0
              else
                 call cupl2 (indmi(i1:i2), surmi(i1:i2), indmj(j1:j2),&
                      surmj(j1:j2), mcomx, lmi, ismi, ashel, npoint)
              end if
           end if
           
           if (npoint <= 0) cycle jncou_loop ! no ashel entries
           jspin = isj(jncou)
           prod = prod1 * SQRT(REAL(lcfgp+lcfgp+1,wp)) * surf1j
           if (idebug /= 0) write (fo,'(a,2i4,e16.8)') 'lj,&
                & jspin, prod = ', lcfgp, jspin, prod

           idrct = 1
           if (ispin /= jspin) idrct = -1

! revise direct lambda limits:
           lamdlo = MAX(ABS(lcfg-lcfgp), lamd1)
           if (MOD(lamdlo+lamd1,2) /= 0) lamdlo = lamdlo + 1
           lamdup = MIN(lcfg+lcfgp, lamd2)
           if (lamdlo > lamdup) idrct = -1

           if (idrct == 1) then  ! direct angular integral
              lcurls = 1
              if (MOD(lcurl1+lcfg+lcfgp,2) == 1) lcurls = -1
              lamdmn = MIN(lamdlo, lamdmn)
              lamdmx = MAX(lamdup, lamdmx)
              lamd_loop: do lamd = lamdlo, lamdup, 2
                 lam2 = lamd + lamd
                 call dracah (lcfg2, lcfgp2, lsigi2, lsigj2, lam2, &
                      lrgl2, rac)
                 d1 = prod * rac * rmetab(lamd,lrhoi,lrhoj) *&
                      rmetab(lamd,lsigi,lsigj)
                 lsign = lcurls
                 lcurl_loop: do lcurl = lcurl1, lcurl2
                    lcur2 = lcurl + lcurl
                    call dracah (lcfg2, lcfgp2, lrhoi2, lrhoj2, &
                         lam2, lcur2, rac)
                    d2 = rac * d1 * lsign
                    lsign = - lsign
                    iscurl_loop: do iscurl = isc1, isc2, 2
                       vval(lamd) = vval(lamd) + d2*ashel(iscurl,lcurl)
                       if (idebug /= 0) then
                          write (fo,'(a,3i6,/,4e14.6)') 'lamd, &
                               &iscurl, lcurl, ashel, d1, d2, &
                               &vval: ', lamd, iscurl, lcurl, &
                               ashel(iscurl,lcurl), d1, d2, vval(lamd)
                       end if
                    end do iscurl_loop
                 end do lcurl_loop
              end do lamd_loop
              if (idebug /= 0) write (fo,'(a,5e10.3)') 'vval = ', &
                   vval(0:4)
           end if

           if (lame1 <= lame2) then ! exchange integral
! calculate the spin at the top of the n-1 tree
! find the appropriate spin racah coefficient from the spinw table
              if (ispin == jspin) then
                 iscur1 = ispin - 1
                 if (iscur1 == 0) then
                    iscur1 = 2
                    iscur2 = 2
                    spifac(2) = spinw(3,ispin)
                 else
                    iscur2 = ispin + 1
                    if (lrgs == iscur1) then
                       spifac(iscur1) = spinw(1,ispin)
                       spifac(iscur2) = spinw(2,ispin)
                    else
                       spifac(iscur1) = spinw(2,ispin)
                       spifac(iscur2) = spinw(3,ispin)
                    end if
                 end if
              else
                 isp1 = MIN(ispin, jspin)
                 iscur1 = isp1 + 1
                 iscur2 = iscur1
                 spifac(iscur1) = spinw(4,isp1)
              end if
              lamemn = lame1
              lamemx = lame2
              kap1 = MAX(ABS(lcfg-lrhoj), ABS(lcfgp-lrhoi))
              kap2 = MIN(lcfg+lrhoj, lcfgp+lrhoi)
              lame_loop: do lame = lame1, lame2, 2
                 ex1 = prod * rmetab(lame,lrhoi,lsigj) * &
                      rmetab(lame,lrhoj,lsigi)
                 lam2 = lame + lame
                 
                 kaplo = MAX(ABS(lrgl-lame), kap1)
                 kapup = MIN((lrgl+lame), kap2)
                 kap_loop: do kap = kaplo, kapup
                    kapt2 = kap + kap
                    call dracah (kapt2, lcfg2, lam2, lsigi2,&
                         lrhoj2, lrgl2, rac1)
                    call dracah (kapt2, lcfgp2, lam2, &
                         lsigj2, lrhoi2, lrgl2, rac2)
                    ex2 = ex1 * (kap + kap + 1) * rac1 * rac2
                    lcurl1_loop: do lcurl = lcurl1, lcurl2
                       lcur2 = lcurl + lcurl
                       call dracah (lcfg2, lrhoj2, lrhoi2, &
                            lcfgp2, kapt2, lcur2, rac3)
                       ex3 = ex2 * rac3
                       do iscurl = iscur1, iscur2, 2
                          exval(lame) = exval(lame) + ex3 * &
                               ashel(iscurl,lcurl) * spifac(iscurl)
                          if (idebug /= 0) then
                             write (fo,'(a,3i6)') 'lame, iscurl, lcurl &
                                  &= ', lame, iscurl, lcurl
                             write (fo,'(a,4e16.8)') 'ashel, ex3, &
                                  &spifac, exval', ashel(iscurl,lcurl),&
                                  ex3, spifac(iscurl), exval(lame)
                          end if
                       end do
                    end do lcurl1_loop
                 end do kap_loop
              end do lame_loop
              if (idebug /= 0) write (fo,'(a,5E10.3)') 'exval = ', &
                   exval(0:4)
           end if
        end do jncou_loop
     end do incou_loop
   end subroutine bbcup

  subroutine cup1 (indmi, surmi, indmj, surmj, mcomx, cup)
! sum over the intermediate l,s (replace with library routine)
    integer, intent(in)     :: indmi(:)
    integer, intent(in)     :: indmj(:)
    integer, intent(in)     :: mcomx
    real(wp), intent(in)    :: surmi(:)
    real(wp), intent(in)    :: surmj(:)
    real(wp), intent(out)   :: cup
    real(wp)                :: t(mcomx), tj
    integer                 :: i

    t = 0.0_wp

! use essl sparse matrix routines:
!    call dsctr (SIZE(indmi), surmi, indmi, t)
 !   cup = ddoti(SIZE(indmj), surmj, indmj, t)

    do i = 1, SIZE(indmi)
       t(indmi(i)) = surmi(i)
    end do
    cup = 0.0_wp
    do i = 1, SIZE(indmj)
       tj = t(indmj(i))
       if (tj /= 0) then
          cup = cup + tj * surmj(i)
       end if
    end do
  end subroutine cup1

  subroutine wrbb (nelem, nonz, ndcont, nlamd, lamdst, irsd, bbd, &
       necont, nlame, lamest, irse, bbe, isre, ncont1, irs1, bb1)
! output bound-bound angular integrals to disk
    use filehand, only: iwrita, iwritb, writb
    use debug, only: bug7
    use vpack, only: pck2, pckl, mx1, mx2, mx3, wrt2
    use statistics, only: coll_stat
    integer, intent(in)    :: irsd(:)   ! direct integral labels
    real(wp), intent(in)   :: bbd(:,:)  ! direct integrals
    integer, intent(in)    :: irse(:)   ! exchange integral labels
    real(wp), intent(in)   :: bbe(:,:)  ! exchange integrals
    integer, intent(in)    :: lamdst(:) ! direct lambda values
    integer, intent(in)    :: nlamd(:)  ! # direct integrals for lambda
    integer, intent(in)    :: lamest(:) ! exchange lambda values
    integer, intent(in)    :: nlame(:)  ! # exhange integrals for lambda
    integer, intent(in)    :: irs1(:)   ! one-electron integral labels
    real(wp), intent(in)   :: bb1(:)    ! one-electron integrals
    integer, intent(in)    :: isre(:)   ! exchange reversed labels
    integer, intent(in)    :: nelem
    integer, intent(in)    :: ndcont    ! # direct lambda values
    integer, intent(in)    :: necont    ! # exchange lambda values
    integer, intent(in)    :: ncont1    ! # one-electron integrals
    integer, intent(inout) :: nonz      ! disk record counter
    real(wp)      :: bb(SIZE(bbd) + SIZE(bbe) + SIZE(bb1))
    integer       :: irs(necont+ndcont)
    integer       :: lamst(necont+ndcont)
    integer       :: nd, id, ne, ie, idn, ien, n1, n, i, nterm, ncont
    integer       :: nelnc

! write the angular integrals for the current pair of bound
! configurations to the files
    if (bug7 == 3) then
       do nd = 1, ndcont
          id = nlamd(nd)
          write (fo,'(1x,i5,i12,e15.5)') lamdst(nd), irsd(nd), bbd(1,nd)
          do idn = 2, id
             write (fo,'(18x,e15.5)') bbd(idn,nd)
          end do
       end do
       do ne = 1, necont
          ie = nlame(ne)
          write (fo,'(1x,i5,23x,i12,e15.5)') lamest(ne), irse(ne), &
               bbe(1,ne)
          do ien = 2, ie
             write (fo,'(41x,e15.5)') bbe(ien,ne)
          end do
       end do
       do n1 = 1, ncont1
          write (fo,'(52x,i5,e15.5)') irs1(n1), bb1(n1)
       end do
    end if

! combine direct and exchange and irs and lamst
! nb. we don't yet take advantage of 2 electrons from one shell
! nor do we prune out zero contributions.  leave latter for debugging.
    i = 0
    if (wrt2) then    ! labels, lambdas separate
       do n = 1, ndcont
          lamst(n) = lamdst(n)
          irs(n) = irsd(n)
          do id = 1, nlamd(n)
             i = i + 1
             bb(i) = bbd(id,n)
          end do
       end do

       do n = 1, necont
          irs(ndcont+n) = isre(n)
          lamst(ndcont+n) = lamest(n)
          do ie = 1, nlame(n)
             i = i + 1
             bb(i) = bbe(ie,n)
          end do
       end do

    else    ! pack lambdas with labels

       do n = 1, ndcont
          lamst(n) = lamdst(n)
          irs(n) = pckl(irsd(n), lamdst(n))
          do id = 1, nlamd(n)
             i = i + 1
             bb(i) = bbd(id,n)
          end do
       end do

       do n = 1, necont
          lamst(ndcont+n) = lamest(n)
          irs(ndcont+n) = pckl(isre(n), lamest(n))
          do ie = 1, nlame(n)
             i = i + 1
             bb(i) = bbe(ie,n)
          end do
       end do
    end if

    call coll_stat (ndcont, irsd, bb, necont, isre, &
         ncont1, irs1, bb1, lamdst, nlamd, lamest, nlame)

! write angular integrals for current pair of bound cfgs to files
    nterm = i
    ncont = ndcont + necont
    if (ncont+ncont1 /= 0) then
       nonz = nonz + 1
       if (ncont1 > mx1 .or. ncont > mx2) then
          write (fo,'(a)') 'wrbb: packing error'
          write (fo,'(2(a,i6))') 'ncont = ', ncont, ' mx2 = ', mx2
          write (fo,'(2(a,i6))') 'ncont1 = ',ncont1, ' mx1 = ', mx1
          stop
       end if
       nelnc = pck2(ncont1, ncont)
       if (nelem < 0) nelnc = - nelnc
       call iwrita (jbufiv, nelnc)
       call iwrita (jbufiv, nelem)
       if (wrt2) then             ! two-integer packing:
          if (ncont > 0) then
             call iwritb (jbufiv, irs, 1, ncont)
             call iwritb (jbufiv, lamst, 1, ncont)
             if (nelem >= 0) then
                call writb (jbuffv, bb, 1, nterm)
             end if
          end if
       else                       ! one-integer packing
          if (ncont > 0) then
             if (nelem < 0) then
                call iwritb (jbufiv, irs, 1, ncont)
             else
                call iwritb (jbufiv, irs, 1, ncont)
                call writb (jbuffv, bb, 1, nterm)
             end if
          end if
       end if
       if (ncont1 > 0) then
          if (nelem < 0) then
             call iwritb (jbufiv, irs1, 1, ncont1)
          else
             call iwritb (jbufiv, irs1, 1, ncont1)
             call writb (jbuffv, bb1, 1, ncont1)
          end if
       end if
    end if
  end subroutine wrbb

end module bb_aux
