module bc_multipoles
! Time-stamp: "2005-03-11 15:23:03 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbufiv2, jbuffv2
  use debug, only: bug7
  implicit none

  real(wp), parameter    :: eps = 1.0e-5_wp

  integer, save          :: itotal
  integer, save          :: ntotal
  real(wp), save         :: xtotal
  real(wp), save         :: atotal

  private
  public angmbc

contains

  subroutine angmbc (npset, lpset, ispset, ippset, npocsh, npocob,   &
       npelsh, nnpfig, npnpq, ncoumx, nset, lset, isset, ipset, nnq, &
       id1)
! calculates the angular integral (weighting factor) for the multipole
! of each pair of bound continuum configurations
    use filehand, only: open, newfil, catlog, iwrita, iwritb, endw, &
         writix, close, writb, iwritc
    use rm_data, only: maxorb
    use surfacing_tables, only: surf_vec
    use sym_sets, only: get_ncset, get_ncfg, ncsy
    use error_prt, only: alloc_error
    integer, intent(in)  :: npset
    integer, intent(in)  :: lpset(:)
    integer, intent(in)  :: ispset(:)
    integer, intent(in)  :: ippset(:)
    integer, intent(in)  :: npocsh(:)
    integer, intent(in)  :: npocob(:,:)
    integer, intent(in)  :: npelsh(:,:)
    integer, intent(in)  :: nnpfig(:,:)
    integer, intent(in)  :: npnpq(:)
    integer, intent(in)  :: ncoumx(:)
    integer, intent(in)  :: nset
    integer, intent(in)  :: lset(:)
    integer, intent(in)  :: isset(:)
    integer, intent(in)  :: ipset(:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: id1     ! surfacing table id
    character(len=7)     :: filea, fileb, stats
    integer              :: irs1(ncsy)
    real(wp)             :: bc1(ncsy)
    integer              :: imbci(nset,npset)
    integer              :: imbc(ncsy,nset,npset)
    integer              :: ndummy, isnp, jsn, icsnp, lrgl, lrgs
    integer              :: lrgpi, lcfgp, jspin, jpi, ncount
    integer              :: inpc, inpcou, inpcq, jnc, k1i, inc, incq, k
    integer              :: ncomx, isig, idelp, ik, i, j
    integer              :: indic, jncou, jcsn
    integer              :: idelps, n1shi, ii
    real(wp)             :: facij1, cup
    real(wp), pointer    :: suri(:)
    integer, pointer     :: indxi(:)
    integer, pointer     :: ncset(:), ncfga(:,:), ncfgb(:,:)
    integer, pointer     :: npcset(:), npcfga(:,:), npcfgb(:,:)
    logical              :: real_typ
    integer              :: jbufiv, jbuffv
    integer, allocatable :: npcset_d(:)
    integer              :: status  

    jbufiv = jbufiv2
    jbuffv = jbuffv2
    filea = 'abcmi'
    fileb = 'abcmr'
    stats = 'new'
    real_typ = .false.
    call open (jbufiv, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffv, fileb, stats, real_typ)
    call newfil (jbufiv, ndummy)
    call newfil (jbuffv, ndummy)

    ncset => get_ncset(1)
    npcset => get_ncset(2)
    call get_ncfg (1, ncfga, ncfgb)
    call get_ncfg (2, npcfga, npcfgb)

    write (fo,'(10x,a,//)') 'ANGMBC: Multipole BC Integrals'
    if (bug7 > 0) then
       if (bug7 == 6) write (fo,'(a)') ' icsnp  jcsn  isig    bc1 '
    end if

    imbci(1:nset,1:npset)=-999
    imbc = 0
    write (fo,'(/,a,/)') ' set     count irs1-sum &
         &                 bc1-sum                abs(bcs1-sum)'
! loop over sets of cfgs with the same lrgs, lrgl, lrgpi
    i_syms: do isnp = 1, npset
       itotal = 0
       ntotal = 0
       atotal = 0.0_wp
       xtotal = 0.0_wp
       lrgl = lpset(isnp)
       lrgpi = ippset(isnp)
       lrgs = ispset(isnp)
       if (bug7 == 6) write (fo,'(a,4i6)') 'lrgl, lrgpi, lrgs, isnp =',&
            lrgl, lrgpi, lrgs, isnp

       j_syms: do jsn = 1, nset ! N-electron cfgs
          lcfgp = lset(jsn)
          jspin = isset(jsn)
          jpi = ipset(jsn)
          if (bug7 == 6) write (fo,'(a,4i6)') 'lcfgp, jpi, jspin, &
               &jsn =', lcfgp, jpi, jspin, jsn
          if (ABS(lrgs-jspin) /= 1) cycle

! store the starting positions of the files in the indexing arrays
          call catlog (jbufiv, imbci(jsn,isnp))

          if (ASSOCIATED(npcset)) then
          i_cfgs: do icsnp = 1, npcset(isnp) ! N+1 cfgs
             ncount = 0
             inpcou = npcfgb(icsnp,isnp)
             inpc = npcfga(icsnp,isnp)
             inpcq = npnpq(inpc)
             n1shi = npocsh(inpc)
             j_cfgs: do jcsn = 1, ncset(jsn) ! N cfgs
                jncou = ncfgb(jcsn,jsn)
                jnc = ncfga(jcsn,jsn)
                if (bug7 == 6) write (fo,'(a,4i6)') 'cups', inpc, &
                     inpcou, jnc, jncou

! bring an electron to the surface of the n+1 cfg from each shell 
! looking for a match of residual N cfg with jnc, if match isig defined
                k1i_loop: do k1i = n1shi, 1, -1
                   inc = nnpfig(k1i,inpc)
                   if (inc == jnc) then
                      incq = nnq(inc)
                      ncomx = ncoumx(incq)
                      call surf_vec (id1, inpcou, k1i, inpcq, indxi, &
                           suri)
                      isig = npocob(k1i,inpc)
                      facij1 = SQRT(REAL(npelsh(k1i,inpc),wp))

! calculate delta p for the n+1 configuration
                      idelp = 0
                      do ik = k1i+1, n1shi
                         idelp = idelp + npelsh(ik,inpc)
                      end do
                      idelps = 1
                      if (MOD(idelp,2) == 1) idelps = -1

                      cup = 0.0_wp
                      do ii = 1, SIZE(indxi)
                         if (indxi(ii) == jncou) then
                            cup = suri(ii)
                            exit
                         end if
                      end do
                      if (ABS(cup) > eps) then
                         ncount = ncount + 1
                         bc1(ncount) = cup * facij1 * idelps
                         irs1(ncount) = isig + (maxorb + 1) * jcsn
                      end if
                      cycle j_cfgs
                   end if
                end do k1i_loop
! end of search for identical configurations; no interacting shells 
! found so does not write anything
             end do j_cfgs

             if (ncount > 0) then
                if (bug7 == 6) then
                   write (fo,'(i5)') icsnp
                   do i = 1, ncount
                      isig = MOD(irs1(i), maxorb+1)
                      jcsn = irs1(i) / (maxorb + 1)
                      write (fo,'(7x,2i5,e15.5)') jcsn, isig, bc1(i)
                   end do
                end if
                imbc(icsnp,jsn,isnp) = ncount
                call iwritb (jbufiv, irs1, 1, ncount)
                call writb (jbuffv, bc1, 1, ncount)
                ntotal = ntotal + 1
                itotal = itotal + SUM(irs1(:ncount))
                xtotal = xtotal + SUM(bc1(:ncount))
                atotal = atotal + SUM(ABS(bc1(:ncount)))
             end if
          end do i_cfgs
          end if
       end do j_syms
       write (fo,'(i4,2i10,2e23.15)') isnp, ntotal, itotal, &
            xtotal, atotal
    end do i_syms

    call endw (jbufiv)
    call endw (jbuffv)
    call writix (jbufiv)

    call iwrita (jbufiv, npset)
    if (ASSOCIATED(npcset)) then 
       call iwritb (jbufiv, npcset, 1, npset)
    else
       allocate(npcset_d(npset), stat=status)
       npcset_d(1:npset) = 0
       call iwritb (jbufiv, npcset_d, 1, npset)
       deallocate(npcset_d, stat=status)
       if (status /= 0) call alloc_error (status, 'angmbc', 'a')
    end if
    call iwritb (jbufiv, lpset, 1, npset)
    call iwritb (jbufiv, ispset, 1, npset)
    call iwritb (jbufiv, ippset, 1, npset)
    call iwrita (jbufiv, maxorb)
    do i = 1, npset
       call iwritb (jbufiv, imbci(:,i), 1, nset)
    end do
    indic = 0
    if (ncsy /= 0) then
    if (ASSOCIATED(npcset)) then 
       do i = 1, npset
          call iwritc (jbufiv, imbc(:,:,i), 1, npcset(i), 1, nset, &
               ncsy, indic)
       end do
!1    else
 !      allocate(npcset_d(npset), stat=status)
 !      npcset_d(1:npset) = 0
 !      do i = 1, npset
 !         call iwritc (jbufiv, imbc(:,:,i), 1, npcset_d(i), 1, nset, &
 !              ncsy, indic)
 !      end do
 !      deallocate(npcset_d, stat=status)
!       if (status /= 0) call alloc_error (status, 'angmbc', 'a')
    end if
    end if
    call endw (jbufiv)
!
    if (bug7 > 0) then
       write (fo,'(//,a,i5)') 'data on index of abcmi and abcmr:     &
            &npset = ', npset
       write (fo,'(//,a)') 'imbci'
       do j = 1, npset
          write (fo,'(8x,9(2x,i6))') imbci(1:nset,j)
       end do
       if (ncsy /= 0) then
          write (fo,'(//,a)') 'imbc'
          do j = 1, npset
             do k = 1, nset
                write (fo,'(8x,9(2x,i6))') imbc(1:npcset(j),k,j)
             end do
          end do
       end if
    end if

    stats = 'keep'
    call close (jbufiv, stats)
    call close (jbuffv, stats)
    write (fo,'(a,//)') 'ANGMBC complete'
  end subroutine angmbc
end module bc_multipoles

