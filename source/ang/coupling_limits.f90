module coupling_limits
! coupling limit data
! Time-stamp: "03/02/24 15:13:07 cjn"
  implicit none

  integer, save     :: lims(4,4)
  integer, save     :: maxln, minln, maxsn, minsn
  integer, save     :: maxlm, minlm, maxsm, minsm

  private
  public set_limits, climits
  public maxln, maxsn, minsn, minln
  public maxlm, maxsm, minsm, minlm

contains

  subroutine climits (n, m)
    integer, intent(in)           :: n
    integer, intent(in), optional :: m
    maxln = lims(1,n)
    minln = lims(2,n)
    maxsn = lims(3,n)
    minsn = lims(4,n)
    if (present(m)) then
       maxlm = lims(1,m)
       minlm = lims(2,m)
       maxsm = lims(3,m)
       minsm = lims(4,m)
    end if
  end subroutine climits

  subroutine set_limits (n, lx, ln, sx, sn)
    integer, intent(in) :: n
    integer, intent(in) :: lx, ln, sx, sn
    lims(:,n) = (/lx, ln, sx, sn/)
  end subroutine set_limits
end module coupling_limits
