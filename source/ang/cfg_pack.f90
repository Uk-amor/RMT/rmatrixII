module cfg_pack
! Manipulation of packed configuration data
! Time-stamp: "2005-03-11 15:17:31 cjn"
  use io_units, only: fo
  use error_prt, only: alloc_error
  implicit none

  integer, allocatable, save   :: mulorb(:) ! multiplication factors
  integer, allocatable, save   :: muqorb(:,:) ! orbital location
  integer, save                :: imax      ! max integer size
  integer, save                :: npk = 1   ! num_pack_integers
  integer, allocatable, save   :: nob(:)    ! orbital -> pack component
  integer, allocatable, save   :: q(:)      ! pack components

! Define a packed data type:
  type pck
     private
     integer, pointer          :: p(:)=>NULL()
  end type pck
  type(pck), pointer, save     :: npack(:)
  type(pck), pointer, save     :: npackp(:)
  type(pck), pointer, save     :: ppack(:)

  interface assignment (=)
     module procedure cppack
  end interface

  interface operator (==)
     module procedure eqpack
  end interface

  private
  public :: pck, def_factors, pack, adpack, mspack, npack
  public :: def_muqorb, zpck, incpck, npackp, incpckq
  public :: operator(==)
  public :: assignment(=)
  public :: def_pck, del_pck, cpy_pck

contains

  subroutine incpckq (m, orbq_last, njqc, njq_mx, lj, nel)
! update of pre-defined pck variable
! orbq_last ensures that n-values are monotonically increasing
    type(pck), intent(inout)   :: m          ! pack variable
    integer, intent(inout)     :: orbq_last ! last orbital index
    integer, intent(inout)     :: njqc       ! current n-value
    integer, intent(in)        :: njq_mx     ! max n-value for lj
    integer, intent(in)        :: lj         ! orbital angular momentum
    integer, intent(in)        :: nel        ! # electrons in shell
    integer                    :: njq, njq_1, orbq, np

    njq_1 = njqc + 1
    do njq = njq_1, njq_mx
       orbq = muqorb(lj+1, njq)
       if (orbq == 0) then ! should never occur
          write (fo,'(a)') 'incpckq: muqorb  error'
          stop
       end if
       if (orbq > orbq_last) then
          njqc = njq
          orbq_last = orbq
          np = nob(orbq)          ! pack component
          m%p(np) = m%p(np) + mulorb(orbq) * nel
          return
       else if (orbq == orbq_last) then ! should not occur
          write (fo,'(a)') 'incpckq: orbq = orbq_last error'
          stop
       end if
    end do
    write (fo,'(a)') 'incpckq: njq not found'
    stop
  end subroutine incpckq
          
  subroutine def_factors (mxshells, nelmax)
 ! Define multiplication factors and # of packing integers
    integer, intent(in)    :: mxshells
    integer, intent(in)    :: nelmax(:)
    integer                :: maxorb, status, i1
    integer                :: s1, s2

    write (fo,'(/,a)') 'Multiplication factors, mulorb: '
    maxorb = SIZE(nelmax)
    imax = HUGE(maxorb)
    allocate (mulorb(maxorb), nob(maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'def_factors', 'a')

    if (maxorb > 0) then
       mulorb(1) = 1
       nob(1) = npk
    end if
    npk = 1
    s1 = 1
    do i1 = 2, maxorb
       if (mulorb(i1-1) > imax / (nelmax(i1-1) + 1)) then ! overflow
          npk = npk + 1
          mulorb(i1-1) = 1
          nob(i1-1) = npk
          write (fo,'(i6,a,i12)') i1-1, ' mulorb = ', mulorb(i1-1)
          s2 = i1 - 2
          if (.NOT.chk_rep(s1,s2,mxshells)) then ! disaster
             write (fo,'(a,i4)') 'def_factors: rep failure, npk = ', npk
             stop
          end if
          mulorb(i1) = mulorb(i1-1) * (nelmax(i1-1) + 1)
          s1 = i1 - 1
          nob(i1) = npk
       else
          mulorb(i1) = mulorb(i1-1) * (nelmax(i1-1) + 1)
          nob(i1) = npk
       end if
       write (fo,'(i6,a,i12)') i1, ' mulorb = ', mulorb(i1)
    end do

    write (fo,'(a,i4)') 'Number of packing components, npk = ', npk
    allocate (q(npk), stat=status)
    if (status /= 0) call alloc_error (status, 'def_factors', 'a')
  contains

    function chk_rep (s1, s2, mxshells)
      logical              :: chk_rep
      integer, intent(in)  :: s1, s2
      integer, intent(in)  :: mxshells
      integer              :: itest, ni, inc, i

      chk_rep = .true.
      ni = MAX(s1, s2 + 1 - mxshells)
      itest = 0
      do i = s2, ni, -1
         inc = mulorb(i) * nelmax(i)
         if (itest > imax - inc) then ! overflow
            chk_rep = .false.
            exit
         end if
         itest = itest + inc
      end do
    end function chk_rep
  end subroutine def_factors

  subroutine def_pack (ncf)
    integer, intent(in)     :: ncf
    integer                 :: status, in, ncf_dim

    ncf_dim = MAX(ncf,1)
!    allocate (ppack(ncf_dim), stat=status)
    allocate (ppack(ncf), stat=status)
    if (status /= 0) call alloc_error (status, 'pack', 'a')
    do in = 1, SIZE(ppack)
       allocate (ppack(in)%p(npk), stat=status)
       if (status /= 0) call alloc_error (status, 'pack', 'a')
       ppack(in)%p = 0      ! initialization
    end do
  end subroutine def_pack

  subroutine pack (nefig, noccsh, nocorb, nelcsh, ncf, norn1)
! pack the n electron configurations:
    integer, intent(in)   :: nefig       ! # cfgs
    integer, intent(in)   :: noccsh(:)   ! # shells in cfg
    integer, intent(in)   :: nocorb(:,:) ! orb of shell, cfg
    integer, intent(in)   :: nelcsh(:,:) ! occ of shell, cfg
    integer, intent(in)   :: ncf
    integer, intent(in)   :: norn1       ! 1=N, 2=N+1
    integer               :: j, in, noc, iorb, ioc

    call def_pack (ncf)
    do in = 1, nefig
       noc = noccsh(in)
       q = 0
       do ioc = 1, noc
          iorb = nocorb(ioc,in)
          j = nob(iorb)
          q(j) = q(j) + mulorb(iorb) * nelcsh(ioc,in)
       end do
       ppack(in)%p = q
    end do
    if (norn1 == 1) then
       npack => ppack
    else
       npackp => ppack
    end if
  end subroutine pack

  subroutine adpack (m, n, orb)
! add electron to packed cfg
    type(pck), intent(inout) :: m
    type(pck), intent(in)    :: n
    integer, intent(in)      :: orb
    integer                  :: np

    m%p = n%p
    np = nob(orb)
    m%p(np) = n%p(np) + mulorb(orb)
  end subroutine adpack

  subroutine mspack (m, n, orb)
! subtract electron from packed cfg
    type(pck), intent(inout) :: m ! N-1 electron packed cfg
    type(pck), intent(in)    :: n  ! N electron packed cfg
    integer, intent(in)      :: orb ! orbital of removed electron
    integer                  :: np

    m%p = n%p
    np = nob(orb)
    m%p(np) = n%p(np) - mulorb(orb)
  end subroutine mspack

  subroutine cppack (m, n)
! copy pck types
    type(pck), intent(in)  :: n
    type(pck), intent(out) :: m
    integer                :: status
    if (.NOT.associated(m%p)) then
       allocate (m%p(npk), stat=status)
       if (status /= 0) call alloc_error (status, 'cppack', 'a')
    end if
    m%p = n%p
  end subroutine cppack

  subroutine zpck (m)
! zero pck type
    type(pck), intent(out) :: m
    integer                :: status
    if (.NOT.associated(m%p)) then
       allocate (m%p(npk), stat=status)
       if (status /= 0) call alloc_error (status, 'cppack', 'a')
    end if
    m%p = 0
  end subroutine zpck

  subroutine incpck (m, l, n, nel)
! update of pre-defined pck variable
    type(pck), intent(inout)   :: m     ! pack variable
    integer, intent(in)        :: l, n  ! l+1, n q. #s
    integer, intent(in)        :: nel   ! # electrons
    integer                    :: orb, np

    orb = muqorb(l, n)     ! corresponding orbital
    if (orb == 0) then
       write (fo,'(a)') 'incpck error'
       stop
    end if
    np = nob(orb)          ! pack component
    m%p(np) = m%p(np) + mulorb(orb) * nel
  end subroutine incpck

  function eqpack (n, m)
! test equality of pck types
    logical                :: eqpack
    type(pck), intent(in)  :: n
    type(pck), intent(in)  :: m
    integer                :: i

    eqpack = .true.
    do i = 1, npk
       if (n%p(i) /= m%p(i)) then
          eqpack = .false.
          exit
       end if
    end do
  end function eqpack

  subroutine def_muqorb (njcomp, ljcomp)
! form muqorb(lp1,n), giving shell as a function of l+1 and n
    integer, intent(in) :: njcomp(:)
    integer, intent(in) :: ljcomp(:)
    integer             :: n_max, l_max, status, iorb, lp1, n

    n_max = MAXVAL(njcomp)
    l_max = MAXVAL(ljcomp) + 1
    allocate (muqorb(l_max,n_max), stat=status)
    if (status /= 0) call alloc_error (status, 'def_muqorb', 'a')
    muqorb = 0
    do iorb = 1, SIZE(njcomp)
       lp1 = ljcomp(iorb) + 1
       n = njcomp(iorb)
       muqorb(lp1,n) = iorb
    end do
  end subroutine def_muqorb

  subroutine def_pck (pv, ncf)
    integer, intent(in)     :: ncf
    type(pck), pointer      :: pv(:), pvn(:)
    integer                 :: status, in

    allocate (pvn(ncf), stat=status)
    if (status /= 0) call alloc_error (status, 'def_pck', 'a')
    do in = 1, ncf
       allocate (pvn(in)%p(npk), stat=status)
       if (status /= 0) call alloc_error (status, 'def_pck', 'a')
       pvn(in)%p = 0      ! initialization
    end do
    pv => pvn
  end subroutine def_pck

  subroutine del_pck (pv)
    type(pck), pointer      :: pv(:)
    integer                 :: status, in

    do in = 1, SIZE(pv)
       deallocate (pv(in)%p, stat=status)
!      if (status /= 0) call alloc_error (status, 'del_pck', 'd')
    end do
    deallocate (pv, stat=status)
!   if (status /= 0) call alloc_error (status, 'del_pck', 'd')
  end subroutine del_pck

  subroutine cpy_pck (pv, pvn)
    type(pck), pointer      :: pv(:)
    type(pck), pointer      :: pvn(:)
    integer                 :: in

    do in = 1, SIZE(pv)
       pvn(in)%p = pv(in)%p
    end do
  end subroutine cpy_pck
end module cfg_pack
