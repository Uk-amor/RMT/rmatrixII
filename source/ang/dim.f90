module dim
! Time-stamp: "2003-03-26 08:50:27 cjn"
  implicit none

  integer, save    :: ldim0, ldim1, ldim2

  private
  public ldim0, ldim1, ldim2, ldim

contains
  subroutine ldim
! defines the value of the variable ildim9
    use startup, only: lrang1, lrglup
    use symmetries, only: lrang3
    integer            :: iddim, lr1, lr3

    lr1 = lrang1   ! max N-electron target orbital l+1
    lr3 = lrang3   ! max N-electron state L+1
    iddim = MAX(lr1, lr3-1)   ! NB. lr1 not lr1-1
    ldim1 = iddim + iddim + 1
    ldim2 = iddim + lrglup + 2
    ldim0 = 4 * (lr1 - 1)
    ldim0 = MAX(ldim0,1)
  end subroutine ldim

end module dim
