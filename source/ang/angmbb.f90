module bb_multipoles
! Time-stamp: "2005-03-11 15:28:30 cjn"
  use precisn, only: wp
  use io_units, only: fo, jbufiv, jbuffv
  implicit none

  real(wp), parameter   :: eps = 1.0e-5_wp

! diagnostic sum data:
  integer, save         :: ntotal ! # elements output
  integer, save         :: nnines ! # zero writes
  integer, save         :: itotal ! index sum
  real(wp), save        :: xtotal ! element sum
  real(wp), save        :: atotal ! sum of ABS value of elements

  private
  public angmbb

contains
  subroutine angmbb (nelc, nset, lset, isset, ipset, noccsh, nocorb, &
       nelcsh, nmefig, nnq, lssetm, mcoumx, mmq, id1)
! calculate the direct angular integral over target configurations.
    use symmetries, only: isran2
    use dim, only: ldim2
    use filehand, only: open, readix, ireada, ireadb, newfil, catlog, &
         lookup, iwrita, endw, writix, iwritb, close, iwritc, writa
    use startup, only: ljcomp
    use debug, only: bug7, bug9
    use angular_momentum, only: dracah, rme
    use surfacing_tables, only: surf_vec
    use sym_sets, only: get_ncset, get_ncfg
    use error_prt, only: alloc_error
    integer, intent(in)  :: nelc        ! # electrons
    integer, intent(in)  :: nset       ! # SLpi cfg sets
    integer, intent(in)  :: lset(:)    ! cfg set L values
    integer, intent(in)  :: isset(:)   ! cfg set S values
    integer, intent(in)  :: ipset(:)   ! cfg set Pi values
    integer, intent(in)  :: noccsh(:)
    integer, intent(in)  :: nocorb(:,:)
    integer, intent(in)  :: nelcsh(:,:)
    integer, intent(in)  :: nmefig(:,:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: lssetm    ! LS q.# set index
    integer, intent(in)  :: mcoumx(:)
    integer, intent(in)  :: mmq(:)
    integer, intent(in)  :: id1

    character(len=7)     :: filea, fileb, stats
    integer              :: ivcont((nset*(nset+1))/2+1)
    integer              :: lamij(nset,nset)
    real(wp)             :: ashel(isran2+4,0:ldim2 + 3)
    integer              :: nines = 9999
    logical              :: interact
    integer              :: ndummy, isjs, is, lcfg, lcfg2, ispin
    integer              :: ipi, js, lamlom, lamupm, lcfgp, lcfgp2
    integer              :: jspin, jpi, nrecv, ibufv, lam1, lam2, lcfgs
    integer              :: iscur1, iscur2, ics, icou, ic, icq
    integer              :: jcslo, jcs, jcou, jc, jcq, ki, kj
    integer              :: ksheli, kshelj, irho, isig, nsheli, nshelj
    integer              :: lrho, lsig, lrho2, lsig2, lmmax, lmmin
    integer              :: lmmins, nmq
    integer              :: npoint, idelp, ik, ishel, jk, jshel, idelps
    integer              :: lsigs, lamlo, lamup, lam, lamt2, lsign
    integer              :: lmval, lmval2, ism, irsl, nsh, inr, nme
    integer              :: irs
    real(wp)             :: fac1, fac2, facdir, rac, vshval, vshel
    real(wp)             :: ashsum
    real(wp), pointer    :: suri(:), surj(:)
    integer, pointer     :: indxi(:), indxj(:)
    integer, pointer     :: ncset(:), ncfga(:,:), ncfgb(:,:)
    logical              :: real_typ
    integer, allocatable :: ncset_d(:)
    integer              :: status  

    write (fo,'(11x,a,//)') 'ANGMBB: Multipole BB Integrals'
    if (bug7 == 1) then
       write (fo,'(a)') '      is    js   ivcont lamij'
       write (fo,'(a,/)') '    irls     vshval       irsks      exsval'
    end if

    ncset => get_ncset (2)
    call get_ncfg (2, ncfga, ncfgb)

! initialise writing to all output files
    filea = 'abbmi'
    fileb = 'abbmr'
    stats = 'new'
    real_typ = .false.
    call open (jbufiv, filea, stats, real_typ)
    real_typ = .true.
    call open (jbuffv, fileb, stats, real_typ)
    call newfil (jbufiv, ndummy)
    call newfil (jbuffv, ndummy)

    write (fo,'(/,a,/)') ' set       count     0-count       i-sum &
         &               sum                  abs-sum'
    isjs = 0
    i_symmetries: do is = 1, nset
       ntotal = 0
       nnines = 0
       itotal = 0
       xtotal = 0.0_wp
       atotal = 0.0_wp

       lcfg = lset(is)
       lcfg2 = lcfg + lcfg
       ispin = isset(is)
       ipi = ipset(is)
       j_symmetries: do js = is, nset
          isjs = isjs + 1
          call catlog (jbufiv, ivcont(isjs))
! set lamij = -999; reset to limits of lambda for sets is, js
          lamij(is,js) = -999
          lamij(js,is) = -999

!      initialise limits on direct lambda for this set pair
          lamlom = 999
          lamupm = 0

          lcfgp = lset(js)
          lcfgp2 = lcfgp + lcfgp
          jspin = isset(js)
          jpi = ipset(js)

          if (ispin /= jspin) then ! spins not equal => no integral
             if (bug7 == 1) then
                call lookup (ivcont(isjs), nrecv, ibufv)
                write (fo,'(7(4x,i5))') is, js, nrecv, ibufv, &
                     lamij(is,js)
             end if
             cycle j_symmetries
          end if

! calculate lambda limits using triangular relation and parity rule
          lam1 = ABS(lcfg - lcfgp)
          lam2 = lcfg + lcfgp
          if (MOD(lam1 + ipi + jpi,2) /= 0) then
             lam1 = lam1 + 1
             lam2 = lam2 - 1
             if (lam1 > lam2) then ! lambda limits => no integral
                if (bug7 == 1) then
                   call lookup (ivcont(isjs), nrecv, ibufv)
                   write (fo,'(7(4x,i5))') is, js, nrecv, ibufv, &
                        lamij(is,js)
                end if
                cycle j_symmetries
             end if
          end if

! calculate spin limits of top of N-1 elec tree and parameters to access
! table of spin racah coefficients spinw
          iscur1 = ispin - 1
          iscur2 = iscur1 + 2
          if (iscur1 == 0) iscur1 = iscur2
! calculate factors which are independent of the shells
          fac1 = ((lcfg2+1) * (lcfgp2+1))**0.5_wp
          lcfgs = 1
          if (MOD(lcfg+lcfgp,2) == 1) lcfgs = -1

! calculate interaction between each cfg in set with each in set js
          if (ASSOCIATED(ncset)) then
          i_cfg: do ics = 1, ncset(is)
             icou = ncfgb(ics,is)
             ic = ncfga(ics,is)
             icq = nnq(ic)

! allow for symmetry in limits if is=js
             if (is == js) then
                jcslo = ics
             else
                jcslo = 1
             end if

             j_cfg: do jcs = jcslo, ncset(js)
                jcou = ncfgb(jcs,js)
                jc = ncfga(jcs,js)
                jcq = nnq(jc)

                if (ic /= jc) then

! test whether configs ic,jc can interact
! The n-1 electron cfg must be the same if an electron is removed from
! ksheli in ic and kshelj in jc (also determines interacting shells)
                   interact = .false.
                   test: do ki = 1, noccsh(ic)
                      nme = nmefig(ki,ic)
                      do kj = 1, noccsh(jc)
                         if (nme == nmefig(kj,jc)) then
                            ksheli = ki
                            kshelj = kj
                            interact = .true.
                            exit test
                         end if
                      end do
                   end do test
                   if (.NOT.interact) then
                      call iwrita (jbufiv, nines)
                      call writa (jbuffv, 0.0_wp)
                      nnines = nnines + 1
                      cycle j_cfg
                   end if

! find interacting shells krho and ksig
                   irho = nocorb(ksheli,ic)
                   isig = nocorb(kshelj,jc)
! find number of electrons in the interacting shells
                   nsheli = nelcsh(ksheli,ic)
                   nshelj = nelcsh(kshelj,jc)
! find angular momentum of the shells
                   lrho = ljcomp(irho)
                   lsig = ljcomp(isig)
                   lrho2 = lrho + lrho
                   lsig2 = lsig + lsig
! calculate L limits of top of N-1 electron tree
! parity of the N-1 system is ipi+lrho
                   lmmax = MIN(lcfg+lrho, lcfgp+lsig)
                   lmmin = MAX(ABS(lcfg-lrho), ABS(lcfgp-lsig))
                   if (lmmin > lmmax) then
                      call iwrita (jbufiv, nines)
                      call writa (jbuffv, 0.0_wp)
                      nnines = nnines + 1
                      cycle j_cfg
                   end if
                   lmmins = 1
                   if (MOD(lmmin+ipi+lrho,2) == 1) lmmins = -1
                   irs = 10000 * irho + 100 * isig

! obtain coupling from surfacing coeffs
                   if (nelc == 1) then  ! no surfacing coeffs
                      lmmax = 0
                      lmmin = 0
                      ashel(1,0) = 1.0_wp
                      ashel(3,0) = 0.0_wp
                   else
                      nmq = mmq(nme)
                      call surf_vec (id1, icou, ksheli, icq, indxi, &
                           suri)
                      call surf_vec (id1, jcou, kshelj, jcq, indxj, &
                           surj)
                      call cuple (indxi, suri, indxj, surj,         &
                           mcoumx(nmq), nmq, lssetm, ashel, npoint)
                   end if

! calculate delta p
                   idelp = 0
                   if (irho < isig) then
                      do ik = ksheli+1, noccsh(ic)
                         ishel = nocorb(ik,ic)
                         if (ishel > isig) exit
                         idelp = idelp + nelcsh(ik,ic)
                      end do
                   else
                      do jk = kshelj+1, noccsh(jc)
                         jshel = nocorb(jk,jc)
                         if (jshel > irho) exit
                         idelp = idelp + nelcsh(jk,jc)
                      end do
                   end if

! calculate the external factors of the angular integral
                   idelps = 1
                   if (MOD(idelp,2) == 1) idelps = -1
                   lsigs = 1
                   if (MOD(lrho+lsig,2) == 1) lsigs = -1
                   fac2 = idelps * lsigs * fac1 * &
                        (nsheli * nshelj)**0.5_wp
                   facdir = fac2 * lcfgs

! reset lambda to obey triangular relation with lrho,lsig
                   lamlo = MAX(lam1, ABS(lrho-lsig))
                   lamup = MIN(lam2, lrho+lsig)

! if not possible shells will not interact
                   if (lamlo <= lamup) then
                      lam_loop: do lam = lamlo, lamup, 2
                         lamt2 = lam + lam
                         vshel = 0.0_wp
                         lsign = lmmins
                         lm: do lmval = lmmin, lmmax
                            lmval2 = lmval + lmval   
                            call dracah (lcfg2, lcfgp2, lrho2, lsig2,&
                                 lamt2, lmval2, rac)
                            rac = rac * lsign
                            lsign = -lsign
                            ashsum = 0.0_wp
                            do ism = iscur1, iscur2, 2
                               ashsum = ashsum + ashel(ism,lmval)
                            end do
                            vshel = vshel + ashsum * rac
                         end do lm

! store the integral in vsh and rho,sig,lam in irhsgl
                         irsl = irs + lam
                         vshval = rme(lrho,lsig,lam) * vshel * facdir
                         if (bug7 == 1) write (fo,'(1x,i8,e15.5)') &
                              irsl, vshval
                         call iwrita (jbufiv, irsl)
                         call writa (jbuffv, vshval)
                         ntotal = ntotal + 1
                         itotal = itotal + irsl
                         xtotal = xtotal + vshval
                         atotal = atotal + ABS(vshval)
                      end do lam_loop
                      lamlom = MIN(lamlom, lamlo)
                      lamupm = MAX(lamupm, lamup)
                   else
                      call iwrita (jbufiv, nines)
                      call writa (jbuffv, 0.0_wp)
                      nnines = nnines + 1
                   end if

                else ! ic = jc => all shells can contribute
                   nsh = noccsh(ic)
                   k_sh: do ksheli = 1, nsh
                      irho = nocorb(ksheli,ic)
                      inr = 10000 * nsh + 100 * irho

                      nsheli = nelcsh(ksheli,ic)
                      lrho = ljcomp(irho)
                      lrho2 = lrho + lrho

! calculate L limits of the top of the N-1 electron tree
                      lmmax = MIN(lcfg+lrho, lcfgp+lrho)
                      lmmin = MAX(ABS(lcfg-lrho), ABS(lcfgp-lrho))
                      if (lmmin > lmmax) then
                         irsl = - inr - 99
                         call iwrita (jbufiv, irsl)
                         call writa (jbuffv, 0.0_wp)
                         ntotal = ntotal + 1
                         itotal = itotal + irsl
                         cycle k_sh
                      end if
                      lmmins = 1
                      if (MOD(lmmin+ipi+lrho,2) == 1) lmmins = -1

! obtain the coupling from the surfacing coefficients
                      if (nelc == 1) then ! no coefficients, 1S
                         lmmax = 0
                         lmmin = 0
                         ashel(1,0) = 1.0_wp
                         ashel(3,0) = 0.0_wp
                      else
                         nme = nmefig(ksheli,ic)
                         nmq = mmq(nme)
!                         mcomx = mcoumx(nmq)
                         call surf_vec (id1, icou, ksheli, icq, indxi, &
                              suri)
                         call surf_vec (id1, jcou, ksheli, icq, indxj, &
                              surj)
                         call cuple (indxi, suri, indxj, surj,         &
                              mcoumx(nmq), nmq, lssetm, ashel, npoint)
                      end if

! calculate the external factor of the angular integral
                      fac2 = fac1 * nsheli
                      facdir = fac2 * lcfgs
! reset lambda to obey triangular relation with lrho,lsig
                      lamlo = lam1
                      lamup = MIN(lam2, lrho2)
                      if (lamlo <= lamup) then ! shells interact
                         lam1_loop: do lam = lamlo, lamup, 2
                            lamt2 = lam + lam
                            vshel = 0.0_wp
                            lsign = lmmins
                            lm1: do lmval = lmmin, lmmax !701
                               lmval2 = lmval + lmval    
                               call dracah (lcfg2, lcfgp2, lrho2, &
                                    lrho2, lamt2, lmval2, rac)
                               rac = rac * lsign
                               lsign = -lsign
                               ashsum = 0.0_wp
                               do ism = iscur1, iscur2, 2
                                  ashsum = ashsum + ashel(ism,lmval)
                               end do
                               vshel = vshel + ashsum * rac
                            end do lm1
! store the integral in vsh  and -nsh,rho,lam in irhsgl
                            irsl = - inr - lam
                            vshval = rme(lrho,lrho,lam) * vshel * facdir
                            if (bug7 == 1) then
                               write (fo,'(1x,i8,e15.5)') irsl, vshval
                            end if
                            call iwrita (jbufiv, irsl)
                            call writa (jbuffv, vshval)
                            ntotal = ntotal + 1
                            itotal = itotal + irsl
                            xtotal = xtotal + vshval
                            atotal = atotal + ABS(vshval)
                         end do lam1_loop
                         lamlom = MIN(lamlom, lamlo)
                         lamupm = MAX(lamupm, lamup)
                      else   ! no interaction
                         irsl = - inr - 99
                         call iwrita (jbufiv, irsl)
                         call writa (jbuffv, 0.0_wp)
                         ntotal = ntotal + 1
                         itotal = itotal + irsl
                      end if
                   end do k_sh
                end if
             end do j_cfg
          end do i_cfg
          end if
          lamij(is,js) = lamlom * 100 + lamupm
     
          if (bug7 == 1) then
             call lookup (ivcont(isjs), nrecv, ibufv)
             write (fo,'(7(4x,i5))') is, js, nrecv, ibufv, lamij(is,js)
          end if

       end do j_symmetries

       write (fo,'(i4,3i12,2e23.15)') is, ntotal, nnines, itotal, &
            xtotal, atotal
    end do i_symmetries

    isjs = isjs + 1
    call catlog (jbufiv, ivcont(isjs))
    call endw (jbufiv)
    call endw (jbuffv)

! write index to integer files
    call writix (jbufiv)
    call iwrita (jbufiv, nset)
    if (ASSOCIATED(ncset)) then
       call iwritb (jbufiv, ncset, 1, nset)
    else
       allocate(ncset_d(nset), stat=status)
       ncset_d(1:nset) = 0
       call iwritb (jbufiv, ncset_d, 1, nset)
       deallocate(ncset_d, stat=status)
       if (status /= 0) call alloc_error (status, 'angmbb', 'a')
    end if
    call iwritc (jbufiv, lamij, 1, nset, 1, nset, nset, 0)
    call iwritb (jbufiv, ivcont, 1, isjs)
!
    stats = 'keep'
    call close (jbuffv, stats)
    call endw (jbufiv)
    call close (jbufiv, stats)
!
    if (bug9 > 0) then
       write (fo,'(//,a,i5)') 'data on index of abbmi:     nset = ',&
            nset
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'ncset', &
            ncset(1:nset)
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'lamij', &
            lamij(1:nset,1:nset)
       write (fo,'(//2x,a6,9(2x,i6)/(8x,9(2x,i6)))') 'ivcont',&
            ivcont(1:isjs)
    end if
    write (fo,'(a)') 'ANGMBB complete'
  end subroutine angmbb

  subroutine cuple (indmi, surmi, indmj, surmj, mcomx, vc, lsset, ash,&
       ipoint)
! sum over the intermediate l.
    use ls_states, only: lvec, svec
    integer, intent(in)     :: mcomx
    integer, intent(in)     :: indmi(:)
    integer, intent(in)     :: indmj(:)
    integer, intent(out)    :: ipoint
    real(wp), intent(in)    :: surmi(:)
    real(wp), intent(in)    :: surmj(:)
    integer, intent(in)     :: vc
    integer, intent(in)     :: lsset
    real(wp), intent(out)   :: ash(:,0:)
    real(wp)                :: t(mcomx), tj, cup
    integer                 :: imc, lmval, ismval, j
    integer, pointer        :: lvm(:), ipm(:)


    lvm => lvec(vc, lsset)
    ipm => svec(vc, lsset)
    ash = 0.0_wp ! coupling coefficients
    ipoint = 0   ! nonzero coefficient pointer
    t = 0.0_wp

! IBM essl routines:
!    call dsctr (SIZE(indmi), surmi, indmi, t)

    do imc = 1, SIZE(indmi)
       t(indmi(imc)) = surmi(imc)
    end do
    do imc = 1, SIZE(indmj)
       j = indmj(imc)
       tj = t(j)
       if (tj == 0.0_wp) cycle
       cup = tj * surmj(imc)
       if (ABS(cup) <= eps) cycle
       ipoint = 1
       lmval = lvm(j)
       ismval = ipm(j)
       ash(ismval,lmval) = ash(ismval,lmval) + cup
    end do
  end subroutine cuple
end module bb_multipoles
