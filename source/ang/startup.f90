module startup
! Time-stamp: "2005-08-19 16:56:07 cjn"
  use precisn, only: wp
  use io_units, only: fi, fo
  use error_prt, only: alloc_error
  implicit none

! orbits
  integer, save :: maxdep                 ! max # occupied shells
  integer, save :: nelc1

  integer, allocatable, save :: njcomp(:) ! orbital n-values
  integer, allocatable, save :: ljcomp(:) ! orbital l-values
  integer, allocatable, save :: multco(:) ! factrs, store q#s occ shells
  integer, allocatable, save :: nelmin(:) ! min occ of each shell
  integer, allocatable, save :: nelmax(:) ! maximum occupations
  integer, allocatable, save :: ncorel(:) ! max core n-value for each l
  integer, allocatable, target, save :: ncoret(:) ! temporary ncorel
  integer, allocatable, save :: njq_mx(:) ! max n_value for each l

  integer, save              :: lrgllo, lrglup, lrglb

  integer, save              :: lrang1
  integer, save              :: fsh
  integer, save              :: ncf         ! max # N+1 electron cfgs

  integer, pointer, save     :: noccsh(:) ! # occ shells for config
  integer, pointer, save     :: nocorb(:,:) ! occupied shells indicies
  integer, pointer, save     :: nelcsh(:,:) ! # electrons in shell
  integer, save              :: ncore       ! # core electrons

  integer, pointer, save     :: noccshp(:) ! # occ shells for N+1 config
  integer, pointer, save     :: nocorbp(:,:) ! occupied N+1 shells indicies
  integer, pointer, save     :: nelcshp(:,:) ! # electrons in N+1 shell

  integer, allocatable, save :: maxnc(:) ! highest n for each l of core
  integer, allocatable, save :: lj(:), nj(:)
  integer, save              :: sc_inorb, ps_inorb, mx_ncorr
  integer, allocatable, save :: sc_lj(:), sc_nj(:), sc_orb(:) ! super-core
  integer, allocatable, save :: ps_lj(:), ps_nj(:), ps_orb(:) ! pseudo

! temporary arrays:
  integer, allocatable       :: kocorb(:,:), kelcsh(:,:)
  integer, allocatable       :: kelmax(:), koccsh(:)

  private
  public stgard, wr_cfg
  public maxdep, njcomp, ljcomp, multco, nelmax, ncorel, ncoret, njq_mx
  public noccsh, nocorb, nelcsh, ncore, maxnc, nelmin, nelc1
  public lrgllo, lrglup, lrglb, lrang1, fsh, ncf
  public rdcfgp
  public noccshp, nocorbp, nelcshp
  public sc_inorb, sc_orb, ps_inorb, ps_orb, mx_ncorr

contains

  subroutine stgard (ndiagd)
! process the input data
    use rm_data, only: rd_nmlst, nelc, lrgle1, lrgle2, lrgld1, lrgld2,&
         lpmax, nset, maxorb, inorb, ncfg, ndiag, mpol, ncfgp, nocore
    use symmetries, only: rd_set_data, prt_set_data
    use cfg_pack, only: def_factors, def_muqorb, pack
    use vpack, only: initialize_vpack
    integer, intent(out)    :: ndiagd    ! diagonalization switch
    integer                 :: noc, iorb1, j, status, in
    integer                 :: iocold, n_max, lp1
    integer                 :: k, i, l, iorb, ioc, nel
    integer                 :: norb, nefig
    character(len=19), parameter :: lspec = 'spdfghspdfghijklmno'

    call rd_nmlst
    ndiagd = ndiag
    nefig = ncfg

    call title ! write run title
    write (fo,'(//,15x,a)') '==========='
    write (fo,'(15x,a)')  'Program ANG'
    write (fo,'(15x,a,//)') '==========='

! lrgllo, lrglup: min, max values of total a.m. of (n+1)-electron system
    if (lrgle1 < 0 .or. lrgle2 < 0) then
       lrgllo = lrgld1
       lrglup = lrgld2
       lrgle1 = -1
       lrglb = -1       ! bound term limit
    else
       lrgllo = MIN(lrgle1, lrgld1)
       lrglup = MAX(lrgle2, lrgld2)
       lrglb = lrgle2
    end if
    write (fo,'(a)') 'Angular coefficients:'
    write (fo,'(2(a,i2),/)') '(N+1)-electron total a.m. from ', &
         lrgllo, ' to ', lrglup
    if (lrglb >= 0) then
       write (fo,'(a,i4)') 'Bound terms for l <= ', lrglb
    else
       write (fo,'(a)') 'No bound terms'
    end if

! read the lspi for each (n-1)-electron symmetry;
! calculate npset, the number of n-electron symmetries, and
! construct the corresponding lspi's.

    call rd_set_data (nset)
    call prt_set_data

! set up the maxorb - inorb nl values using the default sequence
! 1s,2s,2p,3s,... Read in the remaining inorb nl values.
    if (inorb > maxorb) then
       write (fo,'(a,i4)') 'inorb = ', inorb
       write (fo,'(a,i4)') 'maxorb = ', maxorb
       write (fo,'(a)') 'inorb must be >= maxorb'
       stop
    end if
    iorb = maxorb - inorb   ! # default sequence orbitals
    allocate (nj(maxorb), lj(maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'stgard', 'a')
    k = 1;  i = 1
    k_loop: do while (k <= iorb)
       do j = 0, i-1
          nj(k) = i
          lj(k) = j
          k = k + 1
          if (k > iorb) exit k_loop
       end do
       i = i + 1
    end do k_loop
    if (inorb > 0) read (fi,*) (nj(i), lj(i), i=iorb+1,maxorb)
    write (fo,'(/,a,i3,a)') 'The following ', maxorb, ' bound &
         &orbitals are included in the calculation'
    write (fo,'(12(2x,i2,a1))') (nj(i), lspec(lj(i)+1:lj(i)+1), &
         i=1,maxorb)

    read (fi,*) sc_inorb, ps_inorb, mx_ncorr
    write (fo,'(a,i6)') 'Number of super-core orbitals = ', sc_inorb
    write (fo,'(a,i6)') 'Number of pseudo orbitals = ', ps_inorb
    write (fo,'(a,i6)') 'mx_ncorr = ', mx_ncorr
! read super-core orbitals (may have one or two holes)
    if (mx_ncorr <= 0) mx_ncorr = 2
    if (sc_inorb > 0) then
       allocate (sc_nj(sc_inorb), sc_lj(sc_inorb), stat=status)
       if (status /= 0) call alloc_error (status, 'stgard', 'a')
       read (fi,*) (sc_nj(i), sc_lj(i), i=1,sc_inorb)
       write (fo,'(/,a,i3,a)') 'The following ', sc_inorb, ' orbitals &
            &are super-core orbitals'
       write (fo,'(12(2x,i2,a1))') (sc_nj(i), &
            lspec(sc_lj(i)+1:sc_lj(i)+1), i=1,sc_inorb)
    end if

! read pseudo (corrector) orbitals (associated with high-lying states)
    if (ps_inorb > 0) then
       allocate (ps_nj(ps_inorb), ps_lj(ps_inorb), stat=status)
       if (status /= 0) call alloc_error (status, 'stgard', 'a')
       read (fi,*) (ps_nj(i), ps_lj(i), i=1,ps_inorb)
       write (fo,'(/,a,i3,a)') 'The following ', ps_inorb, ' orbitals &
            &are pseudo (corrector) orbitals'
       write (fo,'(12(2x,i2,a1))') (ps_nj(i), &
            lspec(ps_lj(i)+1:ps_lj(i)+1), i=1,ps_inorb)
    end if

    lrang1 = MAXVAL(lj) + 1

! if icon=0  set up default configurations,
!            else for all nefig=nconf configurations, read:
! noccsh, # of occupied shells for each configuration;
!         for each configuration ....
! kocorb, the indices specifying which shells are occupied;
! kelcsh, the number of electrons in each occupied shell;
!    if (icon == 0) then ! set up default configurations using config
!    else
    write (fo,'(//,a,/)') 'N-electron configuration data:'

    call rdcfg (nefig)

! first construct array of maximum occupation. only 2 electrons
! are allowed in f shells and higher due to restriction of
! fractional parentage tables
    allocate (kelmax(maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'stgard', 'a')
    do iorb = 1, maxorb
       if (lj(iorb) > 2) then
          kelmax(iorb) = 2
       else
          kelmax(iorb) = 4 * lj(iorb) + 2
       end if
    end do

    if (nocore == 0) then ! find # core orbitals, ncore
! find number of core orbitals, ncore
       ncore = maxorb
       do in = 1, nefig
          noc = koccsh(in)
          iorb1 = 0
          do ioc = 1, noc
             iorb = kocorb(ioc,in)
             nel = kelcsh(ioc,in)
             if (nel < kelmax(iorb)) exit
            iorb1 = iorb
          end do
          ncore = MIN(ncore, iorb1)
       end do
    else
       ncore = 0
    end if

! find highest orbital for each l in the core
    allocate (maxnc(lrang1), stat=status)
    if (status /= 0) call alloc_error (status, 'stgard', 'a')
    do l = 1, lrang1
       maxnc(l) = l - 1
    end do
    do iorb = 1, ncore
       lp1 = lj(iorb) + 1
       maxnc(lp1) = nj(iorb)
    end do

! estimate # N+1 electron cfgs, ignore possible duplications
    ncf = nefig
! delete the core from each configuration
    allocate (noccsh(ncf), nocorb(fsh,ncf), nelcsh(fsh,ncf), &
         stat=status)
    if (status /= 0) call alloc_error (status, 'stgard', 'a')
    do in = 1, nefig
       noc = koccsh(in)
       ioc = 0
       do iocold = ncore+1, noc
          nocorb(iocold-ncore,in) = kocorb(iocold,in) - ncore
          nelcsh(iocold-ncore,in) = kelcsh(iocold,in)
       end do
    end do
    do in = 1, nefig
       noccsh(in) = koccsh(in) - ncore
    end do

    n_max = maxorb - ncore
    allocate (njcomp(n_max), ljcomp(n_max), nelmax(n_max), &
         ncorel(lrang1), ncoret(lrang1), njq_mx(lrang1), stat=status)
    if (status /= 0) call alloc_error (status, 'stgard', 'a')
    i = 0
    do iorb = ncore+1, maxorb
       ljcomp(iorb-ncore) = lj(iorb)
       njcomp(iorb-ncore) = nj(iorb)
       nelmax(iorb-ncore) = kelmax(iorb)
    end do
    maxorb = maxorb - ncore
! define ncorel, the maximum n-values for each l-val in core
! assume orbitals are defined in canonical order as above
    write (fo,'(/,a)') 'Maximum n-value for each angular momentum:'
    do i = 1, lrang1
       if (.NOT.ANY(lj == i-1)) then ! problem
          write (fo,'(a)') 'No orbital for l = ', i-1
          ncorel(i) = 0
          cycle
       end if
       if (ANY(lj(1:ncore) == i-1)) then ! core orb exists
          ncorel(i) = MAXVAL(nj(1:ncore), lj(1:ncore)==i-1)
       else        ! no core orbital for this l-value
          ncorel(i) = i - 1
       end if
       if (ANY(lj == i-1)) then ! orb exists for l-value
          njq_mx(i) = MAXVAL(nj, lj==i-1)
       else        ! no core orbital for this l-value
          njq_mx(i) = i - 1
       end if
       write (fo,'(a,i4,2(3x,a,i4))') 'l-value = ', i-1, ' core n-&
            &value = ', ncorel(i), ' max n-value = ', njq_mx(i)
    end do

! find the maximum number of occupied shells in the cortex
    maxdep = MAXVAL(noccsh(:nefig))
!  subtract the number of electrons in the core
    nelc1 = nelc - SUM(kelmax(1:ncore))

! for easy comparison cfgs are packed using a multiplication factor
! for each shell. Calculate the multiplication factors mulorb.
    call def_factors (MIN(maxdep+1,maxorb), nelmax)
! pack the n electron configurations:
    call pack (nefig, noccsh, nocorb, nelcsh, ncf, 1)

! form muqorb(lp1,n), giving orbital as a function of l+1 and n
    call def_muqorb (njcomp, ljcomp)


! find the minimum occupation of each shell in the target configurations

    allocate (nelmin(maxorb), multco(maxdep+1), stat=status)
    if (status /= 0) call alloc_error (status, 'stgard', 'a')
    nelmin = 100
    do in = 1, nefig
       noc = noccsh(in)
       ioc = 1
       do iorb = 1, maxorb
          norb = nocorb(ioc,in)
          if (norb == iorb) then
             nel = nelcsh(ioc,in)
             ioc = MIN(ioc+1, noc)
          else
             nel = 0
          end if
          nelmin(iorb) = MIN(nelmin(iorb), nel)
       end do
    end do

    call initialize_vpack (maxorb, lrang1)! define vpack packing factors

    if (ncfgp <= 0) then
       deallocate (nj, lj, koccsh, kocorb, kelcsh, kelmax, stat=status)
    else
       deallocate (koccsh, kocorb, kelcsh, kelmax, stat=status)
    end if
    if (status /= 0) call alloc_error (status, 'stgard', 'a')

    if (sc_inorb > 0) call def_sc
    if (ps_inorb > 0) call def_ps

    write (fo,'(a)') 'stgard completed'
  end subroutine stgard

  subroutine rdcfg (nefig)
! read configuration data:
! read the number of occupied shells for ncfg cfgs,
! then for each configuration read the index for the occupied shells
! and the number of electrons in the shell
    use rm_data, only: nelc
    integer, intent(in)   :: nefig
    integer               :: status, i, n, nelec

    write (fo,'(a,i3)') 'number of configurations = ', nefig
    allocate (koccsh(nefig), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfg', 'a')
    read (fi,*) koccsh
    fsh = MAXVAL(koccsh) + 2
    allocate (kocorb(fsh,nefig), kelcsh(fsh,nefig), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfg', 'a')
    do i = 1, nefig
       n = koccsh(i)
       read (fi,*) kocorb(1:n,i)
       read (fi,*) kelcsh(1:n,i)
       call wr_cfg (i, kelcsh(1:n,i), nj(kocorb(1:n,i)), &
            lj(kocorb(1:n,i)))
       nelec = SUM(kelcsh(1:n,i))
       if (nelec /= nelc) then
          write (fo,'(a)') 'rdcfg: configuration input error'
          write (fo,'(a)') 'incorrect number of electrons'
          stop
       end if
    end do
  end subroutine rdcfg

  subroutine rdcfgp (npefig)
! read N+1 electron configuration data:
! read the number of occupied shells for ncfgp cfgs,
! then for each configuration read the index for the occupied shells
! and the number of electrons in the shell
    use rm_data, only: nelc
    use cfg_pack, only: pack
    integer, intent(in)   :: npefig
    integer               :: status, i, n, in, ioc, noc
    integer               :: iocold, fshp, nelecp

    write (fo,'(a,i3)') 'number of N+1 electron configurations = ', &
         npefig
    allocate (noccshp(npefig), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'a')
    read (fi,*) noccshp
    fshp = MAXVAL(noccshp(1:npefig))
    allocate (nocorbp(fshp,npefig), nelcshp(fshp,npefig), stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'a')
    do i = 1, npefig
       n = noccshp(i)
       read (fi,*) nocorbp(1:n,i)
       read (fi,*) nelcshp(1:n,i)
       if (MAXVAL(nelcshp(1:n,i)) > 10) then
          write (fo,'(a,2i6)') 'Maximum exceeded: i, MAXVAL(nelcshp) &
               &= ', i, MAXVAL(nelcshp(1:n,i))
          stop
       end if
       call wr_cfg (i, nelcshp(1:n,i), nj(nocorbp(1:n,i)), &
            lj(nocorbp(1:n,i)))
       nelecp = SUM(nelcshp(1:n,i))
       if (nelecp /= nelc+1) then
          write (fo,'(a)') 'rdcfgp: configuration input error'
          write (fo,'(a)') 'incorrect number of electrons'
          stop
       end if
    end do
! remove core
    do in = 1, npefig
       noc = noccshp(in)
       ioc = 0
       do iocold = ncore+1, noc
          nocorbp(iocold-ncore,in) = nocorbp(iocold,in) - ncore
          nelcshp(iocold-ncore,in) = nelcshp(iocold,in)
       end do
    end do
    do in = 1, npefig
       noccshp(in) = noccshp(in) - ncore
    end do
    write (fo,'(/,a)') 'N+1 electron configurations: core removed'
    do i = 1, npefig
       n = noccshp(i)
       call wr_cfg (i, nelcshp(1:n,i), njcomp(nocorbp(1:n,i)), &
            ljcomp(nocorbp(1:n,i)))
    end do

    call pack (npefig, noccshp, nocorbp, nelcshp, npefig, 2)
    deallocate (nj, lj, stat=status)
    if (status /= 0) call alloc_error (status, 'rdcfgp', 'd')
  end subroutine rdcfgp

  subroutine title
! write a general title indicating the target ion/atom,
! # continuum electrons, highest electron energy
    use rm_data, only: nelc, nz, coment
    character(len=2), parameter    :: atom(57) = (/ &
         ' H', 'He', 'Li', 'Be', ' B', ' C', ' N', ' O', ' F', &
         'Ne', 'Na', 'Mg', 'Al', 'Si', ' P', ' S', 'Cl', 'Ar', &
         ' K', 'Ca', 'Sc', 'Ti', ' V', 'Cr', 'Mn', 'Fe', 'Co', &
         'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', &
         'Rb', 'Sr', ' Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', &
         'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', ' I', 'Xe', &
         'Cs', 'Ba', 'La'/)
    character(len=1)                :: chrge
    character(len=8)                :: today
    character(len=10)               :: now
    integer                         :: nzeff

    call date_and_time (today, now)
! today: ccyymmdd
    write (fo,'(a,a2,a,a2,a,a4)') 'Job run on ', today(7:8), &
         '/', today(5:6), '/', today(1:4)
! now: hhmmss.sss
    write (fo,'(a,a2,a,a2,a,a5)') 'Time: ', now(1:2), ':', &
         now(3:4), ':', now(5:10)
    if (coment /= ' ') write (fo,'(a80)') coment

    nzeff = nz - nelc
    if (nzeff > 0) then
       chrge = '+'
    else if (nzeff < 0) then
       chrge = '-'
       nzeff = - nzeff
    end if

    if (nzeff > 1) then
       write (fo,'(5x,37("*"))')
       write (fo,'(5x,"*",35x,"*")')
       if (nzeff < 10) then
          write (fo,'(5x,"*",29x,i1,a1,4x,"*")') nzeff, chrge
       else
          write (fo,'(5x,"*",29x,i2,a1,3x,"*")') nzeff, chrge
       end if

       if (nz > 57) then
          write (fo,'(5x,"*",4x,a,i4,6x,"*")') 'Electron scatter&
               &ing by Z = ', nz
       else
          write (fo,'(5x,"*",4x,a,a2,6x,"*")') 'Electron scatter&
               &ing by ', atom(nz)
       end if
       write (fo,'(5x,"*",35x,"*")')
       write (fo,'(5x,37("*"))')
    else
       write (fo,'(/,5x,37("*"))')
       write (fo,'(5x,"*",35x,"*")')
       if (nzeff == 1) write (fo,'(5x,"*",29x,a1,5x,"*")') chrge
       if (nz > 57) then
          write (fo,'(5x,"*",4x,a,i4,6x,"*")') 'Electron scatter&
               &ing by Z = ', nz
       else
          write (fo,'(5x,"*",4x,a,a2,6x,"*")') 'Electron scattering by ',&
               atom(nz)
       end if
       write (fo,'(5x,"*",35x,"*")')
       write (fo,'(5x,37("*"),/)')
    end if
  end subroutine title

  subroutine wr_cfg (n, occ, nval, lval)
    integer, intent(in)          :: n       ! cfg #
    integer, intent(in)          :: occ(:)  ! occupation #s
    integer, intent(in)          :: nval(:) ! principal q. #
    integer, intent(in)          :: lval(:) ! a.m. q. #
    integer                      :: nsh, i, fm, fmp, ad, lv
    character(len=20), parameter :: lspec = 'spdfghiklmnoqrtuvwxy'
    character(len=4), parameter  :: f(2) = (/'(i1)', '(i2)'/)
    character(len=4), parameter  :: fa(3) = (/'(a3)', '(a4)', '(a5)'/)
    character(len=4), parameter  :: x(2) = (/'(1x)', '(2x)'/)
    character(len=3), parameter  :: adv(2) = (/'yes', 'no '/)
    character(len=2)             :: cocc

    i = MAXVAL(lval)
    if (i > 19) then
       write (fo,'(a,i6)') 'wr_cfg: l-range exceeded, l_max = ', i
       return
    end if
    i = MAXVAL(occ)
    if (i > 10) then
       write (fo,'(a,i6)') 'wr_cfg: n-range exceeded, n_max = ', i
       return
    end if
    write (fo,'(/,30x)',advance='no')
    nsh = SIZE(occ)
    do i = 1, nsh
       fm = 1
       if (occ(i) > 9) fm = fm + 1
       if (nval(i) > 9) fm = fm + 1
       ad = 2
       if (i == nsh) ad = 1
       write (cocc,'(i2)') occ(i)
       if (occ(i) == 1) cocc = '  '
       write (fo,fmt=fa(fm),advance=TRIM(adv(ad))) TRIM(cocc)
    end do

    write (fo,'(3x,a,i5,8x)',advance='no') 'configuration ', n
    do i = 1, nsh
       lv = lval(i) + 1
       fm = 1
       if (nval(i) > 9) fm = fm + 1
       ad = 2
       if (i == nsh) ad = 1
       write (fo,fmt=f(fm),advance='no') nval(i)
       write (fo,'(a1)',advance=TRIM(adv(ad))) lspec(lv:lv)
       if (i /= nsh) then
          fmp = 1
          if (occ(i) > 9) fmp = fmp + 1
          write (fo,fmt=x(fmp),advance='no')
       end if
    end do
  end subroutine wr_cfg

  subroutine def_sc
! define orbital sequence numbers for supercore-orbitals
! core orbitals have been subtracted
    use rm_data, only: maxorb
    integer :: status, i, j, ni, li

    allocate (sc_orb(sc_inorb), stat=status)
    if (status /= 0) call alloc_error (status, 'def_sc', 'a')
    do i = 1, sc_inorb
       ni = sc_nj(i)
       li = sc_lj(i)
       do j = 1, maxorb
          if (ni == njcomp(j) .and. li == ljcomp(j)) then
             sc_orb(i) = j
             exit
          end if
       end do
    end do
  end subroutine def_sc

  subroutine def_ps
! define orbital sequence numbers for pseudo-orbitals
! core orbitals have been subtracted
    use rm_data, only: maxorb
    integer :: status, i, j, ni, li
    allocate (ps_orb(ps_inorb), stat=status)
    if (status /= 0) call alloc_error (status, 'def_ps', 'a')
    do i = 1, ps_inorb
       ni = ps_nj(i)
       li = ps_lj(i)
       do j = 1, maxorb
          if (ni == njcomp(j) .and. li == ljcomp(j)) then
             ps_orb(i) = j
             exit
          end if
       end do
    end do
  end subroutine def_ps

end module startup
