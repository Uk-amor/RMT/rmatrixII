module rm_data
! Input data common to all stages of R-matrix program
! public variables set for ANG
! Time-stamp: "2005-03-11 15:02:40 cjn"
  use precisn, only: wp
  use io_units, only: fo, fi
  use debug, only: set_debug_switches
  implicit none

  character(len=80), save :: coment ! run descriptor
  real(wp), save          :: ek2max  ! max energy for Buttle fit
  real(wp), save          :: ra      ! internal region radius in a.u.
  real(wp), save          :: bsto    ! Bloch parameter
  integer, save           :: nelc   ! # electrons in system
  integer, save           :: nz     ! nuclear charge
  integer, save           :: lrgle1, lrgle2 ! (N+1) orb am limits, w exchge
  integer, save           :: lrgld1, lrgld2 ! (N+1) orb am limits, no exchge

  integer, save           :: ltop
  integer, save           :: lstep
  integer, save           :: lrgp    ! Total (N+1) parity, -1 if both
  integer, save           :: lrgslo, lrgsup
  integer, save           :: maxor2
  integer, save           :: lrangr
  integer, save           :: lpmax
  integer, save           :: neprnt
  integer, save           :: nexp
  integer, save           :: lndef
  integer, save           :: inorb   ! # orbitals to be read in directly

  integer, save           :: maxe    ! maximum energy (Ryd) used in SS
  logical, save           :: buttle  ! Buttle correction switch
  integer, save           :: lfixn   ! L-value, nrang2 allowed to decrease
  integer, save           :: nset    ! # N-electron symmetries
  integer, save           :: maxorb  ! # orbitals
  integer, save           :: maxorb_dim  ! used for arrays, MAX(maxorb,1)
  integer, save           :: ncfg    ! # N-electron configurations
  integer, save           :: ndiag   ! =1 if tgt Hams to be diagonalized
  integer, save           :: icon    ! selects cfg input method
  integer, save           :: ibop    ! Slater/Superstructure orbitals
  integer, save           :: nrang2  ! # cont orbitals for each a.m.
  integer, save           :: lamax   ! max # multipole integrals
  integer, save           :: nix     ! # subintrvals in 0 < r <= RA
  integer, save           :: npot    ! choice of cont. orb potl
  integer, save           :: mpol    ! 0, no dipoles, 1 dipoles
  integer, save           :: nocore  ! = 0 find core orbs, else none
  integer, save           :: irbug(9) ! rad diagnostic switches
  integer, save           :: iabug(9) ! ang diagnostic switches
  integer, save           :: ihbug(9) ! ham diagnostic switches
  logical, save           :: restart  ! restart run flag
  integer, save           :: diag   ! diagonalization selection switch
  integer, save           :: prm    ! parallel diagonalization threshold
  logical, save           :: asym_ptlcfs ! output asymptotic potl coeffs
  logical, save           :: xdr    ! XDR PRMAT output switch
  logical, save           :: tgt_mmts ! output target moment file
  logical, save           :: farm   ! use opacity output files
  integer, save           :: ncfgp  ! # N+1 electron configurations
  integer, save           :: sbk    ! surcof block size
  logical, save           :: reorth ! switch to reorthoganalise orbitals

  private
  public rd_nmlst

! limit public variables to those used in ANG:
  public nelc, nz, coment, lrgle1, lrgle2, lrgld1, lrgld2,   &
       lrgslo, lrgsup, lpmax, nset, maxorb, maxorb_dim, inorb, ncfg,     &
       ndiag, lrgp, mpol, restart, icon, ncfgp, sbk, nocore

contains
  subroutine rd_nmlst
! read in generic RMATRXII namelist data

    integer           :: status

    namelist /input/ nelc, nz, coment, lrgle1, lrgle2,               &
         lrgld1, lrgld2, ltop, lstep, lrgslo, lrgsup, lrgp, lpmax,   &
         nset, maxorb, maxor2, inorb, lrangr, ncfg, ra, bsto, buttle,&
         ndiag, icon, ibop, nrang2, ek2max, lfixn, lamax, nix, ncfgp,&
         nexp, neprnt, npot, iabug, irbug, ihbug, nocore, mpol, sbk, &
         restart, diag, prm, xdr, asym_ptlcfs, tgt_mmts, maxe, farm, &
         reorth

! initialise data
    coment = repeat(' ',80)
    lrgslo = -1;  lrgsup = 0;  lrgp = -1
    ndiag = 1
    inorb = 0;    icon = 1;    ibop =0;    nix = -1
    nexp = 0;     neprnt = -1
    npot = 0;     lndef = 10
    nocore = 0

    mpol = 0;    nelc = -1
    nz = -1;      ra = - 1.0_wp;           bsto = 0.0_wp
    nrang2 = 0;   lamax = 4;
    ltop = -1;    lstep = 1
    ihbug = 0;    iabug = 0
    buttle = .true.
    restart = .false.
    diag = 4
    prm = -1
    xdr = .false.
    asym_ptlcfs = .false.
    tgt_mmts = .true.
    farm = .false.
    ncfgp = -1
    sbk = 1000
    reorth = .false.

!   rewind (fi)
    read (fi,input, iostat=status)
    if (status /= 0) then
       write (fo,'(a,i6)') 'rd_nmlst: namelist input read error = ', &
            status
       stop
    end if

    call set_debug_switches (iabug)
    if (ltop < lrgld1 .or. ltop > lrgld2) ltop = lrgld2

! check values of nelc,nz
    if (nelc < 0) then
       write (fo,'(a)') 'value of nelc invalid, must be >= 0.'
       stop
    end if
    if (nz <= 0) then
       write (fo,'(a)') 'value of nz invalid, must be > 0.'
       stop
    end if
    if (mpol < 0 .or. mpol > 1) then
       write (fo,'(a)') 'incorrect value of mpol'
       stop
    end if
    if (mpol /= 0) restart = .false. ! no restart for moment runs
  end subroutine rd_nmlst

end module rm_data
