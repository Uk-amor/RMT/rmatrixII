module shell_coupling
! Time-stamp: "2005-03-11 15:00:49 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use debug, only: bug2
  use startup, only: ljcomp, njcomp, ncorel, ncoret, njq_mx
  use cfg_pack, only: pck, zpck, incpckq, operator(==), assignment(=)
  use rm_data, only: maxorb
  implicit none

  private
  public equiv, bequiv, eq_estim
  public nrows, il, j, n

! quantum numbers of terms which can be formed from configurations l**q.
! only the first half of that part of the table, corresponding to a
! given  l, is included. e.g. d**7 forms the same terms as d**3
  integer, save :: nrows = 18
! il = number of terms that can be formed with each l**q case
  integer, save :: il(18) = (/1, 1, 1, 3, 3, 1, 5, 8,16,16, 1, 1, 1,&
       1, 1, 1, 1, 1/)
! j = location of q.no. pointers, n, for each l**q case
  integer, save :: j(18) = (/  0,  3,  6,  9, 18, 27, 30, 45, 69,&
       117,165,168,171,174,177,180,183,186/)
! n = quantum number pointers; seniority, 2*L+1, 2*S+1
! 2S; 1S; 2P; 1S, 1D, 3P;| 2P, 2D, 4S; 2D; 1S, 1D,| 1G, 3P, 3F; 2D
! 2P, 2D,| 2F, 2G, 2H, 4P, 4F;
  integer, save :: n(189) = (/ &
        1, 1, 2,  0, 1, 1,  1, 3, 2,  0, 1, 1,  2, 5, 1,  2, 3, 3, &
        1, 3, 2,  3, 5, 2,  3, 1, 4,  1, 5, 2,  0, 1, 1,  2, 5, 1, &
        2, 9, 1,  2, 3, 3,  2, 7, 3,  1, 5, 2,  3, 3, 2,  3, 5, 2, &
        3, 7, 2,  3, 9, 2,  3,11, 2,  3, 3, 4,  3, 7, 4,  0, 1, 1, &
        2, 5, 1,  2, 9, 1,  2, 3, 3,  2, 7, 3,  4, 1, 1,  4, 5, 1, &
        4, 7, 1,  4, 9, 1,  4,13, 1,  4, 3, 3,  4, 5, 3,  4, 7, 3, &
        4, 9, 3,  4,11, 3 , 4, 5, 5,  1, 5, 2,  3, 3, 2,  3, 5, 2, &
        3, 7, 2,  3, 9, 2,  3,11, 2,  3, 3, 4,  3, 7, 4,  5, 1, 2, &
        5, 5, 2,  5, 7, 2,  5, 9, 2,  5,13, 2,  5, 5, 4,  5, 9, 4, &
        5, 1, 6,  1, 7, 2,  1, 9, 2,  1,11, 2,  1,13, 2,  1,15, 2, &
        1,17, 2,  1,19, 2,  1,21, 2/)
! arrays i,j,n correspond to the arrays itab,jtab,ntab

contains

  subroutine equiv (n1, n2, noccsh, nocorb, nelcsh, nq1, nq2, nqpac,&
       nnq, nqn1, iparty, ncoumx, lsset, lvmax)
! form equivalent configurations in which shells outside the core
! are replaced by a shell of the same l but the lowest principal
! quantum number. In general there will be more than one configuration
! with the same equivalent configuration. Count the number of
! equivalent configurations nq2 and pack them into nqpac.
! Form link between the true and the equivalent configurations in
! nnq and vice versa in nqn1(inq) = first occurrence of this type
! of congfiguration in the list of true configurations.
    use ls_states, only: lvals, svals
    use startup, only: wr_cfg
    integer, intent(in)   :: n1          ! initial cfg seq - 1
    integer, intent(in)   :: n2          ! final cfg seq
    integer, intent(in)   :: nq1         ! initial equiv cfg - 1
    integer, intent(out)  :: nq2         ! # equivalent cfgs
    integer, intent(in)   :: noccsh(:)   ! # occ shells in cfg
    integer, intent(in)   :: nocorb(:,:) ! orb seqs in cfg
    integer, intent(in)   :: nelcsh(:,:) ! shell occupancies in cfg
    integer, intent(inout):: nnq(:)    ! true -> equivalent cfgs
    type(pck), intent(inout):: nqpac(:)  ! packed equivalent cfgs
    integer, intent(out)  :: nqn1(:)   ! equivalent -> first true cfg
    integer, intent(out)  :: iparty(:) ! parity of equiv cfg
    integer, intent(out)  :: ncoumx(:) ! # couplings for equiv cfg
    integer, intent(in)   :: lsset     ! set of LS q.#s
    integer, intent(inout):: lvmax
    integer, pointer      :: lval(:,:), ispval(:,:)
    integer               :: i, n, in, ipar, lj, orbq_last
    integer               :: inq, noc, ioc, iorb, nel
    integer, pointer      :: njqc(:)
    type(pck)             :: npq

    lval => lvals(lsset)
    ispval => svals(lsset)
    nq2 = nq1
    njqc => ncoret

    in_loop: do in = n1+1, n2
       njqc = ncorel
       call zpck(npq)
       ipar = 0
       noc = noccsh(in)
       orbq_last = 0
       do ioc = 1, noc
          iorb = nocorb(ioc,in)
          nel = nelcsh(ioc,in)
          lj = ljcomp(iorb)
          call incpckq (npq, orbq_last, njqc(lj+1), njq_mx(lj+1), lj, nel)
          ipar = ipar + lj * nel
       end do

! if this is a new configuration store it.  form link between
! equivalent and true. Calculate max. # couplings for this configuration
! and the top l,s of each coupling.
       do inq = 1, nq2
          if (npq == nqpac(inq)) then
             nnq(in) = inq
! additional parity check:
             if (MOD(ipar,2) /= iparty(inq)) then
                write (fo,'(a,2i6)') 'equiv: Parity error, in, inq = ',&
                     in, inq
                stop
             end if
             cycle in_loop
          end if
       end do
       nq2 = nq2 + 1
       if (nq2 > SIZE(nqpac)) then
          write (fo,'(a)') 'equiv: nq2 exceeds array sizes'
          write (fo,'(a,i4)') 'present size =  ', SIZE(nqpac)
          stop
       end if
       nqpac(nq2) = npq
       iparty(nq2) = MOD(ipar,2)
       nnq(in) = nq2
       nqn1(nq2) = in
! empty configuration is singlet s
       if (noc == 0) then
          lval(1,nq2) = 0
          ispval(1,nq2) = 1
          ncoumx(nq2) = 1
       else
          call conqn (noc, nocorb(:,in), nelcsh(:,in), ncoumx(nq2),&
               nq2, lsset, lvmax)
          if (.NOT.associated(lval)) then ! ls_vals extended
             lval => lvals(lsset)
             ispval => svals(lsset)
          end if
       end if
    end do in_loop

    if (bug2 == 1) then
       do inq = nq1+1, nq2
          i = nqn1(inq)
          n = noccsh(i)
          call wr_cfg (i, nelcsh(1:n,i), njcomp(nocorb(1:n,i)), &
               ljcomp(nocorb(1:n,i)))
       end do
    end if
  end subroutine equiv

  subroutine conqn (jact, mact, mnt, ncomx, vc, lsset, lvmax)
! determine the quantum numbers for each shell
! extended to cope with 2 electrons in l>2 shells.
    use startup, only: ljcomp
    integer, intent(in)   :: jact     ! # shells
    integer, intent(in)   :: mact(12) ! shell l-index
    integer, intent(in)   :: mnt(12)  ! # electrons in each shell
    integer, intent(out)  :: ncomx    ! # couplings
    integer, intent(in)   :: vc       ! q.# column
    integer, intent(in)   :: lsset    ! q.# set
    integer, intent(inout):: lvmax    ! max spin value ??
    integer, save         :: ls(11) = (/1,3,6,11,12,13,14,15,16,17,18/)
    integer               :: ifsh(12), mxs(12), mcs(12), ni(12)
    integer               :: j1qn(3,12)
    integer               :: i, nn, ll, ll1, lk1, m, k, ki, mi

    ncomx = 0
    shells: do i = 1, jact
       nn = mact(i)          ! shell index
       ll = ljcomp(nn)       ! l of shell
       ll1 = ll + ll + 1
       lk1 = ll + 1
       m = mnt(i)            ! # electrons in shell
       k = m
       if (m > ll1) k = 2 * ll1 - m    ! effective # electrons in shell
       if (k == 0) then      ! empty or filled shell
          ifsh(i) = 2
       else
          if (ll >= 3 .and. k == 2) then
             mxs(i) = ll1
             mcs(i) = -1
             cycle
          end if
          ifsh(i) = ls(lk1) + k - 1
       end if
       ki = ifsh(i)      ! pointer for given l**q
       mxs(i) = il(ki)   ! # terms for l**q
       mcs(i) = j(ki)    ! pointer to q# array
    end do shells

! loop over the allowed quantum numbers for each shell
    i = 0
    shell_loop: do
       i = i + 1
       ni(i) = 0      ! term indices for each shell
       term_loop: do    ! loop over shell terms
          ni(i) = ni(i) + 1
          if (mcs(i) == -1) then ! speical case: q#s not in table
             j1qn(1,i) = 2
             if (ni(i) == 1) j1qn(1,i) = 0
             j1qn(2,i) = 2 * ni(i) - 1
             j1qn(3,i) = 1
             if (MOD(ni(i),2) == 0) j1qn(3,i) = 3
          else
             mi = mcs(i) + (ni(i) - 1) * 3 ! pointer to q#s
             j1qn(:,i) = n(mi+1:mi+3)
          end if
          if (i < jact) then ! pick up next shell
             cycle shell_loop
          else if (i > jact) then
             return
          end if
! process selected terms:
          call consh (jact, j1qn, ncomx, vc, lsset, lvmax)
          find_term: do  ! find term index to increase
             if (ni(i) < mxs(i)) cycle term_loop ! more terms in shell
             i = i - 1                           ! next lowest shell
             if (i <= 0) exit shell_loop   ! all terms processed - exit
          end do find_term
       end do term_loop
    end do shell_loop
  end subroutine conqn

  subroutine bequiv (n1, n2, noccsh, nocorb, nelcsh, nq1, nq2, nqpac,&
       nnq, nqn1, iparty, ncoumx, neqsen, nshqn, lsset, lvmax)
! form equivalent configurations in which shells outside the core
! are replaced by a shell of the same l but the lowest principal q.no.
! in general there will be more than one configuration with the same
! equivalent configuration.
! count the number of equivalent configs nq2 and pack them
! into nqpac. form link between the true and equivalent configs in
! nnq and vice versa in nqn1(inq) = first occurrence of this type of
! congfig in the list of true config.

! bequiv differs from equiv in that it passes the arrays neqsen and
! nshqn from subroutine bconqn which it calls in place of conqn
    use startup, only: wr_cfg
    integer, intent(in)   :: n1
    integer, intent(in)   :: n2
    integer, intent(in)   :: nq1
    integer, intent(out)  :: nq2
    integer, intent(in)   :: noccsh(:)
    integer, intent(in)   :: nocorb(:,:)
    integer, intent(in)   :: nelcsh(:,:)
    integer, intent(inout):: nnq(:)
    type(pck), intent(inout):: nqpac(:)
    integer, intent(out)  :: nqn1(:)
    integer, intent(out)  :: iparty(:)
    integer, intent(inout):: ncoumx(:)
    integer, intent(inout):: neqsen(:,:)
    integer, intent(inout):: nshqn(:,:)
    integer, intent(in)   :: lsset       ! q.# set
    integer, intent(inout):: lvmax
    integer               :: in, noc, ioc, nel, iorb, orbq_last
    integer               :: lj, inq, i, n, ipar
    integer, pointer      :: njqc(:)
    type(pck)             :: npq

    nq2 = nq1
    njqc => ncoret
    in_loop: do in = n1+1, n2
       njqc = ncorel
       call zpck(npq)
       ipar = 0
       noc = noccsh(in)
       orbq_last = 0
       ioc_loop: do ioc = 1, noc
          iorb = nocorb(ioc,in)
          nel = nelcsh(ioc,in)
          lj = ljcomp(iorb)
          call incpckq (npq, orbq_last, njqc(lj+1), njq_mx(lj+1), lj, nel)
          ipar = ipar + lj * nel
       end do ioc_loop
! if this is a new configuration store it.  form link between
! equivalent and true.
! calculate max. number of couplings for this config and the top l,s
! of each coupling.
       do inq = 1, nq2
          if (npq == nqpac(inq)) then
             nnq(in) = inq
! additional parity check:
             if (MOD(ipar,2) /= iparty(inq)) then
                write (fo,'(a,2i6)') 'bequiv: Parity error, in, &
                     &inq = ', in, inq
                stop
             end if
             cycle in_loop
          end if
       end do
       nq2 = nq2 + 1
       if (nq2 > SIZE(nqpac)) then
          write (fo,'(a)') 'bequiv: npq exceeds array sizes'
          write (fo,'(a,i4)') 'current size = ', SIZE(nqpac)
          stop
       end if
       nqpac(nq2) = npq
       iparty(nq2 ) = MOD(ipar,2)
       nnq(in) = nq2
       nqn1(nq2) = in
       call bconqn (noc, nocorb(:,in), nelcsh(:,in), ncoumx(nq2), &
            neqsen(:,nq2), nshqn(:,nq2), nq2, lsset, lvmax)
    end do in_loop

    if (bug2 == 1) then
       do inq = nq1+1, nq2
          i = nqn1(inq)
          n = noccsh(i)
          call wr_cfg (i, nelcsh(1:n,i), njcomp(nocorb(1:n,i)), &
               ljcomp(nocorb(1:n,i)))
       end do
    end if
  end subroutine bequiv

  subroutine bconqn (jact, mact, mnt, ncomx, neqsen, nshqn, vc, lsset,&
       lvmax)
! determine the quantum numbers for each shell
! extended to cope with 2 electrons in l>2 shells.

! bconqn differs from conqn in that it packs the index to
! the shell quantum numbers for each coupling and stores it
! in array nshqn.  it also stores in array neqsen(icou) the
! coupling index of a coupling which is identical in every
! respect except that the seniority of one shell is different.
! if there are more than one such coupling (eg doublet d in
! a d shell with 5 electrons) the first occurrence is stored
! and if there is no such coupling it contains its own index.
! in essence neqsen(icou)=kcou stores 'equivalent' couplings
! for which offdiagonal terms in dd or ff can occur in the
! bound bound matrix elements.

! bconqn allows the possibility of 2 shells in a configuration
! with l>1 and more than  2 electrons in each.
! switch noffsw ensures that a coupling is considered
! equivalent when only one shell has ls the same but seniority different
    use startup, only: ljcomp, multco
    use error_prt, only: alloc_error
    integer, intent(in)     :: jact       ! # shells
    integer, intent(in)     :: mact(12)   ! orbital index
    integer, intent(in)     :: mnt(12)    ! # electrons in shell
    integer, intent(out)    :: ncomx      ! running total # cpgs
    integer, intent(out)    :: neqsen(:)  ! equivalent coupling #
    integer, intent(out)    :: nshqn(:)   ! packed term indices for cpg
    integer, intent(in)     :: vc
    integer, intent(in)     :: lsset
    integer, intent(inout)  :: lvmax
    integer, allocatable    :: kcou(:,:), noff(:,:)
    integer                 :: ifsh(12), mxs(12), mcs(12), ni(12)
    integer                 :: j1qn(3,12)
! ls locates q.# data for each l:
    integer, save :: ls(11) = (/1,3,6,11,12,13,14,15,16,17,18/)
! isen = seniority flags for 3, 4, 5 electrons in d-shell:
! non-zero values give seq # of equivalent coupling (diff seniority)
    integer, save :: isen(16,3) = reshape(shape = (/16,3/), &
         source = (/0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0, &
         0,0,0,0,0,1,2,0,3,0,4,0,5,0,0,0, &
         0,0,1,0,0,0,0,0,0,1,4,5,0,0,0,0/))
    integer   :: nqcomx, nqcmx1, i, ll, nn, lk1, m, k, ki, km2, llm1
    integer   :: nshq, ic, iqn, noffsw, ll1, icou, iqn1, ncomx1
    integer   :: j1q1, j1q2, j1q3, mi, iq2, status

    allocate (kcou(16,maxorb), noff(16,maxorb), stat=status)
    if (status /= 0) call alloc_error (status, 'bconqn', 'a')
    ncomx = 0
    nqcomx = 0
    nqcmx1 = 0
    noffsw = 0
    shells: do i = 1, jact
       nn = mact(i)        ! index of shell
       ll = ljcomp(nn)     ! a.m. of shell
       ll1 = ll + ll + 1
       lk1 = ll + 1
       m = mnt(i)          ! # electrons in shell
       k = m
       if (m > ll1) k = 2 * ll1 - m   ! # holes (shell > half full)
       if (k == 0) then     ! empty shell
          ifsh(i) = 2                    ! config location
          ki = ifsh(i)
          mxs(i) = il(ki)                ! # terms
          mcs(i) = j(ki)                 ! location of q.#s
       else
          if (ll >= 3 .and. k == 2) then ! 2 electrons, f or higher shell
             mxs(i) = ll1
             mcs(i) = -1
          else                           ! normal case
             ifsh(i) = ls(lk1) + k - 1   ! locate shell config (l**q)
             ki = ifsh(i)
             mxs(i) = il(ki)             ! # terms
             mcs(i) = j(ki)              ! location of q.#s for terms
          end if
       end if
       if (ll < 2 .or. k < 3) then  ! l = s, p or 0,1,2 electrons/holes
          noff(1:mxs(i),i) = 0      ! no seniority off-diag els
       else
!         llm1 = ll - 1
          km2 = k - 2               ! 3,4,5 electrons in shell
          do iq2 = 1, mxs(i)        ! insert seniority flags 
!            noff(iq2,i) = isen(iq2,km2,llm1)  ! may need to generalize
             noff(iq2,i) = isen(iq2,km2) ! flag equivalent cplg
          end do
       end if
    end do shells

! loop over the allowed quantum numbers for each shell
! systematically select terms for each of the jact shells
! for each selection, ni defines the term for each shell
    if (jact > 0) then 
    i = 0
    i1_loop: do
       i = i + 1
       ni(i) = 0
       ni_loop: do
          ni(i) = ni(i) + 1
          iqn = ni(i)
! at this point ni contains the jact selected terms

          kcou(iqn,i) = ncomx
          iqn1 = noff(iqn,i)
          if (iqn1 /= 0) then  ! this term has an LS equivalent term
             noffsw = noffsw + 1   ! count # equivalent couplings
             if (noffsw == 1) then
                nqcomx = kcou(iqn1,i)    ! -> equivalent coupling
             else
                nqcmx1 = nqcomx
                nqcomx = ncomx           ! actual coupling
             end if
          end if

          if (mcs(i) == -1) then    ! 2 electrons in f or higher shell
             j1q1 = 2
             if (ni(i) == 1) j1q1 = 0
             j1q2 = 2 * ni(i) - 1
             j1q3 = 1
             if (MOD(ni(i),2) == 0) j1q3 = 3
          else                      ! s, p or d shell
             mi = mcs(i) + (ni(i) - 1) * 3   ! locate term q.#s
             j1q1 = n(mi+1)         ! seniority
             j1q2 = n(mi+2)         ! 2L+1
             j1q3 = n(mi+3)         ! 2S+1
          end if
          j1qn(:,i) = (/j1q1, j1q2, j1q3/) ! shell term q.#s

          if (i < jact) cycle i1_loop  ! more shells to process

          ncomx1 = ncomx + 1        ! coupling index

          call consh (jact, j1qn, ncomx, vc, lsset, lvmax)

! pack the term sequence #s of each shell
          nshq = SUM(ni(1:jact) * multco(1:jact))
          ic = 0
          do icou = ncomx1, ncomx  ! couplings returned by consh
             ic = ic + 1
             nshqn(icou) = nshq    ! packed term seqs for shells
             neqsen(icou) = nqcomx + ic ! equivalent coupling #
          end do
          nqcomx = nqcomx + ic
          nqcmx1 = nqcmx1 + ic

          iqn_loop: do     ! update term selection

             if (noff(iqn,i) /= 0) then  ! has LS equivalent cplg
                noffsw = noffsw - 1
                if (noffsw == 1) then
                   nqcomx = nqcmx1
                else
                   nqcomx = ncomx
                end if
             end if

             if (iqn < mxs(i)) cycle ni_loop ! mxs = # terms for shell
             i = i - 1
             if (i == 0) exit i1_loop   ! all possible combinations done
             iqn = ni(i)
          end do iqn_loop
       end do ni_loop
    end do i1_loop
    end if

    deallocate (kcou, noff, stat=status)
    if (status /= 0) call alloc_error (status, 'bconqn', 'd')
  end subroutine bconqn

  subroutine consh (jact, j1qn, ncomx, vc, lsset, lvmax)
! determine the coupling between the shells
    use coupling_limits, only: maxln, minln, maxsn, minsn
    use ls_states, only: lvec, svec, extend_lsvals
    integer, intent(in)    :: jact       ! # shells
    integer, intent(inout) :: ncomx      ! total # couplings
    integer, intent(in)    :: lsset      ! q. # set
    integer, intent(inout) :: lvmax      ! max L
    integer, intent(in)    :: vc         ! q.# column
    integer, intent(in)    :: j1qn(:,:)  ! shell term q#s
    integer, pointer       :: lvaln(:), ispvl(:)
    integer :: j1qn1(SIZE(j1qn,dim=1),SIZE(j1qn,dim=2))
    integer :: li(12), lll(12), llh(12), lsp(12), lsl(12), lsh(12)
    integer :: i, ll0, ls0, lcfgn, ispinn, jactm1, lsi, lli, ncc

    lvaln => lvec(vc, lsset)
    ispvl => svec(vc, lsset)
    jactm1 = jact - 1               ! # couplings
    if (jactm1 <= 0) then
       lcfgn = (j1qn(2,jact) - 1) / 2
       ispinn = j1qn(3,jact)
       if (lcfgn <= maxln .and. lcfgn >= minln .and. ispinn <= maxsn &
            .and. ispinn >= minsn) then
          ncomx = ncomx + 1
          lvmax = MAX(lvmax, lcfgn)
          lvaln(ncomx) = lcfgn
          ispvl(ncomx) = ispinn
       end if
       return
    end if

    j1qn1(1,1:jactm1) = 0
    ls0 = j1qn(3,1) - 1          ! 2S of first term
    ll0 = (j1qn(2,1) - 1) / 2    ! L of first term
    lll(1) = ABS(ll0 - (j1qn(2,2) - 1) / 2) ! 1st cplg, min L
    llh(1) = ll0 + (j1qn(2,2) - 1) / 2 + 1  ! 1st cplg, max L + 1
    lsl(1) = ABS(ls0 - j1qn(3,2) + 1) - 1   ! 1st cplg, 2*min S -1
    lsh(1) = ls0 + j1qn(3,2)                ! 1st cplg, 2*max S +1

! loop over all possible couplings between the shells
    i = 0
    shell_loop: do
       i = i + 1
       li(i) = lll(i)    ! set min L for first cplg
       orb_am_loop: do
          li(i) = li(i) + 1         ! L + 1
          lli = li(i) - 1           ! L
          j1qn1(2,i) = 2 * lli + 1  ! set L
          if (i < jactm1) then      ! limits for next cplg
             lll(i+1) = ABS(lli - (j1qn(2,i+2) - 1) / 2)
             llh(i+1) = lli + (j1qn(2,i+2) - 1) / 2 + 1
          end if

          lsp(i) = lsl(i)  ! set min S for coupling
          spin_loop: do
             lsp(i) = lsp(i) + 2
             lsi = lsp(i) - 1
             j1qn1(3,i) = lsi + 1    ! set S
             if (i < jactm1) then
                lsl(i+1) = ABS(lsi - j1qn(3,i+2) + 1) - 1
                lsh(i+1) = lsi + j1qn(3,i+2)
                cycle shell_loop
             else if (i == jactm1) then ! state complete
                lcfgn = (j1qn1(2,jactm1) - 1) / 2 ! treetop L
                ispinn = j1qn1(3,jactm1)          ! treetop S
                if (lcfgn <= maxln .and. lcfgn >= minln .and. &
                     ispinn <= maxsn .and. ispinn >= minsn) then ! save
                   ncomx = ncomx + 1       ! state count
                   if (ncomx > SIZE(lvaln)) then
                      ncc = SIZE(lvaln)
                      call extend_lsvals (dim=1, q=lsset)
                      lvaln => lvec(vc,lsset)
                      ispvl => svec(vc,lsset)
                      write (fo,'(a)') 'consh: ncc extension'
                   end if
                   lvmax = MAX(lvmax, lcfgn)  ! max L at treetop
                   lvaln(ncomx) = lcfgn       ! L at treetop
                   ispvl(ncomx) = ispinn      ! S at treetop
                end if
             else
                return
             end if
             j_loop: do
                if (lsp(i) < lsh(i)) cycle spin_loop
                if (li(i) < llh(i)) cycle orb_am_loop
                i = i - 1
                if (i <= 0) exit shell_loop
             end do j_loop

          end do spin_loop
       end do orb_am_loop
    end do shell_loop
  end subroutine consh

  subroutine eq_estim (n1, n2, noccsh, nocorb, nelcsh, nq1, nq2, nqpac,&
       ncupl)
! form equivalent configurations in which shells outside the core
! are replaced by a shell of the same l but the lowest principal
! quantum number. In general there will be more than one configuration
! with the same equivalent configuration. Count the number of
! equivalent configurations nq2 and pack them into nqpac.
    integer, intent(in)   :: n1          ! initial cfg seq - 1
    integer, intent(in)   :: n2          ! final cfg seq
    integer, intent(in)   :: nq1         ! initial equiv cfg - 1
    integer, intent(out)  :: nq2         ! # equivalent cfgs
    integer, intent(in)   :: noccsh(:)   ! # occ shells in cfg
    integer, intent(in)   :: nocorb(:,:) ! orb seqs in cfg
    integer, intent(in)   :: nelcsh(:,:) ! shell occupancies in cfg
    type(pck), intent(inout):: nqpac(:)  ! packed equivalent cfgs
    integer, intent(out)  :: ncupl     ! max # couplings / cfg
    integer               :: in, lj
    integer               :: inq, noc, ioc, iorb, nel
    integer               :: ncomx, orbq_last
    integer, pointer      :: njqc(:)
    type(pck)             :: npq

    nq2 = nq1
    njqc => ncoret

    ncomx = 0
    ncupl = 0
    in_loop: do in = n1+1, n2
       njqc = ncorel
       call zpck(npq)
       noc = noccsh(in)
       orbq_last = 0
       do ioc = 1, noc
          iorb = nocorb(ioc,in)
          nel = nelcsh(ioc,in)
          lj = ljcomp(iorb)
          call incpckq (npq, orbq_last, njqc(lj+1), njq_mx(lj+1), lj, nel)
       end do

! if this is a new configuration store it.  form link between
! equivalent and true. Calculate max. # couplings for this configuration
! and the top l,s of each coupling.
       do inq = 1, nq2
          if (npq == nqpac(inq)) cycle in_loop
       end do
       nq2 = nq2 + 1
       if (nq2 > SIZE(nqpac)) then
          write (fo,'(a)') 'equiv: nq2 exceeds array sizes'
          write (fo,'(a,i4)') 'present size =  ', SIZE(nqpac)
          stop
       end if
       nqpac(nq2) = npq
! empty configuration is singlet s
       if (noc == 0) then
          ncomx = 1
       else
          call conqn_cnt (noc, nocorb(:,in), nelcsh(:,in), ncomx)
       end if
       ncupl = MAX(ncupl, ncomx)
    end do in_loop
    write (fo,'(/,a,i7)') 'eq_estim: max # couplings / cfg = ', ncupl
    write (fo,'(a,i7,/)') 'eq_estim: max # equiv cfgs      = ', nq2
  end subroutine eq_estim

  subroutine conqn_cnt (jact, mact, mnt, ncomx)
! Determine max # couplings per configuration, ncc
! Dry run of bconqn
    use startup, only: ljcomp
    integer, intent(in)     :: jact     ! # active shells
    integer, intent(in)     :: mact(:)  ! orb seq for shells
    integer, intent(in)     :: mnt(:)   ! occupancy of shells
    integer, intent(out)    :: ncomx    ! # couplings
! determine the quantum numbers for each shell
! extended to cope with 2 electrons in l>2 shells.
    integer   :: ifsh(12), mxs(12), mcs(12), ni(12)
    integer   :: j1qn(3,12)
    integer, save :: ls(11) = (/1,3,6,11,12,13,14,15,16,17,18/)
    integer   :: i, ll, nn, lk1, m, k, ki
    integer   :: iqn, ll1, j1q1, j1q2, j1q3, mi

    ncomx = 0
    i_loop: do i = 1, jact
       nn = mact(i)
       ll = ljcomp(nn)
       ll1 = ll + ll + 1
       lk1 = ll + 1
       m = mnt(i)
       k = m
       if (m > ll1) k = 2 * ll1 - m
       if (k == 0) then
          ifsh(i) = 2
          ki = ifsh(i)
          mxs(i) = il(ki)
          mcs(i) = j(ki)
       else
          if (ll >= 3 .and. k == 2) then
             mxs(i) = ll1
             mcs(i) = -1
          else
             ifsh(i) = ls(lk1) + k - 1
             ki = ifsh(i)
             mxs(i) = il(ki)
             mcs(i) = j(ki)
          end if
       end if
    end do i_loop

! loop over the allowed quantum numbers for each shell
    i = 0
    i1_loop: do
       i = i + 1
       ni(i) = 0
       ni_loop: do
          ni(i) = ni(i) + 1
          iqn =ni(i)
          if (mcs(i) == -1) then
             j1q1 = 2
             if (ni(i) == 1) j1q1 = 0
             j1q2 = 2 * ni(i) - 1
             j1q3 = 1
             if (MOD(ni(i),2) == 0) j1q3 = 3
          else
             mi = mcs(i) + (ni(i) - 1) * 3
             j1q1 = n(mi+1)
             j1q2 = n(mi+2)
             j1q3 = n(mi+3)
          end if
          j1qn(:,i) = (/j1q1, j1q2, j1q3/)
          if (i < jact) then
             cycle i1_loop
          else if (i > jact) then
             return
          end if
          call conshp (jact, j1qn, ncomx)

          iqn_loop: do
             if (iqn < mxs(i)) cycle ni_loop
             i = i - 1
             if (i == 0) exit i1_loop
             iqn = ni(i)
          end do iqn_loop
       end do ni_loop
    end do i1_loop
  end subroutine conqn_cnt

  subroutine conshp (jact, j1qn, ncomx)
! count the number of couplings between the shells
! dry run version of consh
    use coupling_limits, only: maxln, minln, maxsn, minsn
    integer, intent(in)    :: jact       ! # shells
    integer, intent(inout) :: ncomx      ! total # couplings
    integer, intent(in)    :: j1qn(:,:)  ! shell term q#s
    integer :: j1qn1(SIZE(j1qn,dim=1),SIZE(j1qn,dim=2))
    integer :: li(12), lll(12), llh(12), lsp(12), lsl(12), lsh(12)
    integer :: i, ll0, ls0, lcfgn, ispinn, jactm1, lsi, lli

    jactm1 = jact - 1               ! # couplings
    if (jactm1 <= 0) then
       lcfgn = (j1qn(2,jact) - 1) / 2
       ispinn = j1qn(3,jact)
       if (lcfgn <= maxln .and. lcfgn >= minln .and. ispinn <= maxsn &
            .and. ispinn >= minsn) then
          ncomx = ncomx + 1
       end if
       return
    end if

    j1qn1(1,1:jactm1) = 0
    ls0 = j1qn(3,1) - 1          ! 2S of first term
    ll0 = (j1qn(2,1) - 1) / 2    ! L of first term
    lll(1) = ABS(ll0 - (j1qn(2,2) - 1) / 2) ! 1st cplg, min L
    llh(1) = ll0 + (j1qn(2,2) - 1) / 2 + 1  ! 1st cplg, max L + 1
    lsl(1) = ABS(ls0 - j1qn(3,2) + 1) - 1   ! 1st cplg, 2*min S -1
    lsh(1) = ls0 + j1qn(3,2)                ! 1st cplg, 2*max S +1

! loop over all possible couplings between the shells
    i = 0
    shell_loop: do
       i = i + 1
       li(i) = lll(i)    ! set min L for first cplg
       orb_am_loop: do
          li(i) = li(i) + 1         ! L + 1
          lli = li(i) - 1           ! L
          j1qn1(2,i) = 2 * lli + 1  ! set L
          if (i < jactm1) then      ! limits for next cplg
             lll(i+1) = ABS(lli - (j1qn(2,i+2) - 1) / 2)
             llh(i+1) = lli + (j1qn(2,i+2) - 1) / 2 + 1
          end if

          lsp(i) = lsl(i)  ! set min S for coupling
          spin_loop: do
             lsp(i) = lsp(i) + 2
             lsi = lsp(i) - 1
             j1qn1(3,i) = lsi + 1    ! set S
             if (i < jactm1) then
                lsl(i+1) = ABS(lsi - j1qn(3,i+2) + 1) - 1
                lsh(i+1) = lsi + j1qn(3,i+2)
                cycle shell_loop
             else if (i == jactm1) then ! state complete
                lcfgn = (j1qn1(2,jactm1) - 1) / 2 ! treetop L
                ispinn = j1qn1(3,jactm1)          ! treetop S
                if (lcfgn <= maxln .and. lcfgn >= minln .and. &
                     ispinn <= maxsn .and. ispinn >= minsn) then ! save
                   ncomx = ncomx + 1       ! state count
                end if
             else
                return
             end if
             j_loop: do
                if (lsp(i) < lsh(i)) cycle spin_loop
                if (li(i) < llh(i)) cycle orb_am_loop
                i = i - 1
                if (i <= 0) exit shell_loop
             end do j_loop

          end do spin_loop
       end do orb_am_loop
    end do shell_loop
  end subroutine conshp

end module shell_coupling
