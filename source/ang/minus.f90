module plus_minus
! Time-stamp: "2005-03-11 15:07:53 cjn"
  use precisn, only: wp
  use io_units, only: fo
  use rm_data, only: maxorb, lrgp
  use startup, only: ljcomp, njcomp, nelmin, nelmax, wr_cfg
  use debug, only: bug2
  use cfg_pack, only: pck, mspack, adpack, operator(==), assignment(=),&
       zpck
 implicit none

  integer, save  :: psize            ! size of pack array
  character(len=9), save :: sname    ! subroutine name

  private
  public minus, plus
  public est_minus, est_plus

contains

  subroutine minus (np1, np2, npocsh, npocob, npelsh, nppack, &
       n1, n2, noccsh, nocorb, nelcsh, npack, nnpfig)
! form n electron configurations by removing 1 electron from
! each occupied cortex shell of each n+1 configuration. form
! linking array nnpfig(kshell,inp).  store any n electron
! configurations formed which are not already stored.
! avoid depleting a shell by more than 1 less than the minimum
! occupation of that shell in the target.
    integer, intent(in)    :: np1 ! # n+1 elec cfgs already processed
    integer, intent(in)    :: np2 ! total # n+1 electron configurations
    type(pck), intent(in)  :: nppack(:) ! packed n+1 configurations
    integer, intent(in)    :: n1 ! total # n elctron configs stored
    integer, intent(out)   :: n2 ! new total number of n elctron configs
    integer, intent(out)   :: nnpfig(:,:) ! linking array
! between the n+1 and the n electron configurations.
    integer, intent(out)   :: noccsh(:)
    integer, intent(inout) :: nocorb(:,:)
    integer, intent(inout) :: nelcsh(:,:)
    type(pck), intent(inout) :: npack(:)
    integer, intent(in)    :: npocsh(:), npocob(:,:)
    integer, intent(in)    :: npelsh(:,:)
    integer :: inp, npoc, korb, nel, i, iocc1, iocc2
    integer :: n, kshell, iocc3
    type(pck) :: nppck, npck

    psize = SIZE(npack)
    sname = 'minus'
    n2 = n1
    call zpck(npck)
    inp_loop: do inp = np1+1, np2
       nppck = nppack(inp)
       npoc = npocsh(inp)
       kshell_loop: do kshell = 1, npoc
          korb = npocob(kshell,inp)
          nel = npelsh(kshell,inp)
          if (nel >= nelmin(korb)) then
             call mspack (npck, nppck, korb)
! form link between n+1 and n
             do i = 1, n2
                if (npck == npack(i)) then
                   nnpfig(kshell,inp) = i
                   cycle kshell_loop
                end if
             end do
             n2 = n2 + 1
             call overwrite (n2)
             nnpfig(kshell,inp) = n2
             npack(n2) = npck

! store this configuration
             if (nel > 1) then
                do iocc1 = 1, npoc
                   nelcsh(iocc1,n2) = npelsh(iocc1,inp)
                   nocorb(iocc1,n2) = npocob(iocc1,inp)
                end do
                nelcsh(kshell,n2) = nel - 1
                noccsh(n2) = npoc
             else
                do iocc2 = 1, kshell-1
                   nelcsh(iocc2,n2) = npelsh(iocc2,inp)
                   nocorb(iocc2,n2) = npocob(iocc2,inp)
                end do
                do iocc3 = kshell+1, npoc
                   nelcsh(iocc3-1,n2) = npelsh(iocc3,inp)
                   nocorb(iocc3-1,n2) = npocob(iocc3,inp)
                end do
                noccsh(n2) = npoc - 1
             end if
          else
            nnpfig(kshell,inp) = 0
          end if
       end do kshell_loop
    end do inp_loop

    if (bug2 == 1) then
       do i = n1+1, n2
          n = noccsh(i)
          call wr_cfg (i, nelcsh(1:n,i), njcomp(nocorb(1:n,i)), &
               ljcomp(nocorb(1:n,i)))
       end do
    end if
  end subroutine minus

  subroutine plus (nefig, noccsh, nocorb, nelcsh, npack, npefig, &
       npocsh, npocob, npelsh, nppack, nnq, iparty)
! construct n+1 configurations
! for each n electron configuration add 1 electron into each shell if
! it is not already full and into each higher shell in turn.
! Adding into interior shells may cause duplication
    use startup, only: sc_inorb, ps_inorb, sc_orb, ps_orb, mx_ncorr
    integer, intent(in)  :: nefig
    integer, intent(out) :: npefig
    integer, intent(in)  :: noccsh(:)
    integer, intent(in)  :: nocorb(:,:)
    integer, intent(in)  :: nelcsh(:,:)
    type(pck), intent(in)  :: npack(:)
    integer, intent(out) :: npocsh(:)
    integer, intent(out) :: npocob(:,:)
    integer, intent(out) :: npelsh(:,:)
    type(pck), intent(inout) :: nppack(:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: iparty(:)
    integer              :: inp, in, noc, nel, i
    integer              :: korb, kshell, n, ish, morb, knext, kshel1
    integer              :: ncorr, orb, ipty, nprty
    type(pck)            :: nppck, npck

    psize = SIZE(nppack)
    sname = 'plus'
    inp = 0
    call zpck(nppck)
    L5: do in = 1, nefig
       noc = noccsh(in)
       npck = npack(in)
! count correctors in N-electron cfg:
       if (allocated(sc_orb)) then
         if (ps_inorb > 0) then
            ncorr = 0
            do i = 1, noc
               orb = nocorb(i,in)
               if (ANY(orb == ps_orb)) ncorr = ncorr + nelcsh(i,in)
               if (ANY(orb == sc_orb) .and. nelcsh(i,in) /= nelmax(orb)) &
                  ncorr = ncorr + 1
            end do
            if (ncorr > mx_ncorr) cycle
         end if
      else
         if (ps_inorb > 0) then
            ncorr = 0
            do i = 1, noc
               orb = nocorb(i,in)
               if (ANY(orb == ps_orb)) ncorr = ncorr + nelcsh(i,in)
            end do
            if (ncorr > mx_ncorr) cycle
         end if
       end if
! obtain parity of current configuration:
       ipty = iparty(nnq(in))

       knext = 1
       L10: do morb = 1, maxorb

! skip unwanted parity cases:
! lrgp: parity selection switch
!         lrgp = -1 => no selection, do both parities
!         lrgp = 0, 1 => select even or odd parity cases          
          nprty = MOD(ipty+ljcomp(morb), 2)
          if (lrgp /= -1 .and. nprty /= lrgp) cycle L10

          if (sc_inorb /= 0) then
             if (sc_inorb > 0 .and. ANY(morb == sc_orb)) cycle
          end if
          if (ps_inorb /= 0) then
             if (ANY(ps_inorb > 0 .and. morb == ps_orb) .and. &
               ncorr + 1 > mx_ncorr) cycle
          end if
          call adpack (nppck, npck, morb)
          kshel1 = knext
          L20: do kshell = kshel1, noc
             korb = nocorb(kshell,in)

! add the electron into an embedded empty shell
             if (morb < korb) then
                do i = 1, inp
                   if (nppck == nppack(i)) cycle L10
                end do
! count correctors and if threshold exceeded, filter
!!
                inp = inp + 1
                call overwrite (inp)
                nppack(inp) = nppck
                npocsh(inp) = noc + 1
                do ish = 1, kshell-1
                   npocob(ish,inp) = nocorb(ish,in)
                   npelsh(ish,inp) = nelcsh(ish,in)
                end do
                do ish = kshell, noc
                   npocob(ish+1,inp) = nocorb(ish,in)
                   npelsh(ish+1,inp) = nelcsh(ish,in)
                end do
                npocob(kshell,inp) = morb
                npelsh(kshell,inp) = 1
                cycle L10

! add the electron into an occupied shell if there is room
             else if (morb == korb)then
                nel = nelcsh(kshell,in)
                if (nel == nelmax(morb)) then
                   knext = kshel1 + 1
                else
                   do i  =  1, inp
                      if(nppck == nppack(i)) cycle L10
                   end do
                   inp = inp + 1
                   call overwrite (inp)
                   nppack(inp) = nppck
                   npocsh(inp) = noc
                   do ish = 1, noc
                      npocob(ish,inp) = nocorb(ish,in)
                      npelsh(ish,inp) = nelcsh(ish,in)
                   end do
                   npelsh(kshell,inp) = nel + 1
                   knext = kshel1 + 1
                end if
                cycle L10
             end if
          end do L20

! add into an empty shell above all occupied shells
          do i = 1, inp
             if (nppck == nppack(i)) cycle L10
          end do
          inp = inp + 1
          call overwrite (inp)
          nppack(inp) = nppck
          npocsh(inp) = noc + 1
          do ish = 1, noc
             npocob(ish,inp) = nocorb(ish,in)
             npelsh(ish,inp) = nelcsh(ish,in)
          end do
          npelsh(noc+1,inp) = 1
          npocob(noc+1,inp) = morb
       end do L10
    end do L5
    npefig = inp

    do i = 1, npefig
       n = npocsh(i)
       call wr_cfg (i, npelsh(1:n,i), njcomp(npocob(1:n,i)), &
            ljcomp(npocob(1:n,i)))
    end do
  end subroutine plus

  subroutine est_minus (np1, np2, npocsh, npocob, npelsh, nppack, &
       n1, n2, npack)
! count n electron configurations by removing 1 electron from
! each occupied cortex shell of each n+1 configuration
    integer, intent(in)    :: np1 ! # N+1 elec cfgs already processed
    integer, intent(in)    :: np2 ! total # N+1 electron configurations
    type(pck), intent(in)  :: nppack(:) ! packed N+1 configurations
    integer, intent(in)    :: n1 ! total # N elctron configs stored
    integer, intent(out)   :: n2 ! new total number of N elctron configs
! between the n+1 and the n electron configurations.
    type(pck), intent(inout) :: npack(:)
    integer, intent(in)    :: npocsh(:), npocob(:,:)
    integer, intent(in)    :: npelsh(:,:)
    integer                :: inp, npoc, korb, nel, i, kshell
    type(pck)              :: nppck, npck

    psize = SIZE(npack)
    sname = 'est_minus'
    n2 = n1
    call zpck(npck)
    inp_loop: do inp = np1+1, np2
       nppck = nppack(inp)
       npoc = npocsh(inp)
       kshell_loop: do kshell = 1, npoc
          korb = npocob(kshell,inp)
          nel = npelsh(kshell,inp)   ! # electrons in shell
          if (nel >= nelmin(korb)) then
             call mspack (npck, nppck, korb)
             do i = 1, n2    ! check current N-cfgs
                if (npck == npack(i)) cycle kshell_loop
             end do
             n2 = n2 + 1     ! new cfg, update count
             call overwrite (n2)
             npack(n2) = npck ! packed cfg
          end if
       end do kshell_loop
    end do inp_loop
    write (fo,'(/,a,i4,/)') 'est_minus: npefig = ', n2
  end subroutine est_minus

  subroutine est_plus (nefig, noccsh, nocorb, nelcsh, npack, npefig, &
       nppack, nnq, iparty)
! count N+1 configurations
! for each n electron configuration add 1 electron into each shell if
! it is not already full and into each higher shell in turn.
! Adding into interior shells may cause duplication
    integer, intent(in)  :: nefig
    integer, intent(out) :: npefig
    integer, intent(in)  :: noccsh(:)
    integer, intent(in)  :: nocorb(:,:)
    integer, intent(in)  :: nelcsh(:,:)
    type(pck), intent(in)    :: npack(:)
    type(pck), intent(inout) :: nppack(:)
    integer, intent(in)  :: nnq(:)
    integer, intent(in)  :: iparty(:)
    integer              :: inp, in, noc, nel, i, ipty, nprty
    integer              :: korb, kshell, morb, knext, kshel1
    type(pck)            :: nppck, npck

    psize = SIZE(nppack)
    sname = 'est_plus'
    inp = 0
    call zpck(nppck)
    L5: do in = 1, nefig
       noc = noccsh(in)
       npck = npack(in)
       knext = 1
! obtain parity of current configuration:
       ipty = iparty(nnq(in))
       L10: do morb = 1, maxorb
          nprty = MOD(ipty+ljcomp(morb), 2)
          if (lrgp /= -1 .and. nprty /= lrgp) cycle L10
          call adpack (nppck, npck, morb)
          kshel1 = knext
          L20: do kshell = kshel1, noc
             korb = nocorb(kshell,in)
             if (morb < korb) then ! add electron into embedded empty shell
                do i = 1, inp
                   if (nppck == nppack(i)) cycle L10
                end do
                inp = inp + 1
                call overwrite (inp)
                nppack(inp) = nppck
                cycle L10
             else if (morb == korb) then ! add electron into occupied shell
                nel = nelcsh(kshell,in)
                if (nel == nelmax(morb)) then
                   knext = kshel1 + 1
                else
                   do i  =  1, inp
                      if (nppck == nppack(i)) cycle L10
                   end do
                   inp = inp + 1
                   call overwrite (inp)
                   nppack(inp) = nppck
                   knext = kshel1 + 1
                end if
                cycle L10
             end if
          end do L20
! add into an empty shell above all occupied shells
          do i = 1, inp
             if (nppck == nppack(i)) cycle L10
          end do
          inp = inp + 1
          call overwrite (inp)
          nppack(inp) = nppck
       end do L10
    end do L5
    npefig = inp
    write (fo,'(/,a,i4,/)') 'est_plus: npefig = ', npefig
  end subroutine est_plus

  subroutine overwrite (n)
    integer, intent(in)  :: n ! current count
    if (n > psize) then
       write (fo,'(a)') TRIM(sname) // ': pack array overwrite'
       stop
    end if
  end subroutine overwrite
end module plus_minus
