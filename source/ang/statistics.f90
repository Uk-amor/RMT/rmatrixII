module statistics
! collect and print BB, BC statistics.
! Time-stamp: "2005-03-11 16:06:39 cjn"
  use precisn, only: wp
  use io_units, only: fo
  implicit none

  integer, save           :: integ = 1
  integer, parameter      :: m = HUGE(integ)  ! max integer size

  type bi             ! integer with overflow counter
     integer  :: s    ! sum
     integer  :: o    ! overflow count
  end type bi

  private
  public coll_stat, report_stat, reset_stat
  public coll_bc_stat, report_bc_stat, reset_bc_stat

  integer, save        :: nd_total = 0
  type(bi), save       :: irsd_tot
  integer, save        :: nld_tot = 0
  real(wp), save       :: bbd_tot = 0.0_wp
  real(wp), save       :: bbd_atot = 0.0_wp
  integer, save        :: bbd_cut = 0

  integer, save        :: ne_total = 0
  integer, save        :: nle_tot = 0
  type(bi), save       :: isre_tot
  real(wp), save       :: bbe_tot = 0.0_wp
  real(wp), save       :: bbe_atot = 0.0_wp
  integer, save        :: bbe_cut = 0

  integer, save        :: n1_total = 0
  integer, save        :: irs1_tot = 0
  real(wp), save       :: bb1_tot = 0.0_wp
  real(wp), save       :: bb1_atot = 0.0_wp
  integer, save        :: bb1_cut = 0

  integer, save        :: ncont_total = 0
  type(bi), save       :: irsde_tot
  integer, save        :: ncont1_total = 0
  integer, save        :: irs11_tot = 0
  real(wp), save       :: bc_tot = 0.0_wp
  real(wp), save       :: bc1_tot = 0.0_wp

contains

  subroutine coll_stat (ndcont, irsd, bb, necont, isre, &
       ncont1, irs1, bb1, lamdst, nlamd, lamest, nlame)
    integer, intent(in)    :: ndcont    ! # direct lambda values
    integer, intent(in)    :: irsd(:)   ! direct integral labels
    real(wp), intent(in)   :: bb(:)  ! direct integrals
    integer, intent(in)    :: lamdst(:)
    integer, intent(in)    :: nlamd(:)

    integer, intent(in)    :: necont    ! # exchange lambda values
    integer, intent(in)    :: isre(:)   ! exchange integral labels
    integer, intent(in)    :: lamest(:)
    integer, intent(in)    :: nlame(:)

    integer, intent(in)    :: ncont1    ! # one-electron integrals
    integer, intent(in)    :: irs1(:)   ! one-electron integral labels
    real(wp), intent(in)   :: bb1(:)    ! one-electron integrals

    integer                :: i, nd, ne, nd0, ne0
    real(wp)               :: t

    nd0 = nd_total
    do i = 1, ndcont
       nd_total  = nd_total + nlamd(i)
       nld_tot = nld_tot + lamdst(i)
       if (m - irsd_tot%s > irsd(i)) then
          irsd_tot%s = irsd_tot%s + irsd(i)
       else
          irsd_tot%s = irsd_tot%s - m
          irsd_tot%s = irsd_tot%s + irsd(i)
          irsd_tot%o = irsd_tot%o + 1
       end if
       if (irsd(i) < 0 .or. irsd_tot%s < 0) then
          write (fo,'(a,2i30)') 'stats: d-label error, ', irsd(i), &
               irsd_tot%s
          stop
       end if
    end do
    nd = nd_total - nd0
    bbd_tot = bbd_tot + SUM(bb(1:nd))
    bbd_atot = bbd_atot + SUM(ABS(bb(1:nd)))
    do i = 1, nd
       t = bb(i)
       if (ABS(t) > 1.0e-12_wp) bbd_cut = bbd_cut + 1
    end do

    ne0 = ne_total
    do i = 1, necont
       ne_total = ne_total + nlame(i)
       nle_tot = nle_tot + lamest(i)
       if (m - isre_tot%s > isre(i)) then
          isre_tot%s = isre_tot%s + isre(i)
       else
          isre_tot%s = isre_tot%s - m
          isre_tot%s = isre_tot%s + isre(i)
          isre_tot%o = isre_tot%o + 1
       end if
       if (isre(i) < 0 .or. isre_tot%s < 0) then
          write (fo,'(a,2i30)') 'stats: e-label error, ', isre(i), &
               isre_tot%s
          stop
       end if
    end do
    ne = ne_total - ne0
    bbe_tot = bbe_tot + SUM(bb(nd+1:nd+ne))
    bbe_atot = bbe_atot + SUM(ABS(bb(nd+1:nd+ne)))
    do i = nd+1, nd+ne
       t = bb(i)
       if (ABS(t) > 1.0e-12_wp) bbe_cut = bbe_cut + 1
    end do

    n1_total  = n1_total + ncont1
    irs1_tot = irs1_tot + SUM(irs1(1:ncont1))
    bb1_tot = bb1_tot + SUM(bb1(1:ncont1))
    bb1_atot = bb1_atot + SUM(ABS(bb1(1:ncont1)))
    do i = 1, ncont1
       t = bb1(i)
       if (ABS(t) > 1.0e-12_wp) bb1_cut = bb1_cut + 1
    end do
  end subroutine coll_stat

  subroutine reset_stat
    nd_total = 0
    irsd_tot%s = 0
    irsd_tot%o = 0
    nld_tot = 0
    bbd_tot = 0.0_wp
    bbd_atot = 0.0_wp
    bbd_cut = 0

    ne_total = 0
    isre_tot%s = 0
    isre_tot%o = 0
    nle_tot = 0
    bbe_tot = 0.0_wp
    bbe_atot = 0.0_wp
    bbe_cut = 0

    n1_total = 0
    irs1_tot = 0
    bb1_tot = 0.0_wp
    bb1_atot = 0.0_wp
    bb1_cut = 0
  end subroutine reset_stat

  subroutine report_stat
    write (fo,'(/,a,/)') 'ANGBB Statistics:'
    write (fo,'(/,a,i30)') 'nd_total = ', nd_total
    write (fo,'(/,a,i30)') 'nld_tot = ', nld_tot
    write (fo,'(a,2i30)')  'irsd_tot = ', irsd_tot%s, irsd_tot%o
    write (fo,'(a,2e30.15)') 'bbd_tot  = ', bbd_tot, bbd_atot
    write (fo,'(a,i30,/)') 'bbd_cut  = ', bbd_cut
    
    write (fo,'(/,a,i30)') 'ne_total = ', ne_total
    write (fo,'(/,a,i30)') 'nle_tot = ', nle_tot
    write (fo,'(a,2i30)') 'isre_tot = ', isre_tot%s, isre_tot%o
    write (fo,'(a,2e30.15)') 'bbe_tot  = ', bbe_tot, bbe_atot
    write (fo,'(a,i30,/)') 'bbe_cut  = ', bbe_cut
    
    write (fo,'(/,a,i30)') 'n1_total = ', n1_total
    write (fo,'(a,i30)') 'irs1_tot = ', irs1_tot
    write (fo,'(a,2e30.15)') 'bb1_tot  = ', bb1_tot, bb1_atot
    write (fo,'(a,i30,/)') 'bb1_cut  = ', bb1_cut
    
  end subroutine report_stat

  subroutine coll_bc_stat (nterm, ncont, irsde, bc, ncont1, irs1, bc1)
    integer, intent(in)   :: nterm
    integer, intent(in)   :: ncont
    integer, intent(in)   :: irsde(:)
    real(wp), intent(in)  :: bc(:)
    integer, intent(in)   :: ncont1
    integer, intent(in)   :: irs1
    real(wp), intent(in)  :: bc1
    integer               :: i

    if (ncont+ncont1 == 0) return
    if (ncont > 0) then
       do i = 1, ncont
          if (m - irsde_tot%s > irsde(i)) then
             irsde_tot%s = irsde_tot%s + irsde(i)
          else
             irsde_tot%s = irsde_tot%s - m
             irsde_tot%s = irsde_tot%s + irsde(i)
             irsde_tot%o = irsde_tot%o + 1
          end if
       end do
       ncont_total = ncont_total + ncont
       bc_tot = bc_tot + SUM(bc(:nterm))
    end if
    if (ncont1 > 0) then
       ncont1_total = ncont1_total + ncont1
       irs11_tot = irs11_tot + irs1
       bc1_tot = bc1_tot + bc1
    end if
  end subroutine coll_bc_stat

  subroutine reset_bc_stat
    ncont_total = 0
    irsde_tot%s = 0
    irsde_tot%o = 0
    bc_tot = 0.0_wp
    ncont1_total = 0
    irs11_tot = 0
    bc1_tot = 0.0_wp
  end subroutine reset_bc_stat

  subroutine report_bc_stat
    write (fo,'(/,a,/)') 'ANGBC Statistics:'
    write (fo,'(a,i30)') 'ncont_total = ', ncont_total
    write (fo,'(a,2i30)')  'irsde_tot = ', irsde_tot%s, irsde_tot%o
    write (fo,'(a,e30.15)') 'bc_tot  = ', bc_tot

    write (fo,'(/,a,i30)') 'ncont1_total = ', ncont1_total
    write (fo,'(a,2i30)')  'irs1_tot = ', irs11_tot
    write (fo,'(a,2e30.15)') 'bc1_tot  = ', bc1_tot
  end subroutine report_bc_stat
end module statistics
